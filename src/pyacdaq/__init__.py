#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
PyAcadq is a Python class library for Automation Control & Data Acquisition Systems.

PyAcadq is meant as a possible substitute (Plan B) for the LabVIEW based
[CS](https://git.gsi.de/EE-LV/CS) and [CS++](https://git.gsi.de/EE-LV/CSPP) frameworks.

pyacdaq_logger = logging.getLogger('pyacdaq') is used for package specific logging.

Classes
-------
base.Base(): Base class for classes maintaining unique resources.
exceptions.CommunicationTimeout(TimeoutError): Could be raised by DeviceArctors.
logger.MQTTLogHandler(logging.Handler): A handler writing logging records to MQTT broker topic.

Methods
-------
logger.configure_logging_handler(): Configure logging handlers.


Lizenziert unter EUPL V. 1.2

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
import logging
aci_logger = logging.getLogger('asyn_com_interface').addHandler(logging.NullHandler())
pyacdaq_logger: logging.Logger = logging.getLogger('pyacdaq').addHandler(logging.NullHandler())
