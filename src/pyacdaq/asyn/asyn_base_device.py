#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Module contains the _AsyncBase_ class for derived classes supporting asyncio in PyAcdaq.

It provides basic attributes and methods to support:
- asyncio task for periodic actions,
- asyncio task to handle commands received from queue,
- asyncio task to subscribe MQTT topics.
- publishing to MQTT topics.

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
import asyncio
import argparse
import getpass
import inspect
import logging
import os
import re
import sys
from typing import Any, override
import pint
from abc import abstractmethod
import aiomqtt
import nest_asyncio
from .asyn_base import ureg, AsynBase

pyacdaq_logger: logging.Logger = logging.getLogger('pyacdaq')


class AsynBaseDevice(AsynBase):
    """
    Base class for derived device classes in PyAcdaq supporting asyncio.

    It provides basic attributes and methods for:
    - IDN query,
    - Revision query,
    - Reset and
    - SelfTest

    """

    def __init__(self,
                 name: str,
                 mqtt_client: aiomqtt.Client,
                 top_level_topic: str,
                 resource: str | None = None,
                 id_query_flag: bool = True,
                 reset_flag: bool = True,
                 reset_options: str | None = None,
                 selftest_flag: bool = True
                 ) -> None:
        """
        Initialize this instance.

        Parameters
        ----------
        name : str
            Name of instance.
        mqtt_client : aiomqtt.Client
            mqtt client connection.
        top_level_topic : str
            Name of top_level_topic to be uses as prefix.
        resource : str
            Resource string containing the device connection information.
        id_query_flag: bool
            Query device id if True. The default is True.
        reset_flag: bool
            Reset device if True. The default is True.
        reset_options: str | None
            Options to used when resetting device. The default is None.
        selftest_flag: bool
            Selftest device if True. The default is True.

        Returns
        -------
        None

        """
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        if not isinstance(name, str):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid name.')
        if not isinstance(mqtt_client, aiomqtt.Client):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid mqtt_client.')
        if not isinstance(top_level_topic, str):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid parameter.')
        if not isinstance(resource, str | None):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid resource.')
        if not isinstance(id_query_flag, bool):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid id_query_flag.')
        if not isinstance(reset_flag, bool):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid reset_flag.')
        if not isinstance(reset_options, str | None):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid reset_options.')
        if not isinstance(selftest_flag, bool):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid parameter.')
        super().__init__(name, mqtt_client, top_level_topic)
        self._resource: str | None = resource
        self._id_query_flag: bool = id_query_flag
        self._reset_flag: bool = reset_flag
        self._reset_options: str | None = reset_options
        self._selftest_flag: bool = selftest_flag
        self._firmware_revision: str = 'NA'
        self._hardware_revision: str = 'NA'
        self._idn: str = 'NA'
        self._initialized: int = -1
        self._reset_result: str = 'NA'
        self._selftest_code: int = -1
        self._selftest_message: str = 'NA'
        self._serial_number: str = 'NA'
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} initialized.')

    def __del__(self) -> None:
        """Finalize this instance."""
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{fn}: {self._name} Finalizing self.')
        super().__del__()

    # Private methods
    # Protected methods
    @abstractmethod
    async def _close(self):
        """
        Set instrument to save mode and close connection.

        Virtual protected method.
        Override method needs to be implemented by derived class.

        Returns
        -------
        int
            Error code.

        """
        self._initialized = -1
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/initialized', payload=self._initialized, retain=False)
        return -1

    @override
    def _command_help(self) -> str:
        """Return help text for command line interface as defined in derived class override method."""
        help_text: str = \
            '  id_query\n'\
            '  revision_query\n'\
            '  reset options:str\n'\
            '  selftest\n'
        return super()._command_help() + help_text

    @override
    async def _handle_command(self, q_item: list[Any]) -> None:
        """Handle command received from queue."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        if not isinstance(q_item, list):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid q_item.')
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Handle command: {q_item}')
        match q_item[0]:
            case 'id_query':
                try:
                    await self.id_query()
                except Exception as ex:
                    pyacdaq_logger.warning(f'{tn}.{cn}.{fn}: {self._name} Command id_query ignored. {ex=}')
            case 'revision_query':
                try:
                    await self.revision_query()
                except Exception as ex:
                    pyacdaq_logger.warning(f'{tn}.{cn}.{fn}: {self._name} Command revision_query ignored. {ex=}')
            case 'reset':
                try:
                    await self.reset(q_item[0])
                except Exception as ex:
                    pyacdaq_logger.warning(f'{tn}.{cn}.{fn}: {self._name} Command reset ignored. {ex=}')
            case 'selftest':
                try:
                    await self.selftest()
                except Exception as ex:
                    pyacdaq_logger.warning(f'{tn}.{cn}.{fn}: {self._name} Command selftest ignored. {ex=}')
            case _:
                await super()._handle_command(q_item)

    @override
    async def _handle_subscriptions(self, topic_levels: list[str], payload: str) -> None:
        """Handle subscribed topics."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Handle subscribed topics.')
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} {topic_levels=}:{payload=!r}')
        if not isinstance(topic_levels, list):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid topic_levels.')
        if not isinstance(payload, str):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid payload.')
        match topic_levels[1]:
            case _ if topic_levels[1] == self._name:
                match topic_levels[2]:
                    case _ if topic_levels[2] == 'set':
                        match topic_levels[3]:
                            case _ if topic_levels[3] == 'reset':
                                try:
                                    await self._command_queue.put(['reset', payload])
                                except Exception as ex:
                                    pyacdaq_logger.warning(f'{tn}.{cn}.{fn}: {self._name} reset with {payload=} ignored. {str(ex)}')
                                    await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/log_msg', payload=f'reset with {payload=} ignored. {str(ex)}', retain=True)
                            case _ if topic_levels[3] == 'selftest':
                                try:
                                    await self._command_queue.put(['selftest'])
                                except Exception as ex:
                                    pyacdaq_logger.warning(f'{tn}.{cn}.{fn}: {self._name} selftest with {payload=} ignored. {str(ex)}')
                                    await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/log_msg', payload=f'selftest with {payload=} ignored. {str(ex)}', retain=True)
                            case _:
                                await super()._handle_subscriptions(topic_levels, payload)
                    case _:
                        await super()._handle_subscriptions(topic_levels, payload)
            case _:
                await super()._handle_subscriptions(topic_levels, payload)

    @abstractmethod
    async def _id_query(self) -> str:
        """
        Perform id query of instrument.

        Virtual protected method. Override method needs to be implemented by derived class.

        Returns
        -------
        str
            Instrument IDN.

        """
        return 'NA'

    @abstractmethod
    async def _initalize(self, resource, id_query_flag=True, reset_flag=True, reset_options=None, selftest_flag=True) -> int:
        """
        Open connection and initialize instrument.

        Virtual protected method. Extend method needs to be implemented by derived class.

        Parameters
        ----------
        resource : str
            Resource identifier.
        id_query_flag : bool
            Flag requesting an id_query. Default isTrue.
        reset_flag : bool
            Flag requesting a reset. Default is True.
        reset_options : str
            String containing reset options. Default is None.
        selftest_flag : bool
            Flag requesting an id_query. Default is True.

        Returns
        -------
        int
            Error code.

        """
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        if not isinstance(resource, str | None):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid resource.')
        if not isinstance(id_query_flag, bool):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid id_query_flag.')
        if not isinstance(reset_flag, bool):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid reset_flag.')
        if not isinstance(reset_options, str | None):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid reset_options.')
        if not isinstance(selftest_flag, bool):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid parameter.')
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Initialize device at {resource=}.')
        try:
            if id_query_flag:
                await self.id_query()
            self._firmware_revision, self._hardware_revision = await self.revision_query()
            if reset_flag:
                await self.reset(reset_options)
            if selftest_flag:
                await self.selftest()
            return 0
        except Exception as ex:
            pyacdaq_logger.exception(f'{tn}.{cn}.{fn}: {self._name} Exception cought. {ex}')

    @override
    async def _periodic_action(self) -> None:
        """Implement periodic actions in derived class override method."""
        await super()._periodic_action()

    @override
    async def _periodic_start(self) -> None:
        """Implement periodic start actions in derived class override method."""
        await super()._periodic_start()

    @override
    async def _periodic_stop(self) -> None:
        """Implement periodic stop actions in derived class override method."""
        await super()._periodic_stop()

    @override
    async def _publish_topics(self) -> None:
        """Publish topics in derived class override method."""
        await super()._publish_topics()
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/initialized', payload=self._initialized, retain=True)
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/resource', payload=str(self._resource), retain=True)
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/set/reset', payload=str(self._reset_options), retain=False)
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/set/selftest', payload='0', retain=False)

    @abstractmethod
    async def _reset(self, reset_options: str | None = None) -> str:
        """
        Reset instrument with options.

        Virtual protected method. Override method needs to be implemented by derived class.

        Parameters
        ----------
        reset_options : str | None
            String containing reset options. Default is None.

        Returns
        -------
        str
            Reset result.

        """
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        if not isinstance(reset_options, str | None):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid reset_options.')
        return 'NA'

    @abstractmethod
    async def _revision_query(self) -> tuple[str, str]:
        """
        Perform instrument revision query, firmware and hardware.

        Virtual protected method. Override method needs to be implemented by derived class.

        Returns
        -------
        str
            Firmware revision.
        str
            Hardware revision.

        """
        return ("NA", "NA")

    @abstractmethod
    async def _selftest(self) -> tuple[int, str]:
        """
        Perform instrument selftest.

        Virtual protected method. Override method needs to be implemented by derived class.

        Returns
        -------
        int
            Selftest return code.
        str
            Selftest message.

        """
        return (-1, 'NA')

    @abstractmethod
    async def _sn_query(self) -> str:
        """
        Perform instrument serial number query.

        Virtual protected method. Override method needs to be implemented by derived class.

        Returns
        -------
        str
            Serial number.

        """
        return 'NA'

    @override
    async def _start(self, subscribe_task: bool = False, periodic_interval: pint.Quantity = -1. * ureg.s) -> None:
        """
        Perform start actions in derived class override method.

        Parameters
        ----------
        subscribe: bool, optional
            Start subcriber task if subscribe is True. The default is False.
        periodic_interval: pint.Quantity, optional
            Start periodic task if greater or equal 0. The default is -1s.

        Returns
        -------
        None

        """
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        if not isinstance(subscribe_task, bool):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid subscribe_task.')
        if not isinstance(periodic_interval, pint.Quantity):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid periodic_interval.')
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Perform start actions.')
        self._initialized = await self.initalize(
            resource=self._resource,
            id_query_flag=self._id_query_flag,
            reset_flag=self._reset_flag,
            reset_options=self._reset_options,
            selftest_flag=self._selftest_flag)
        await super()._start(subscribe_task, periodic_interval)
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Performing start actions done.')

    @override
    async def _stop(self) -> None:
        """
        Perform stop actions in derived class override method.

        Returns
        -------
        None
        """
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Perform stop actions.')
        await self.close()
        await super()._stop()
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Performing stop actions done.')

    @override
    async def _subscribe_topics(self) -> list[str]:
        """
        Define topics to subscribe in derived class override method.

        Returns
        -------
        list[str]
            Topics to subscribe.

        """
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Subscribe topics.')
        topics: set[str] = await super()._subscribe_topics()
        topics.add(f'{self._top_level_topic}/{self._name}/set/reset')
        topics.add(f'{self._top_level_topic}/{self._name}/set/selftest')
        return topics

    @override
    def _sw_revision(self) -> str:
        """
        Revision of this class inclusive parents.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        string
            Software version

        """
        return 'AsyncBaseDevice_0.0.0.0' + '|' + super()._sw_revision()

    # Public methods
    async def close(self):
        """
        Set instrument to save mode and close connection.

        Returns
        -------
        int
            Error code.

        """
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Closing connection.')
        return await self._close()

    async def id_query(self) -> tuple[str, str]:
        """
        Perform id query of instrument.

        Returns
        -------
        str
            Instrument IDN.
        str
            Serial number.

        """
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Performing id query.')
        self._idn = await self._id_query()
        self._serial_number = await self._sn_query()
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/idn', payload=self._idn, retain=True)
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/sn', payload=self._serial_number, retain=True)
        return (self._idn, self._serial_number)

    async def initalize(self,
                        resource: str | None = None,
                        id_query_flag: bool = True,
                        reset_flag: bool = True,
                        reset_options: str | None = None,
                        selftest_flag: bool = True):
        """
        Open connection and initialize instrument.

        Parameters
        ----------
        resource : str
            Resource identifier.
        id_query_flag : bool
            Flag requesting an id_query. Default is True.
        reset_flag : bool
            Flag requesting a reset. Defaultis True.
        reset_options : str
            String containing reset options. Default is None.
        selftest_flag : bool
            Flag requesting an id_query. Default is True.

        Returns
        -------
        None

        """
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        if not isinstance(resource, str | None):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid resource.')
        if not isinstance(id_query_flag, bool):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid id_query_flag.')
        if not isinstance(reset_flag, bool):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid reset_flag.')
        if not isinstance(reset_options, str | None):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid reset_options.')
        if not isinstance(selftest_flag, bool):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid parameter.')
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Initializing device.')
        self._resource = resource
        self._id_query_flag = id_query_flag
        self._reset_flag = reset_flag
        self._reset_options = reset_options
        self._selftest_flag = selftest_flag
        try:
            await self.close()
        except Exception as ex:
            pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Exception during close(). {ex=}')
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Initializing device with {resource=} {id_query_flag=} {reset_flag=} {reset_options=} {selftest_flag=} .')
        self._initialized = await self._initalize(self._resource, self._id_query_flag, self._reset_flag, self._reset_options, self._selftest_flag)
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/set/reset', payload=str(self._reset_options), retain=False)
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/resource', payload=str(self._resource), retain=True)
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/initialized', payload=self._initialized, retain=True)

    async def reset(self, reset_options: str | None = None) -> str:
        """
        Reset instrument with options.

        Parameters
        ----------
        reset_options : str
            String containing reset options. Default is None.

        Returns
        -------
        str
            Reset result.

        """
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        if not isinstance(reset_options, str | None):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid reset_options.')
        pyacdaq_logger.warning(f'{tn}.{cn}.{fn}: {self._name} Performing reset with {str(reset_options)=}')
        self._reset_options = reset_options
        self._reset_result = await self._reset(self._reset_options)
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/reset_result', payload=self._reset_result, retain=True)
        return self._reset_result

    async def revision_query(self):
        """
        Perform instrument revision query, firmware and hardware.

        Returns
        -------
        str
            Firmware revision.
        str
            Hardware revision.

        """
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Performing revision query.')
        self._firmware_revision, self._hardware_revision = await self._revision_query()
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/firmware_revision', payload=self._firmware_revision, retain=True)
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/hardware_revision', payload=self._hardware_revision, retain=True)
        return self._firmware_revision, self._hardware_revision

    async def selftest(self) -> tuple[int, str]:
        """
        Perform instrument selftest.

        Returns
        -------
        int
            Selftest return code.
        str
            Selftest message.

        """
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Performing selftest.')
        self._selftest_code, self._selftest_message = await self._selftest()
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} {self._selftest_code=} {self._selftest_message=}')
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/selftest_code', payload=self._selftest_code, retain=True)
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/selftest_message', payload=self._selftest_message, retain=True)
        return self._selftest_code, self._selftest_message

    # Getter
    def get_idn(self) -> str:
        """Get property device identification."""
        return self._idn

    def get_selftest_code(self) -> int:
        """Get property selftest_code."""
        return self._selftest_code

    def get_selftest_message(self) -> str:
        """Get property selftest_message."""
        return self._selftest_message

    def get_serial_number(self) -> str:
        """Get property serial_number."""
        return self._serial_number

    def get_resource(self) -> str | None:
        """Get property resource."""
        return self._resource

    # Setter
    # Properties
    Idn = property(get_idn)
    SelftestCode = property(get_selftest_code)
    SelftestMessage = property(get_selftest_message)
    SerialNumber = property(get_serial_number)
    Resource = property(get_resource)


async def do_something(mqtt_client: aiomqtt.Client, top_level_topic: str):
    """Do something within mqtt client context."""
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    if not isinstance(top_level_topic, str):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid top_level_topic.')
    try:
        print(f'{tn}.{cn}.{fn}: Instantiate and start.')
        abd_0: AsynBaseDevice = AsynBaseDevice('asynBaseDevice_0', mqtt_client, top_level_topic, resource='/dev/tty0', id_query_flag=True, reset_flag=True, reset_options='options', selftest_flag=True)
        print(f'{tn}.{cn}.{fn}: {abd_0.Name=}, {str(abd_0.Birthday)=}, {abd_0.SWRevision=}, {abd_0.PeriodicInterval=}')
        print(f'{tn}.{cn}.{fn}: Command_help\n{abd_0.command_help()}')
        print(f'{tn}.{cn}.{fn}: {await abd_0.start(subscribe_task=True, periodic_interval=-1. * ureg.s)=}')
        if False:
            await asyncio.sleep(10.)
            print(f'{tn}.{cn}.{fn}: Use methods for manipulation.')
            print(f'{tn}.{cn}.{fn}: {abd_0.PeriodicActive=} {abd_0.PeriodicInterval=} {abd_0.PeriodicCounter=}')
            print(f'{tn}.{cn}.{fn}: {await abd_0.start_periodic(10. * ureg.s)=}')
            await asyncio.sleep(1.)
            print(f'{tn}.{cn}.{fn}: {abd_0.PeriodicActive=} {abd_0.PeriodicInterval=} {abd_0.PeriodicCounter=}')
            await asyncio.sleep(10.)
            print(f'{tn}.{cn}.{fn}: {abd_0.PeriodicActive=} {abd_0.PeriodicInterval=} {abd_0.PeriodicCounter=}')
            await abd_0.set_periodic_interval(2. * ureg.s)
            await asyncio.sleep(10.)
            print(f'{tn}.{cn}.{fn}: {abd_0.PeriodicActive=} {abd_0.PeriodicInterval=} {abd_0.PeriodicCounter=}')
            await abd_0.stop_periodic()
            await asyncio.sleep(10.)
            print(f'{tn}.{cn}.{fn}: {abd_0.PeriodicActive=} {abd_0.PeriodicInterval=} {abd_0.PeriodicCounter=}')
            print(f'{tn}.{cn}.{fn}: Use queue for manipulation.')
            abd_0_cq: asyncio.Queue = abd_0.CommandQueue
            print(f'{tn}.{cn}.{fn}: {abd_0.PeriodicActive=} {abd_0.PeriodicInterval=} {abd_0.PeriodicCounter=}')
            await abd_0_cq.put(['reset', 'options'])
            await abd_0_cq.put(['selftest'])
            await abd_0_cq.put(['periodic_start', 1. * ureg.s])
            await asyncio.sleep(1.)
            print(f'{tn}.{cn}.{fn}: {abd_0.PeriodicActive=} {abd_0.PeriodicInterval=} {abd_0.PeriodicCounter=}')
            await asyncio.sleep(10.)
            print(f'{tn}.{cn}.{fn}: {abd_0.PeriodicActive=} {abd_0.PeriodicInterval=} {abd_0.PeriodicCounter=}')
            await abd_0_cq.put(['periodic_interval', 2. * ureg.s])
            await asyncio.sleep(10.)
            print(f'{tn}.{cn}.{fn}: {abd_0.PeriodicActive=} {abd_0.PeriodicInterval=} {abd_0.PeriodicCounter=}')
            await abd_0_cq.put(['periodic_stop'])
            await asyncio.sleep(10.)
            print(f'{tn}.{cn}.{fn}: {abd_0.PeriodicActive=} {abd_0.PeriodicInterval=} {abd_0.PeriodicCounter=}')
        print('Waiting for topic changes...')
        while True:
            await asyncio.sleep(1.)
    finally:
        print(f'{tn}.{cn}.{fn}: Stop and delete')
        try:
            if abd_0:
                await abd_0.stop()
        finally:
            if abd_0:
                del abd_0


async def main() -> None:
    """Implement yout test code here."""
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    parser = argparse.ArgumentParser(prog=os.path.basename(__file__),
                                     description='This is the asyn_base test.',
                                     epilog='Default command: python -m pyacdaq.asyn.asyn_base -b localhost -p 1883')
    parser.add_argument('-b', '--broker', dest='broker', required=False, default='localhost', help='MQTT Broker Node (Name or IP)')
    # parser.add_argument('-b', '--broker', dest='broker', required=True, default=None, help='MQTT Broker Node (Name or IP)')
    parser.add_argument('-p', '--port', dest='port', type=int, default=1883, help='MQTT Broker port (Default:1883)')
    parser.add_argument('--mqtt_user', dest='mqtt_user', default=None, help='MQTT Broker User Name')
    parser.add_argument('--top_level_topic', dest='top_level_topic', default='PyACDAQ', help='MQTT top-level topic. Default is "PyACDAQ")')
    args = parser.parse_args()
    while True:
        answer: str = input('THIS IS BETA SOFTWARE! You will use it on your own risk!\nDo you want to continue (yes or default: no)? ')
        if re.match('yes', answer):
            break
        if len(answer) == 0 or re.match('no', answer):
            sys.exit(1)
    mqtt_password: str | None = None
    if args.mqtt_user:  # but required
        print('Set password for MQTT')
        mqtt_password = getpass.getpass()
        if len(mqtt_password) == 0:
            sys.exit(-2)
    mqtt: dict[str, int | str | None] = {'broker': args.broker,
                                         'port': args.port,
                                         'user': args.mqtt_user,
                                         'password': mqtt_password}
    try:
        async with aiomqtt.Client(hostname=mqtt['broker'],
                                  port=mqtt['port'],
                                  username=mqtt['user'],
                                  password=mqtt['password'],
                                  will=aiomqtt.Will(topic=args.top_level_topic, payload='disconnected', retain=True),
                                  clean_session=True
                                  ) as mqtt_client:
            await mqtt_client.publish(args.top_level_topic, payload='online', retain=True)
            await mqtt_client.publish(args.top_level_topic + '/log_msg', payload='', retain=True)
            await do_something(mqtt_client, args.top_level_topic)
            await mqtt_client.publish(args.top_level_topic, payload='offline', retain=True)
    except Exception as ex:
        pyacdaq_logger.exception(f'{tn}.{cn}.{fn}: Stopping due to exception: {ex}')


if __name__ == '__main__':
    print(os.path.basename(__file__) + "\n\
Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme\n")
    logging.basicConfig(filename='./pyacdaq.log', filemode='w', encoding='utf-8', level=logging.DEBUG, style='{', format='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    pyacdaq_logger.setLevel(logging.DEBUG)
    nest_asyncio.apply()
    # Change to the "Selector" event loop if platform is Windows
    if sys.platform.lower() == "win32" or os.name.lower() == "nt":
        from asyncio import set_event_loop_policy, WindowsSelectorEventLoopPolicy
        set_event_loop_policy(WindowsSelectorEventLoopPolicy())
    # Run your async application as usual
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print('Script interrupted by <Ctr+C>')
    except asyncio.exceptions.CancelledError:
        pass
    except BaseException as e:
        # print(f'__main__: BaseException cought. {type(e)} {e}')
        print(f'__main__: BaseException cought. {type(e)}')
        pass
    logging.shutdown()
    print(__name__, 'Done.')
