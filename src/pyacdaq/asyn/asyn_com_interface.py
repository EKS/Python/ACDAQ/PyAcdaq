#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Module contains classes for communication via serial line and socket.

- AsynComInterface: Base class
- AsynSerialInterface: Derived class of CommunictionInterface
- AsynSocketInterface: Derived class of CommunictionInterface

Known issues:
- Basic function are implemented for now.
- Missing functionalitiy can be added on demand.

Testing
This module can be tested using
- scpi_device_simulator_ethernet.py and/or
- scpi_device_simulator_serial.py
to be found at https://git.gsi.de/EKS/Python/ACDAQ/PyAcdaq/-/tree/master/examples/scpi_device?ref_type=heads

Copyright 2024 Brand New Technologies
Dr. Holger Brand, Asternweg 12a, 64291 Darmstadt, Germany
eMail: Holger.Brand@BrandNewTechnologies.de
Web: https//www.brandnewtechnolgies.de
"""
from abc import abstractmethod
import asyncio
import inspect
import logging
import re
from typing import override
import aioserial

aci_logger = logging.getLogger('asyn_com_interface')


class AsynComInterface:
    """Base class for device communication via asyncio."""

    def __init__(self, timeout: float = 60):
        self._timeout = timeout

    @abstractmethod
    async def __aenter__(self):
        """Enter context manager."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        aci_logger.debug(f'{tn}.{cn}.{fn}: AsynComInterface: Entering context manager...')
        raise NotImplementedError('This class ist abstract.')

    @abstractmethod
    async def __aexit__(self, exc_type, exc, tb):
        """Exit context manager."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        aci_logger.debug(f'{tn}.{cn}.{fn}: AsynComInterface: Exiting context manager...')
        raise NotImplementedError('This class ist abstract.')

    @abstractmethod
    async def open(self) -> None:
        """Open connection to device."""
        raise NotImplementedError('This class ist abstract.')

    @abstractmethod
    async def read_bytes(self, size: int = 1) -> bytes:
        """Read bytes from device."""
        raise NotImplementedError('This class ist abstract.')

    @abstractmethod
    async def read_line(self) -> str:
        """Read line from device (until NL)."""
        raise NotImplementedError('This class ist abstract.')

    @abstractmethod
    async def read_until(self, termination: str) -> str:
        """Read from device until termination."""
        raise NotImplementedError('This class ist abstract.')

    @abstractmethod
    async def write(self, message: str, termination: str) -> None:
        """Write line with termination to device."""
        raise NotImplementedError('This class ist abstract.')

    @abstractmethod
    async def write_bytes(self, bytes_to_write: bytes) -> None:
        """Write bytes to device."""
        raise NotImplementedError('This class ist abstract.')

    @abstractmethod
    async def close(self) -> None:
        """Close connection to device."""
        raise NotImplementedError('This class ist abstract.')

    @staticmethod
    def create(*args, **kwargs):
        """Create an concrete instance of a class derived from this. (Factory Pattern)."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        aci_logger.debug(f'{tn}.{cn}.{fn}: AsynComInterface.create: Parameters: {args}, Keyword-Parameters: {kwargs}')
        # print(kwargs)
        # for key in kwargs:
        #     print(f'{key} = {kwargs[key]}')
        try:
            if 'timeout' in kwargs:
                timeout: int = kwargs["timeout"]
            else:
                timeout = 60
            if 'interface' in kwargs:
                if not kwargs['interface']:
                    raise ValueError('Interface not specified')
                try:
                    host: str = re.split(':', kwargs['interface'])[0]
                    port: int = int(re.split(':', kwargs['interface'])[1])
                    aci_logger.debug(f'{tn}.{cn}.{fn}: AsynComInterface: Connect to {host}:{port} with timeout of {timeout}s')
                    return AsynSocketInterface(timeout=timeout, host=host, port=port)
                except IndexError:
                    interface: str | None = kwargs.get('interface')
                    # Set serial communication parameters
                    baudrate: int = kwargs.get('baudrate', 9600)
                    bytesize: int = kwargs.get('bytesize', aioserial.EIGHTBITS)
                    parity: str = kwargs.get('parity', aioserial.PARITY_NONE)
                    stopbits: int = kwargs.get('stopbits', aioserial.STOPBITS_ONE)
                    xonxoff: bool = kwargs.get('xonxoff', False)
                    aci_logger.debug(f'{tn}.{cn}.{fn}: AsynComInterface: Connect to {interface} using {baudrate=}, {bytesize=}, {parity=}, {stopbits=}, {xonxoff=} with timeout of {timeout}s')
                    return AsynSerialInterface(timeout=timeout, interface=interface, baudrate=baudrate, bytesize=bytesize, parity=parity, stopbits=stopbits, xonxoff=xonxoff)
        except KeyError as ex:
            aci_logger.debug(f'{tn}.{cn}.{fn}: AsynComInterface.create: Exception cought. {ex}')
            raise KeyError from ex


class AsynSerialInterface(AsynComInterface):
    """Derived class for device communication via aioserial."""

    @override
    def __init__(self,
                 timeout: float = 60,
                 interface: str | None = None,
                 baudrate: int = 9600,
                 bytesize=aioserial.EIGHTBITS,
                 parity=aioserial.PARITY_NONE,
                 stopbits=aioserial.STOPBITS_ONE,
                 xonxoff: bool = False):
        super().__init__(timeout)
        self._interface = interface
        self._baudrate = baudrate
        self._bytesize = bytesize
        self._parity = parity
        self._stopbits = stopbits
        self._xonxoff = xonxoff
        self._ser = None

    @override
    async def __aenter__(self):
        """Enter context manager."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        aci_logger.debug(f'{tn}.{cn}.{fn}: AsynSerialInterface: Entering context manager...')
        await self.open()
        return self

    @override
    async def __aexit__(self, exc_type, exc, tb):
        """Exit context manager."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        await self.close()
        aci_logger.debug(f'{tn}.{cn}.{fn}: AsynSerialInterface: Exiting context manager...')

    @override
    async def open(self) -> None:
        """Open connection to device."""
        self._ser = aioserial.AioSerial(
            self._interface,
            baudrate=self._baudrate,
            bytesize=self._bytesize,
            parity=self._parity,
            stopbits=self._stopbits,
            xonxoff=self._xonxoff,
            timeout=self._timeout)

    @override
    async def read_bytes(self, size: int = 1) -> bytes:
        """Read bytes from device."""
        assert self._ser
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        bytes_read: int = await self._ser.read_async(size)
        aci_logger.debug(f'{tn}.{cn}.{fn}: AsynSerialInterface.read_bytes(): Received = {bytes_read}')
        return bytes_read

    @override
    async def read_line(self) -> str:
        """Read line from device (until NL)."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        assert self._ser
        line = await self._ser.readline_async()
        aci_logger.debug(f'{tn}.{cn}.{fn}: AsynSerialInterface.read_line(): Received = {line}')
        return re.split('\n', line.decode('utf-8'))[0]

    @override
    async def read_until(self, termination: str) -> str:
        """Read from device until termination."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        assert self._ser
        line = await self._ser.read_until_async(termination.encode('utf-8'))
        aci_logger.debug(f'{tn}.{cn}.{fn}: AsynSerialInterface.read_until(): Received = {line}')
        return re.split(termination, line.decode('utf-8'))[0]

    @override
    async def write(self, message: str, termination: str) -> None:
        """Write string with termination to device."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        assert self._ser
        str_to_send = message + termination
        aci_logger.debug(f'{tn}.{cn}.{fn}: AsynSerialInterface.write(): Sending: {str_to_send}')
        await self._ser.write_async(str_to_send.encode('utf-8'))

    @override
    async def write_bytes(self, bytes_to_write: bytes) -> None:
        """Write bytes to device."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        assert self._ser
        aci_logger.debug(f'{tn}.{cn}.{fn}: AsynSerialInterface.write(): Sending: {bytes_to_write}')
        await self._ser.write_async(bytes_to_write)

    @override
    async def close(self) -> None:
        """Close connection to device."""
        if self._ser:
            self._ser.close()
            self._ser = None


class AsynSocketInterface(AsynComInterface):
    """Derived class for device communication via asyncio streams."""

    @override
    def __init__(self, timeout: float = 60, host: str | None = None, port: int | None = None):
        super().__init__(timeout)
        self._host = host
        self._port = port
        # self._timeout = timeout
        self._reader: asyncio.StreamReader | None = None
        self._writer: asyncio.StreamWriter | None = None

    @override
    async def __aenter__(self):
        """Enter context manager."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        aci_logger.debug(f'{tn}.{cn}.{fn}: AsynSocketInterface: Entering context manager...')
        await self.open()
        return self

    @override
    async def __aexit__(self, exc_type, exc, tb):
        """Exit context manager."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        await self.close()
        aci_logger.debug(f'{tn}.{cn}.{fn}: AsynSocketInterface: Exiting context manager...')

    @override
    async def open(self) -> None:
        """Open connection to device."""
        self._reader, self._writer = await asyncio.open_connection(self._host, self._port)

    @override
    async def read_bytes(self, size: int = 1) -> bytes:
        """Read bytes from device."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        assert self._reader
        bytes_read: bytes = await self._reader.read(size)
        aci_logger.debug(f'{tn}.{cn}.{fn}: AsynSocketInterface.read_bytes(): Received = {bytes_read!r}')
        return bytes_read

    @override
    async def read_line(self) -> str:
        """Read line from device (until NL)."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        assert self._reader
        line: bytes = await self._reader.readline()
        aci_logger.debug(f'{tn}.{cn}.{fn}: AsynSocketInterface.read_line(): Received: {line!r}')
        return re.split('\n', line.decode('utf-8'))[0]

    @override
    async def read_until(self, termination: str) -> str:
        """Read from device until termination."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        assert self._reader
        line: bytes = await self._reader.readuntil(termination.encode('utf-8'))
        aci_logger.debug(f'{tn}.{cn}.{fn}: AsynSocketInterface.read_until(): Received: {line!r}')
        return re.split(termination, line.decode('utf-8'))[0]

    @override
    async def write(self, message, termination: str) -> None:
        """Write string with termination to device."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        assert self._writer
        str_to_send = message + termination
        aci_logger.debug(f'{tn}.{cn}.{fn}: AsynSocketInterface.write(): Sending: {str_to_send}')
        self._writer.write(str_to_send.encode('utf-8'))
        await self._writer.drain()

    @override
    async def write_bytes(self, bytes_to_write: bytes) -> None:
        """Write bytes to device."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        assert self._writer
        aci_logger.debug(f'{tn}.{cn}.{fn}: AsynSocketInterface.write(): Sending: {bytes_to_write!r}')
        self._writer.write(bytes_to_write)
        await self._writer.drain()

    @override
    async def close(self) -> None:
        """Close connection to device."""
        if self._writer:
            self._writer.close()
            await self._writer.wait_closed()
            self._reader = None
            self._writer = None
