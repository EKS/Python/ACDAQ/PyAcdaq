#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This module contains pyacdaq classes supporting asyncio.

Classes
-------
asyn.AsynBase(base.Base())
    Base class for active objects.
asyn.AsynDevice(asyn.AsynBase)
    Base class for derived device classes.

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
