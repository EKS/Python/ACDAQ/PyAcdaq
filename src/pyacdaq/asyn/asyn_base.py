#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Module contains the _AsynBase_ class for derived classes supporting asyncio in PyAcdaq.

It provides basic attributes and methods to support:
- asyncio task for periodic actions,
- asyncio task to handle commands received from queue,
- asyncio task to subscribe MQTT topics.
- publishing to MQTT topics.

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
import asyncio
import argparse
import getpass
import inspect
import logging
import os
import re
import sys
import time
from typing import Any, override
import pint
from abc import abstractmethod
import aiomqtt
import nest_asyncio
from pyacdaq.base import Base

pyacdaq_logger: logging.Logger = logging.getLogger('pyacdaq')
ureg = pint.UnitRegistry(system='SI')
ureg.formatter.default_format = "~P"


async def event_wait(evt: asyncio.Event,
                     timeout: pint.Quantity
                     ) -> bool:
    """
    Wait for event with timeout.

    Parameters
    ----------
    evt : asyncio.Event
        Event to by waited on.
    timeout : float
        Time to wait for event set.

    Returns
    -------
    timedout : bool
        True if event timed out, else False.

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(evt, asyncio.Event):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid evt.')
    if not isinstance(timeout, pint.Quantity):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid timeout.')
    pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: Wait for {evt}')
    try:
        timedout = False
        await asyncio.wait_for(evt.wait(), timeout.to_base_units().magnitude)
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {evt} was set.')
    except asyncio.TimeoutError:
        timedout = True
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {evt} timed out.')
    evt.clear()
    return timedout


class AsynBase(Base):
    """
    Base class for derived classes in PyAcdaq supporting asyncio.

    It provides basic attributes and methods for:
    - asyncio task for handling of commands received by queue,
    - asyncio task for periodic actions,
    - asyncio task to subscribe to MQTT topics,
    - publish MQTT topics.
    """

    def __init__(self,
                 name: str,
                 mqtt_client: aiomqtt.Client,
                 top_level_topic: str
                 ) -> None:
        """
        Initialize this instance.

        Parameters
        ----------
        name : str
            Name of instance.
        mqtt_client : aiomqtt.Client
            mqtt client connection.
        top_level_topic : str
            Name of top_level_topic to be uses as prefix.

        Returns
        -------
        None
        """
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        if not isinstance(name, str):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid name.')
        if not isinstance(mqtt_client, aiomqtt.Client):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid mqtt_client.')
        if not isinstance(top_level_topic, str):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid parameter.')
        super().__init__(name)
        self._mqtt_client = mqtt_client
        self._top_level_topic = top_level_topic
        self._command_queue: asyncio.Queue = asyncio.Queue()
        self._command_task: asyncio.Task | None = None
        self._periodic_active: bool = False
        self._periodic_counter: int = -1
        self._periodic_interval: pint.Quantity = -1. * ureg.s
        self._periodic_wakeup: asyncio.Event = asyncio.Event()
        self._periodic_task: asyncio.Task | None = None
        self._subscribtions: set[str] = set()
        self._subscribe_task: asyncio.Task | None = None
        self._periodic_time = 0
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} initialized.')

    def __del__(self) -> None:
        """Finalize this instance."""
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{fn}: {self._name} deleted.')

    # Private methods
    async def __command_loop(self) -> None:
        """Handle commands received from queue in derived classes override method."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Handle commands received from queue.')
        while True:
            pyacdaq_logger.debug('{tn}.{cn}.{fn}: {self._name} wait for queue.')
            q_item: list[Any] = await self._command_queue.get()
            pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Command received. {q_item}')
            await self._handle_command(q_item)  # Call override method of derived class

    async def __periodic_loop(self, periodic_interval: pint.Quantity = -1. * ureg.s) -> None:
        """Perform periodic actions in derived classes override method.."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        if not isinstance(periodic_interval, pint.Quantity):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid periodic_interval.')
        self._periodic_interval = periodic_interval
        begin_msg: str = 'Start performing periodic actions.'
        end_msg: str = 'Stop performing periodic actions.'
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} {begin_msg}')
        pyacdaq_logger.info(f'{tn}.{cn}.{fn}: {self._name} Periodic task started with {self._periodic_interval=}')
        self._periodic_active = True
        self._periodic_counter = 0
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/log_msg', payload=begin_msg, retain=True)
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/periodic_active', payload=self._periodic_active, retain=True)
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/periodic_counter', payload=self._periodic_counter, retain=True)
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/periodic_interval', payload=self._periodic_interval.to_base_units().magnitude, retain=True)
        await self._periodic_start()  # Call override method of derived class
        try:
            while True:
                t_begin: float = time.time()
                await self._periodic_action()  # Call override method of derived class
                t_delta: float = time.time() - t_begin
                self._periodic_counter += 1
                await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/periodic_counter', payload=self._periodic_counter, retain=True)
                await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/periodic_time', payload=t_delta, retain=True)
                await event_wait(self._periodic_wakeup, max(0. * ureg.s, self._periodic_interval - t_delta * ureg.s))
        finally:
            await self._periodic_stop()  # Call override method of derived class
            self._periodic_active = False
            self._periodic_interval = -1. * ureg.s
            await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/periodic_active', payload=self._periodic_active, retain=False)
            await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/periodic_counter', payload=self._periodic_counter, retain=True)
            await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/periodic_interval', payload=self._periodic_interval.to_base_units().magnitude, retain=True)
            await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/log_msg', payload=end_msg, retain=True)
            pyacdaq_logger.info(f'{tn}.{cn}.{fn}: {self._name} Periodic task stopped.')
            pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} {end_msg}')

    async def __publish_all_topics(self) -> None:
        """Publish topics as defined in derived class override method."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Publish topics.')
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/log_msg', payload='Publish topics.', retain=True)
        await self._publish_topics()  # Call override method of derived class
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Publish topics done.')

    async def __subscriber_loop(self) -> None:
        """Subscribe topics as defined in derived class override method."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Subscribe topics.')
        self._subscribtions = await self._subscribe_topics()
        for topic in self._subscribtions:
            await self._mqtt_client.subscribe(topic)
        try:
            async for message in self._mqtt_client.messages:
                pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} {message.topic=}:{message.payload=!r}')
                payload: str = message.payload.decode('utf-8')
                topic_levels: list[str] = re.split('/', str(message.topic))
                await self._handle_subscriptions(topic_levels, payload)  # Call override method of derived class
        finally:
            pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Unubscribe topics.')
            for set_topic in self._subscribtions:
                await self._mqtt_client.unsubscribe(set_topic)

    # Protected methods
    @abstractmethod
    def _command_help(self) -> str:
        """Return help text for command line interface as defined in derived class override method."""
        help_text: str = \
            '  periodic_interval interval:pint.Quantity[s]\n'\
            '  periodic_start interval:pint.Quantity[s]\n'\
            '  periodic_stop\n'
        return help_text

    async def _handle_command(self, q_item: list[Any]) -> None:
        """Handle command received from queue."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        if not isinstance(q_item, list):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid q_item.')
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Handle command: {q_item}')
        match q_item[0]:
            case 'periodic_interval':
                try:
                    await self.set_periodic_interval(q_item[1])
                except ValueError as ex:
                    pyacdaq_logger.warning(f'{tn}.{cn}.{fn}: {self._name} Command periodic_interval ignored. {ex=}')
            case 'periodic_start':
                try:
                    await self.start_periodic(q_item[1])
                except ValueError as ex:
                    pyacdaq_logger.warning(f'{tn}.{cn}.{fn}: {self._name} Command periodic_start ignored. {ex=}')
            case 'periodic_stop':
                await self.stop_periodic()
            case _:
                # await super()._handle_command(q_item)
                pyacdaq_logger.warning(f'{tn}.{cn}.{fn}: {self._name} Invalid command received. {q_item}')

    async def _handle_subscriptions(self, topic_levels: list[str], payload: str) -> None:
        """Handle subscribed topics."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        if not isinstance(topic_levels, list):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid topic_levels.')
        if not isinstance(payload, str):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid payload.')
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Handle subscribed topics.')
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} {topic_levels=}:{payload=!r}')
        match topic_levels[1]:
            case _ if topic_levels[1] == self._name:
                match topic_levels[2]:
                    case _ if topic_levels[2] == 'set':
                        match topic_levels[3]:
                            case _ if topic_levels[3] == 'periodic_interval':
                                try:
                                    await self._command_queue.put(['periodic_interval', float(payload) * ureg.s])
                                except ValueError:
                                    pyacdaq_logger.warning(f'{tn}.{cn}.{fn}: {self._name} Periodic_interval with {payload=} ignored')
                                    await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/log_msg', payload=f'Periodic_interval with {payload=} ignored', retain=True)
                            case _ if topic_levels[3] == 'periodic_start':
                                try:
                                    await self._command_queue.put(['periodic_start', float(payload) * ureg.s])
                                except ValueError:
                                    pyacdaq_logger.warning(f'{tn}.{cn}.{fn}: {self._name} Periodic_interval with {payload=} ignored')
                                    await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/log_msg', payload=f'Periodic_interval with {payload=} ignored', retain=True)
                            case _ if topic_levels[3] == 'periodic_stop':
                                await self._command_queue.put(['periodic_stop'])
                            case _:
                                # await super()._handle_subscriptions(topic_levels, payload)
                                pass  # match topic_level[3]:
                    case _:
                        # await super()._handle_subscriptions(topic_levels, payload)
                        pass  # match topic_level[2]:
            case _:
                # await super()._handle_subscriptions(topic_levels, payload)
                pass  # match topic_level[1]:

    @abstractmethod
    async def _periodic_action(self) -> None:
        """Implement periodic actions in derived class override method."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} {self._periodic_counter=} Performing periodic actions.')

    @abstractmethod
    async def _periodic_start(self) -> None:
        """Implement periodic start actions in derived class override method."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} {self._periodic_counter=} Performing periodic start actions.')
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/log_msg', payload=f'{self._periodic_counter=} Performing periodic start actions.', retain=True)

    @abstractmethod
    async def _periodic_stop(self) -> None:
        """Implement periodic stop actions in derived class override method."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} {self._periodic_counter=} Performing periodic stop actions.')
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/log_msg', payload=f'{self._periodic_counter=} Performing periodic stop actions.', retain=True)

    @abstractmethod
    async def _publish_topics(self) -> None:
        """Publish topics in derived class override method."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Publish topics.')
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/periodic_active', payload=self._periodic_active, retain=True)
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/set/periodic_interval', payload=self._periodic_interval.to_base_units().magnitude, retain=False)
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/set/periodic_start', payload=self._periodic_interval.to_base_units().magnitude, retain=False)
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/set/periodic_stop', payload='NA', retain=False)

    @abstractmethod
    async def _start(self, subscribe_task: bool = False, periodic_interval: pint.Quantity = -1. * ureg.s) -> None:
        """
        Perform start actions in derived class override method.

        Parameters
        ----------
        subscribe: bool, optional
            Start subcriber task if subscribe is True. The default is False.
        periodic_interval: pint.Quantity, optional
            Start periodic task if greater or equal 0. The default is -1s.

        Returns
        -------
        None
        """
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        if not isinstance(subscribe_task, bool):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid subscribe_task.')
        if not isinstance(periodic_interval, pint.Quantity):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid periodic_interval.')
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Perform start actions.')
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/sw_revision', payload=self.sw_revision(), retain=True)
        await self.__publish_all_topics()
        self._command_task = asyncio.create_task(self.__command_loop())
        self._subscribe_task = asyncio.create_task(self.__subscriber_loop()) if subscribe_task else None
        await self.start_periodic(periodic_interval)
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Performing start actions done.')

    @abstractmethod
    async def _stop(self) -> None:
        """
        Perform stop actions in derived class override method.

        - Stop periodic task.
        - Stop subcriber task.
        - Stop command task.

        Returns
        -------
        None
        """
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Perform stop actions.')
        if self._periodic_task:
            self._periodic_task.cancel()
        if self._subscribe_task:
            self._subscribe_task.cancel()
        if self._command_task:
            self._command_task.cancel()
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Performing stop actions done.')

    @abstractmethod
    async def _subscribe_topics(self) -> set[str]:
        """
        Define topics to subscribe in derived class override method.

        Returns
        -------
        set[str]
            Topics to subscribe.
        """
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Subscribe topics.')
        topics: list[str] = {
            f'{self._top_level_topic}/{self._name}/set/periodic_interval',
            f'{self._top_level_topic}/{self._name}/set/periodic_start',
            f'{self._top_level_topic}/{self._name}/set/periodic_stop'
        }
        return topics

    @override
    def _sw_revision(self) -> str:
        """
        Revision of this class inclusive parents.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        string
            Software version
        """
        return 'AsyncBase_0.0.0.1' + '|' + super()._sw_revision()

    # Public methods
    def command_help(self) -> str:
        """Return help text for command line interface."""
        return self._name + '\n' + self._command_help()

    async def start(self, subscribe_task: bool = False, periodic_interval: pint.Quantity = -1. * ureg.s) -> None:
        """Perform start actions."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        if not isinstance(subscribe_task, bool):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid subscribe_task.')
        if not isinstance(periodic_interval, pint.Quantity):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid periodic_interval.')
        begin_msg: str = 'Perform start actions.'
        end_msg: str = 'Performing start actions done.'
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} {begin_msg}')
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/log_msg', payload=begin_msg, retain=True)
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/sw_revision', payload=self.sw_revision(), retain=True)
        await self._start(subscribe_task=subscribe_task, periodic_interval=periodic_interval)  # Call protected overide method.
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/log_msg', payload=end_msg, retain=True)
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} {end_msg}')

    async def start_periodic(self, periodic_interval: pint.Quantity = -1. * ureg.s) -> None:
        """Start periodic task."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        if not isinstance(periodic_interval, pint.Quantity):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid periodic_interval.')
        begin_msg: str = 'Start periodic task.'
        end_msg: str = 'Start periodic task done.'
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} {begin_msg}')
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/log_msg', payload=begin_msg, retain=True)
        if not self._periodic_task:
            self._periodic_task = asyncio.create_task(self.__periodic_loop(periodic_interval)) if periodic_interval >= 0. * ureg.s else None
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/log_msg', payload=end_msg, retain=True)
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} {end_msg}')

    async def stop(self) -> None:
        """Perform stop actions."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        begin_msg: str = 'Perform stop actions.'
        end_msg: str = 'Performing stop actions done.'
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} {begin_msg}')
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/log_msg', payload=begin_msg, retain=True)
        await self._stop()
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/log_msg', payload=end_msg, retain=True)
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} {end_msg}')

    async def stop_periodic(self) -> None:
        """Stop periodic task."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        begin_msg: str = 'Stop periodic task.'
        end_msg: str = 'Stop periodic task done.'
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} {begin_msg}')
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/log_msg', payload=begin_msg, retain=True)
        if self._periodic_task:
            while not self._periodic_task.done():
                self._periodic_task.cancel()
                await asyncio.sleep(.1)
        self._periodic_task = None
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/log_msg', payload=end_msg, retain=True)
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} {end_msg}')

    # Getter
    def get_command_queue(self) -> asyncio.Queue:
        """Get property command_queue."""
        return self._command_queue

    def get_periodic_active(self) -> float:
        """Get property periodic_active."""
        return self._periodic_active

    def get_periodic_counter(self) -> float:
        """Get property periodic_counter."""
        return self._periodic_counter

    def get_periodic_interval(self) -> pint.Quantity:
        """Get property periodic_interval."""
        return self._periodic_interval

    def get_periodic_time(self) -> pint.Quantity:
        """Get property periodic_time."""
        return self._periodic_time * ureg.s

    # Setter
    async def set_periodic_interval(self, periodic_interval: pint.Quantity) -> None:
        """Set property periodic_interval."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        if not isinstance(periodic_interval, pint.Quantity):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid periodic_interval.')
        self._periodic_interval = max(0. * ureg.s, periodic_interval)
        pyacdaq_logger.info(f'{tn}.{cn}.{fn}: {self._name} {self._periodic_interval=}')
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/periodic_interval', payload=self._periodic_interval.to_base_units().magnitude, retain=False)
        self._periodic_wakeup.set()

    # Properties
    CommandQueue = property(get_command_queue)
    PeriodicActive = property(get_periodic_active)
    PeriodicCounter = property(get_periodic_counter)
    PeriodicInterval = property(get_periodic_interval)
    PeriodicInterval = property(get_periodic_time)


async def do_something(mqtt_client: aiomqtt.Client, top_level_topic: str):
    """Do something within mqtt client context."""
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    if not isinstance(top_level_topic, str):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid top_level_topic.')
    try:
        print(f'{tn}.{cn}.{fn}: Instantiate and start.')
        ab_0: AsynBase = AsynBase('asynBase_0', mqtt_client, top_level_topic)
        print(f'{tn}.{cn}.{fn}: {ab_0.Name=}, {str(ab_0.Birthday)=}, {ab_0.SWRevision=}, {ab_0.PeriodicInterval=}')
        print(f'{tn}.{cn}.{fn}: Command_help\n{ab_0.command_help()}')
        await ab_0.start(subscribe_task=True, periodic_interval=-1. * ureg.s)
        if False:
            await asyncio.sleep(10.)
            print(f'{tn}.{cn}.{fn}: Use methods for manipulation.')
            print(f'{tn}.{cn}.{fn}: {ab_0.PeriodicActive=} {ab_0.PeriodicInterval=} {ab_0.PeriodicCounter=}')
            await ab_0.start_periodic(1. * ureg.s)
            await asyncio.sleep(1.)
            print(f'{tn}.{cn}.{fn}: {ab_0.PeriodicActive=} {ab_0.PeriodicInterval=} {ab_0.PeriodicCounter=}')
            await asyncio.sleep(10.)
            print(f'{tn}.{cn}.{fn}: {ab_0.PeriodicActive=} {ab_0.PeriodicInterval=} {ab_0.PeriodicCounter=}')
            await ab_0.set_periodic_interval(2. * ureg.s)
            await asyncio.sleep(10.)
            print(f'{tn}.{cn}.{fn}: {ab_0.PeriodicActive=} {ab_0.PeriodicInterval=} {ab_0.PeriodicCounter=}')
            await ab_0.stop_periodic()
            await asyncio.sleep(10.)
            print(f'{tn}.{cn}.{fn}: {ab_0.PeriodicActive=} {ab_0.PeriodicInterval=} {ab_0.PeriodicCounter=}')
            print(f'{tn}.{cn}.{fn}: Use queue for manipulation.')
            ab_0_cq: asyncio.Queue = ab_0.CommandQueue
            print(f'{tn}.{cn}.{fn}: {ab_0.PeriodicActive=} {ab_0.PeriodicInterval=} {ab_0.PeriodicCounter=}')
            await ab_0_cq.put(['periodic_start', 1. * ureg.s])
            await asyncio.sleep(1.)
            print(f'{tn}.{cn}.{fn}: {ab_0.PeriodicActive=} {ab_0.PeriodicInterval=} {ab_0.PeriodicCounter=}')
            await asyncio.sleep(10.)
            print(f'{tn}.{cn}.{fn}: {ab_0.PeriodicActive=} {ab_0.PeriodicInterval=} {ab_0.PeriodicCounter=}')
            await ab_0_cq.put(['periodic_interval', 2. * ureg.s])
            await asyncio.sleep(10.)
            print(f'{tn}.{cn}.{fn}: {ab_0.PeriodicActive=} {ab_0.PeriodicInterval=} {ab_0.PeriodicCounter=}')
            await ab_0_cq.put(['periodic_stop'])
            await asyncio.sleep(10.)
            print(f'{tn}.{cn}.{fn}: {ab_0.PeriodicActive=} {ab_0.PeriodicInterval=} {ab_0.PeriodicCounter=}')
        print('Waiting for topic changes...')
        while True:
            await asyncio.sleep(1.)
    finally:
        print(f'{tn}.{cn}.{fn}: Stop and delete')
        try:
            if ab_0:
                await ab_0.stop()
        finally:
            if ab_0:
                del ab_0


async def main() -> None:
    """Implement yout test code here."""
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    parser = argparse.ArgumentParser(prog=os.path.basename(__file__),
                                     description='This is the async_base test.',
                                     epilog='Default command: python -m pyacdaq.asyn.asyn_base -b localhost -p 1883')
    parser.add_argument('-b', '--broker', dest='broker', required=False, default='localhost', help='MQTT Broker Node (Name or IP)')
    # parser.add_argument('-b', '--broker', dest='broker', required=True, default=None, help='MQTT Broker Node (Name or IP)')
    parser.add_argument('-p', '--port', dest='port', type=int, default=1883, help='MQTT Broker port (Default:1883)')
    parser.add_argument('--mqtt_user', dest='mqtt_user', default=None, help='MQTT Broker User Name')
    parser.add_argument('--top_level_topic', dest='top_level_topic', default='PyACDAQ', help='MQTT top-level topic. Default is "PyACDAQ")')
    args = parser.parse_args()
    while True:
        answer: str = input('THIS IS BETA SOFTWARE! You will use it on your own risk!\nDo you want to continue (yes or default: no)? ')
        if re.match('yes', answer):
            break
        if len(answer) == 0 or re.match('no', answer):
            sys.exit(1)
    mqtt_password: str | None = None
    if args.mqtt_user:  # but required
        print('Set password for MQTT')
        mqtt_password = getpass.getpass()
        if len(mqtt_password) == 0:
            sys.exit(-2)
    mqtt: dict[str, int | str | None] = {'broker': args.broker,
                                         'port': args.port,
                                         'user': args.mqtt_user,
                                         'password': mqtt_password}
    try:
        async with aiomqtt.Client(hostname=mqtt['broker'],
                                  port=mqtt['port'],
                                  username=mqtt['user'],
                                  password=mqtt['password'],
                                  will=aiomqtt.Will(topic=args.top_level_topic, payload='disconnected', retain=True),
                                  clean_session=True
                                  ) as mqtt_client:
            await mqtt_client.publish(args.top_level_topic, payload='online', retain=True)
            await mqtt_client.publish(args.top_level_topic + '/log_msg', payload='', retain=True)
            await do_something(mqtt_client, args.top_level_topic)
            await mqtt_client.publish(args.top_level_topic, payload='offline', retain=True)
    except Exception as ex:
        pyacdaq_logger.exception(f'{tn}.{cn}.{fn}: Stopping due to exception: {ex}')


if __name__ == '__main__':
    print(os.path.basename(__file__) + "\n\
Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme\n")
    logging.basicConfig(filename='./pyacdaq.log', filemode='w', encoding='utf-8', level=logging.DEBUG, style='{', format='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    pyacdaq_logger.setLevel(logging.DEBUG)
    nest_asyncio.apply()
    # Change to the "Selector" event loop if platform is Windows
    if sys.platform.lower() == "win32" or os.name.lower() == "nt":
        from asyncio import set_event_loop_policy, WindowsSelectorEventLoopPolicy
        set_event_loop_policy(WindowsSelectorEventLoopPolicy())
    # Run your async application as usual
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print('Script interrupted by <Ctr+C>')
    except asyncio.exceptions.CancelledError:
        pass
    except BaseException as e:
        # print(f'__main__: BaseException cought. {type(e)} {e}')
        print(f'__main__: BaseException cought. {type(e)}')
        pass
    logging.shutdown()
    print(__name__, 'Done.')
