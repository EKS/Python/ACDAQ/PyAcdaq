#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This is a handler publishing logging record messages to MQTT topic.

Lizenziert unter EUPL V. 1.2

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
import logging  # https://docs.python.org/3/howto/logging.html
import logging.handlers
import os
import paho.mqtt.client as mqtt
from platformdirs import PlatformDirs
import re
import time
import warnings

pyacdaq_logger = logging.getLogger('pyacdaq').addHandler(logging.NullHandler())


class MQTTLogHandler(logging.Handler):
    """A handler class which writes logging records, appropriately formatted, to a MQTT broker topic."""

    def __init__(self,
                 broker: str,
                 topic: str,
                 qos: int = 2,
                 retain: bool = False,
                 port: int = 1883,
                 client_id: str = '',
                 clean_session: bool = True,
                 username: str | None = None,
                 password: str | None = None,
                 keepalive: int = 60,
                 will: str | None = None,
                 auth: str | None = None,
                 tls=None,
                 protocol=mqtt.MQTTv31,
                 transport: str = 'tcp') -> None:
        logging.Handler.__init__(self)
        self._topic = topic
        self._qos = qos
        self._retain = retain
        self._broker = broker
        self._port = port
        self._client_id = client_id
        self._clean_session = clean_session
        self._username = username
        self._password = password
        self._keepalive = keepalive
        self._will = will
        self._auth = auth
        self._tls = tls
        self._protocol = protocol
        self._transport = transport
        self.__connect_broker()

    def __connect_broker(self):
        """Connect MQTT client to broker."""
        try:
            self._client = mqtt.Client(self._client_id,
                                       self._clean_session
                                       )
            self._client.connected_flag = False
            if self._username is not None and self._password is not None:
                self._client.username_pw_set(username=self._username, password=self._password)
            self._client.will_set(self._client_id, self._will, 1, False)
            # logger.debug('MqttHandler Connecting client ' + self._client_id + ' to broker ' + self._broker)
            self._client.loop_start()
            self._client.connect(self._broker, self._port, keepalive=60, bind_address='')
        except BaseException as e:
            # logger.exception('MqttHandler Exception catched! ' + str(e))
            print('MqttHandler Exception catched! ' + str(e))
            self._client.connected_flag = False
        pass

    def __disconnect_broker(self):
        """Disconnect MQTT client from broker."""
        try:
            logger.debug('MqttLogHandler __disconnect_broker() Publishing: "disconnected"')
            rc, mid = self._client.publish(self._client_id, "disconnected")
            logger.debug('MqttLogHandler __disconnect_broker() Publishing: "disconnected" returned rc=' + str(rc) + ' mid=' + str(mid))
            logger.debug('MqttLogHandler __disconnect_broker() Disonnecting from broker ' + self._broker)
            self._client.disconnect()
            time.sleep(1)
            self._client.loop_stop(force=True)
        except BaseException as e:
            logger.exception(" MqttHandler __disconnect_broker() Exception catched when discopnnection from MQTT broker! ", e)
        finally:
            self._client.loop_stop(force=True)
        pass

    def emit(self, record):
        """Publish a single formatted logging record to a broker."""
        msg = self.format(record)
        rc, mid = self._client.publish(self._topic, msg, self._qos, self._retain)
        # print('MqttLogHandler emit() Publishing sub_topic: ' + self._topic + ' returned rc=' + str(rc) + ' mid=' + str(mid))


def configure_logging_handler(app_name: str,
                              loggers: tuple[logging.logger],
                              formatter: logging.Formatter,
                              severity: dict[str, int | None] = {'console': logging.WARNING, 'file': logging.WARNING, 'mqtt': None, 'syslog': None},
                              mqtt: dict[str, str] = {'broker': 'localhost', 'topic': 'log-msg', 'user': '', 'password': ''}
                              ) -> None:
    """
    Configure logging handlers.

    Parameters
    ----------
    app_name : string
        Name of calling python module.
    loggers : tuple[logging.logger]
        Tuple of loggers.
    formatter : logging.Formatter
        logging.Formatter defined by caller, e.g. logging.Formatter(style='{', fmt='{asctime} [{levelname:8}] {module} {funcName} {lineno} {message}')
    severity : dict[str, int | None]
        Use handler severity for logging.
        Default: severity={'console': logging.WARNING, 'file': logging.WARNING, 'mqtt': None, 'syslog': None}
    mqtt : dict[str, str]
        Parameters necessary to connect to a MQTT broker.
        E.g. mqtt={'broker': 'node', 'topic': 'log_msg', 'user': 'user_name', 'password': 'user_password'}
        To be extended for more parameters.

    Returns
    -------
    None.

    """
    # print(f'OS Name: {os.name}')
    # Get OS user name
    match os.name:
        case 'nt':
            user_name = os.getlogin()
            pass
        case 'posix':
            import pwd
            user_name = pwd.getpwuid(os.geteuid()).pw_name
            pass
        case _:
            user_name = os.getlogin()
            pass
    # print(f'OS User: {user_name}')
    dirs: list[str] = PlatformDirs(app_name, user_name)
    log_filename: str = os.path.join(dirs.user_log_dir, app_name + '.log')
    if not os.path.exists(os.path.dirname(log_filename)):
        os.makedirs(os.path.dirname(log_filename), exist_ok=True)
    print('Log filename:', log_filename)
    if severity['console']:  # Create and configure console handler
        console_log_handler: logging.Handler = logging.StreamHandler()
        console_log_handler.setLevel(severity['console'])
        console_log_handler.setFormatter(formatter)
        for logger in loggers:
            logger.addHandler(console_log_handler)
    if severity['file']:  # Create and configure file handler
        file_log_handler: logging.Handler = logging.FileHandler(log_filename, encoding='utf-8', mode='w')  # mode='a' or 'w'
        file_log_handler.setLevel(severity['file'])
        file_log_handler.setFormatter(formatter)
        for logger in loggers:
            logger.addHandler(file_log_handler)
    if severity['mqtt']:  # Create and configure MQTT handler
        if not mqtt['topic']:
            logger_topic: str = app_name + '/log_msg'
        else:
            logger_topic = mqtt['topic']
        mqtt_log_handler: logging.Handler = MQTTLogHandler(mqtt['broker'], logger_topic, client_id=app_name + '_logger', username=mqtt['user'], password=mqtt['password'])
        mqtt_log_handler.setLevel(severity['mqtt'])
        mqtt_log_handler.setFormatter(formatter)
        for logger in loggers:
            logger.addHandler(mqtt_log_handler)
    if severity['syslog']:  # Create and configure syslog handler
        syslog_log_handler: logging.Handler = logging.handlers.SysLogHandler(facility=logging.handlers.SysLogHandler.LOG_DAEMON, address='/dev/log')
        syslog_log_handler.setLevel(severity['syslog'])
        syslog_log_handler.setFormatter(formatter)
        for logger in loggers:
            logger.addHandler(syslog_log_handler)
    pass


if __name__ == '__main__':
    print("logger.py\n\
Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme")
    # mp.freeze_support()
    app_name: str = re.split('.py', os.path.basename(__file__))[0]
    mqtt_broker: str = 'localhost'  # fill in with your MQTT broker node
    print(f'OS Name: {os.name}')
    # Get OS user name
    match os.name:
        case 'nt':
            user_name: str = os.getlogin()
            pass
        case 'posix':
            import pwd
            user_name = pwd.getpwuid(os.geteuid()).pw_name
            pass
        case _:
            user_name = os.getlogin()
            pass
    # print(f'OS User: {user_name}')
    print('Configuring logging...')
    # Define logger to be used
    # logging.basicConfig(  # filename='example.log', filemode='w', encoding='utf-8', level=logging.INFO, style='{', format='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    logger = logging.getLogger('App-Logger')
    logger.setLevel(logging.DEBUG)
    pyacdaq_logger: logging.Logger = logging.getLogger('pyacdaq')
    pyacdaq_logger.setLevel(logging.DEBUG)
    # Define logging formatter
    logging_formatter: logging.Formatter = logging.Formatter(style='{', fmt='{asctime} [{levelname:8}] {module} {funcName} {lineno} {message}')
    # logging_formatter = logging.Formatter(style='{', fmt='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    configure_logging_handler(app_name, (logger, pyacdaq_logger), logging_formatter, severity={'console': logging.INFO, 'file': logging.DEBUG, 'mqtt': logging.INFO, 'syslog': logging.WARNING}, mqtt={'broker': mqtt_broker, 'topic': app_name + '/log_msg', 'user': '', 'password': ''})

    # Issue log messages
    logger.debug('App-Logger: Debug message.')
    time.sleep(1)
    logger.info('App-Logger: Info message.')
    time.sleep(1)
    logger.warning('App-Logger: Warning message.')
    time.sleep(1)
    logger.error('App-Logger: Error message.')
    time.sleep(1)
    logger.exception('App-Logger: Exception message.')
    time.sleep(1)
    logger.critical('App-Logger: Critical message.')
    time.sleep(1)

    # Issue module log messages
    pyacdaq_logger.debug('pyacdaq-Logger: Debug message.')
    time.sleep(1)
    pyacdaq_logger.info('pyacdaq-Logger: Info message.')
    time.sleep(1)
    pyacdaq_logger.warning('pyacdaq-Logger: Warning message.')
    time.sleep(1)
    pyacdaq_logger.error('pyacdaq-Logger: Error message.')
    time.sleep(1)
    pyacdaq_logger.exception('pyacdaq-Logger: Exception message.')
    time.sleep(1)
    pyacdaq_logger.critical('pyacdaq-Logger: Critical message.')

    # Issue warnings messages
    warnings.warn('Warning in library avoidable by caller.')
    logging.shutdown()
    print('Done.')
