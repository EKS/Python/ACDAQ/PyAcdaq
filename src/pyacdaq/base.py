#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This module contains the _Base_ class for derived classes in PyAcdaq.

It provides basic attributes like name, date_of_birth, software revision etc.
for objects using unique resources that cannot be simply copied.

Lizenziert unter EUPL V. 1.2

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""

import datetime
import inspect
import logging
import os
from abc import ABC, abstractmethod

pyacdaq_logger = logging.getLogger('pyacdaq')


class Base(ABC):
    """
    Base class for derived classes in PyAcdaq.

    It provides basic attributes like name, date_of_birth, software revision etc.
    for objects using unique resources that cannot be simply copied.

    Attributes
    ----------
    name -- Name of instance
    date_of_birth -- Creation date of instance

    Methods
    -------
    software_revision -- Revision of class.
    """

    def __init__(self, name: str) -> None:
        """
        Initialize this instance.

        Arguments
        ---------
        name: str
            Name of instance
        """
        fn: str = inspect.currentframe().f_code.co_name
        self._name = name
        self._birthday: datetime.datetime = datetime.datetime.now()
        pyacdaq_logger.debug(f'{fn}: {self._name} born at {str(self._birthday)}')

    def __del__(self) -> None:
        """Finalize this instance."""
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{fn}: {self._name} born at {str(self._birthday)} lived for {str((datetime.datetime.now() - self._birthday))}s')

    # Protected methods
    @abstractmethod
    def _sw_revision(self) -> str:
        """Return revision of this class."""
        return 'Base_0.1.0.1'

    # Public methods
    def sw_revision(self) -> str:
        """Return revision of this class."""
        return self._sw_revision()

    # Getter
    def getBirthday(self) -> datetime.datetime:
        """Property Birthday."""
        return self._birthday

    def getName(self) -> str:
        """Property Name."""
        return self._name

    def getSWRevision(self) -> str:
        """Property Software Revision."""
        return self._sw_revision()

    # Properties
    Birthday = property(getBirthday)
    Name = property(getName)
    SWRevision = property(getSWRevision)


def main():
    """Implement yout test code here."""
    fn: str = inspect.currentframe().f_code.co_name
    base = Base('Base')
    print(f'{fn}, {base.Name=}, {str(base.Birthday)=}, {base.SWRevision=}')
    del base


if __name__ == '__main__':
    print(os.path.basename(__file__) + "\n\
Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme\n")
    logging.basicConfig(filename='./pyacdaq.log', filemode='w', encoding='utf-8', level=logging.DEBUG, style='{', format='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    pyacdaq_logger.setLevel(logging.DEBUG)
    main()
    print(__name__, 'Done.')
