#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Actor is the base class for active pykka.Actor classes.

Lizenziert unter EUPL V. 1.2

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
if __name__ == '__main__':
    # from acdaq.actor.mqtt import Mqtt as Mqtt
    from .mqtt import Mqtt as Mqtt
    pass
import datetime
import enum
import getpass
import logging
import os
# from platformdirs import PlatformDirs
import pykka
import re
import time
import warnings
import tkinter as tk
from pyacdaq.logger import configure_logging_handler
from .tkguis import ActorTkGui as ActorTkGui

pyacdaq_logger = logging.getLogger('pyacdaq')


class PeriodicCallReason(enum.Enum):
    """Enumeration of reasons for calling Actor._periodic()"""
    Start = enum.auto()
    Do = enum.auto()
    Stop = enum.auto()
    pass


class Actor(pykka.ThreadingActor):
    """
    Actor is the base class for active objects.

    It is designed to perform periodic actions in its own thread.

    Attributes
    ----------
    name : str
        Name of instance
    date_of_birth : datetime
        Creation date of instance. Default is datetime.datetime.now()

    Methods
    -------
    software_revision -- Revision of class.

    """

    def __init__(self, name, start_periodic=False, mqtt_actor=None):
        """
        Initialize this instance.

        Parameters
        ----------
        name : str
            Name of instance.
        start_periodic : bool, optional
            Auto start periodic action if true. Default is False.
        mqtt_actor : Mqtt Proxy, optional
            Proxy to MQTT actor. Default is None.

        Returns
        -------
        None.

        """
        self._name = name
        self._birthday = datetime.datetime.now()
        self._start_periodic = start_periodic
        self._periodic_active = False
        self._periodic_time = False
        self._software_revision = None
        self._actor_subscriptions = ('Set-PeriodicActive', )
        self._mqtt = mqtt_actor
        pyacdaq_logger.debug(self._name + ' Actor born at ' + str(self._birthday) + ' start_periodic=' + str(self._periodic_active))
        super().__init__(name)
        pass

    def __del__(self):
        """Finalize instance."""
        self._periodic_active = False
        pyacdaq_logger.debug(self._name + '  Actor born at ' + str(self._birthday) + ' lived for ' + str((datetime.datetime.now() - self._birthday)))
        pass

    # Private callback methods of pykka.ThreadingActor
    def on_connect(self):
        """
        Called from AcdaqMqtt._on_receive() when message was received from paho.mqtt.client callback message on_connect.

        Perform action on MQTT disconnect here, e.g. publish and subscribe topics.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' Actor on_connect() Received MQTT connected message.')
        self._on_connect()
        pass

    def on_disconnect(self):
        """
        Called from AcdaqMqtt._on_receive() when message was received from paho.mqtt.client callback message on_connect.

        Perform action on MQTT disconnect here.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' AcdaqMqtt _on_disconnect() Received MQTT disconnected message.')
        self._on_disconnect()
        pass

    def on_failure(self, exception_type, exception_value, traceback):
        """
        Hook for doing any cleanup after an unhandled exception is raised, and before the actor stops.

        Returns
        -------
        None.

        """
        pyacdaq_logger.error(self._name + ' Actor on_failed(). Handling exception... ')
        self._periodic_active = False
        self._periodic_counter = 0
        if self._mqtt is not None:
            self._mqtt.publish_topic(self._name + '/PeriodicActive', self._periodic_active, qos=0, retain=True, expand=False)
            self._mqtt.publish_topic(self._name + '/PeriodicCounter', self._periodic_counter, qos=0, retain=True, expand=False)
        self._on_failure(exception_type, exception_value, traceback)
        pass

    def on_message(self, topic, msg):
        """
        Called from AcdaqMqtt._on_receive() when message was received from paho.mqtt.client callback message on_message.

        Perform action on MQTT messages here.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' Actor on_message() Received MQTT topic message.')
        self._on_message(topic, msg)
        pass

    def on_receive(self, message):
        """
        May be implemented for the actor to handle regular non-proxy messages.

        Returns
        -------
        None.

        """
        try:
            pyacdaq_logger.debug(self._name + ' Actor on_receive(): Message received.')
            self._on_receive(message)
        except Exception as e:
            pyacdaq_logger.exception(self._name + ' Actor on_receive(): Exception cought.')
            pyacdaq_logger.exception(self._name + ' Actor on_receive(): Message: ' + str(message))
            pyacdaq_logger.exception(self._name + ' Actor on_receive(): Exception: ' + str(e))
        pass

    def on_start(self):
        """
        Hook for doing any setup that should be done after the actor is started, but before it starts processing messages.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' starting. Perfoming setup... ')
        self._periodic_counter = 0
        self._in_future = self.actor_ref.proxy()
        try:
            if self._mqtt is not None:
                self._mqtt.register_subscriber(self._in_future)
        except Exception as e:
            pyacdaq_logger.exception(self._name + ' Actor on_start(): Exception cought registering self as subsciber.')
            pyacdaq_logger.exception(self._name + ' Actor on_start(): Exception: ' + str(e))
        self._on_start()
        self._sw_revision = self.sw_revision()
        pyacdaq_logger.debug(self._name + ' self._periodic_active=' + str(self._periodic_active))
        if self._start_periodic:
            pyacdaq_logger.info(self._name + ' Auto start periodic action.')
            self._in_future.do_periodic(PeriodicCallReason.Start)
        pass

    def on_stop(self):
        """
        Hook for doing any cleanup that should be done after the actor has processed the last message, and before the actor stops.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' stopping. Perfoming cleanup... ')
        self._periodic_active = False
        if self._mqtt is not None:
            self._mqtt.publish_topic(self._name + '/PeriodicActive', self._periodic_active, qos=0, retain=True, expand=False)
        self._on_stop()
        pass

    # Protected virtual methods defined by this class
    def _on_connect(self):
        """
        Called from _on_receive() when message was received from paho.mqtt.client callback message on_connect.

        Perform action on MQTT disconnect here, e.g. publish and subscribe topics.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' Actor _on_connect() Received MQTT connected message. Publish and subscribe...')
        try:
            if self._mqtt is not None:
                self._mqtt.publish_topic(self._name + '/Name', self._name, qos=0, retain=True, expand=False)
                self._mqtt.publish_topic(self._name + '/Birthday', self._birthday.ctime(), qos=0, retain=True, expand=False)
                self._mqtt.publish_topic(self._name + '/SoftwareRevision', self._software_revision, qos=0, retain=True, expand=False)
                self._mqtt.publish_topic(self._name + '/PeriodicActive', self._periodic_active, qos=0, retain=True, expand=False)
                self._mqtt.publish_topic(self._name + '/PeriodicCounter', self._periodic_counter, qos=0, retain=True, expand=False)
                pyacdaq_logger.debug(self._name + ' Actor _on_connect() Publishing:' + str(self._actor_subscriptions))
                prefixed_topics = ()
                for topic in self._actor_subscriptions:
                    prefixed_topic = self._name + '/' + topic
                    pyacdaq_logger.debug(self._name + ' Actor _on_connect() Publishing:' + str(prefixed_topic))
                    prefixed_topics = prefixed_topics + (prefixed_topic,)
                    self._mqtt.publish_topic(prefixed_topic, '', qos=0, retain=True, expand=False)
                rcs_mids = self._mqtt.subscribe_topics(self._in_future, prefixed_topics, qos=0).get()
                pyacdaq_logger.debug(self._name + ' Actor _on_connect() Subscribing to: ' + str(prefixed_topics) + ' returned rcs, mids = ' + str(rcs_mids))
        except Exception as e:
            pyacdaq_logger.exception(self._name + ' Actor _on_connect(): Exception cought:')
            pyacdaq_logger.exception(self._name + ' Actor _on_connect(): Exception: ' + str(e))
        pass

    def _on_disconnect(self):
        """
        Called from _on_receive() when message was received from paho.mqtt.client callback message on_disconnect.

        Perform action on MQTT disconnect here.

        Virtual protected method. Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' Actor _on_disconnect() Received MQTT disconnected message.')
        pass

    def _on_failure(self, exception_type, exception_value, traceback):
        """
        Hook for doing any cleanup after an unhandled exception is raised, and before the actor stops.

        Virtual protected method. Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        warnings.warn(self._name + ' Actor _on_failure() Method needs to be implemented by derived class.')
        pass

    def _on_message(self, topic, msg):
        """
        Called from AcdaqMqtt._on_receive() when message was received from paho.mqtt.client callback message on_message.

        Perform action on MQTT messages here.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' Actor _on_message() Received MQTT topic.' + topic + ' msg: ' + str(msg))
        sub_topics = re.split('/', topic)
        match sub_topics[2]:
            case 'Set-PeriodicActive':
                if re.match('false', msg.lower()) is not None:
                    pyacdaq_logger.info(self._name + ' Actor _on_message() stop periodic.')
                    self._in_future.periodic_action(start=False)
                elif re.match('true', msg.lower()) is not None:
                    pyacdaq_logger.info(self._name + ' Actor _on_message() start periodic.')
                    self._in_future.periodic_action(start=True)
                else:
                    pyacdaq_logger.debug(self._name + ' Actor _on_message() Set-PeriodicActive ignored due to invalid value, msg: ' + str(msg))
            case _:
                pyacdaq_logger.error(self._name + ' Actor _on_message() unknown topic: ' + topic + ' msg: ' + str(msg))
        pass

    def _on_receive(self, message):
        """
        May be implemented for the actor to handle regular non-proxy messages.

        Virtual protected method. Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        warnings.warn(self._name + ' Actor _on_receive() Method needs to be implemented by derived class.')
        pass

    def _on_start(self):
        """
        Hook for doing any setup that should be done after the actor is started, but before it starts processing messages.

        Virtual protected method. Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        warnings.warn(self._name + ' Actor _on_start() Method needs to be implemented by derived class.')
        pass

    def _on_stop(self):
        """
        Hook for doing any cleanup that should be done after the actor has processed the last message, and before the actor stops.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' Actor _on_stop()')
        try:
            if self._mqtt is not None:
                prefixed_topics = ()
                pyacdaq_logger.debug(self._name + ' Actor _on_stop() Unsubscribing: ' + str(self._actor_subscriptions))
                for topic in self._actor_subscriptions:
                    prefixed_topic = self._name + '/' + topic
                    prefixed_topics = prefixed_topics + (prefixed_topic,)
                pyacdaq_logger.debug(self._name + ' Actor _on_stop() Unsubscribing from: ' + str(prefixed_topics))
                self._mqtt.unsubscribe_topics(self, prefixed_topics)
            self._mqtt.unregister_subscriber(self._in_future)
        except Exception as e:
            pyacdaq_logger.exception(self._name + ' Actor _on_stop(): Exception cought:')
            pyacdaq_logger.exception(self._name + ' Actor _on_stop(): Exception: ' + str(e))
        pass

    def _periodic(self, reason):
        """
        Implementation of periodic actions.

        Virtual protected method. Override with concrete child class implementation.

        Parameters
        ----------
        reason : PeriodicCallReason
            Reason for calling this method.

        Returns
        -------
        None.

        """
        warnings.warn(self._name + ' Actor _periodic() Method needs to be implemented by derived class.')
        match reason:
            case PeriodicCallReason.Start:
                pass
            case PeriodicCallReason.Do:
                time.sleep(1)
                pass
            case PeriodicCallReason.Stop:
                pass
        pass

    def _sw_revision(self):
        """
        Return revision of this class inclusive parents.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        string
            Software version

        """
        pyacdaq_logger.debug(self._name + ' Actor _sw_revision()')
        return "Actor_0.1.0.0"

    # Public methds
    def do_periodic(self, reason):
        """
        Method executes periodic actions and calls protected _periodic() overrides.

        Parameters
        ----------
        reason : PeriodicCallReason
            Reason for calling this method.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' Actor do_periodic() Begin iteration...')
        match reason:
            case PeriodicCallReason.Start:
                if not self._periodic_active:
                    self._periodic_active = True
                    if self._mqtt is not None:
                        self._mqtt.publish_topic(self._name + '/PeriodicActive', self._periodic_active, qos=0, retain=True, expand=False)
                    self._periodic_counter = 0
                    pyacdaq_logger.debug(self._name + ' Actor do_periodic() Begin _periodic iteration:' + str(self._periodic_counter) + ' ...')
                    if self._mqtt is not None:
                        self._mqtt.publish_topic(self._name + '/PeriodicCounter', self._periodic_counter, qos=0, retain=True, expand=False)
                    self._periodic(reason)
                    pyacdaq_logger.debug(self._name + ' Actor do_periodic() Request next do_periodic.')
                    self._in_future.do_periodic(PeriodicCallReason.Do)
                    self._periodic_time = time.time()
                else:
                    pyacdaq_logger.warning(self._name + ' Actor do_periodic() Periodic is already active.')
                pass
            case PeriodicCallReason.Do:
                if self._periodic_active:
                    self._periodic_counter += 1
                    pyacdaq_logger.debug(self._name + ' Actor do_periodic() Next _periodic iteration:' + str(self._periodic_counter) + ' ...')
                    if self._mqtt is not None:
                        self._mqtt.publish_topic(self._name + '/PeriodicCounter', self._periodic_counter, qos=0, retain=True, expand=False)
                    t0 = time.time()
                    self._periodic(reason)
                    if self._mqtt is not None:
                        self._mqtt.publish_topic(self._name + '/PeriodicTime', time.time() - t0, qos=0, retain=True, expand=False)
                        self._mqtt.publish_topic(self._name + '/PeriodicInterval', time.time() - self._periodic_time, qos=0, retain=True, expand=False)
                    pyacdaq_logger.debug(self._name + ' Actor do_periodic() Request next do_periodic.')
                    self._in_future.do_periodic(reason)
                    self._periodic_time = time.time()
                pass
            case PeriodicCallReason.Stop:
                if self._periodic_active:
                    self._periodic(reason)
                    pyacdaq_logger.debug(self._name + ' Actor do_periodic() Periodic not active.')
                    self._periodic_active = False
                    if self._mqtt is not None:
                        self._mqtt.publish_topic(self._name + '/PeriodicActive', self._periodic_active, qos=0, retain=True, expand=False)
                else:
                    pyacdaq_logger.warning(self._name + ' Actor do_periodic() Periodic is not active.')
                pass
        pyacdaq_logger.debug(self._name + ' Actor do_periodic() Iteration done.')
        pass

    def periodic_action(self, start=False):
        """
        Activate or deactivate the automatic execution of periodic actions.

        Parameters
        ----------
        start : bool, optional
            Start periodic execution of periodic action if True else stop.
            The default is False.

        Returns
        -------
        None.

        """
        if start and not self._periodic_active:
            pyacdaq_logger.debug(self._name + ' Actor periodic_action() Request to start periodic action received.')
            self._in_future.do_periodic(PeriodicCallReason.Start)
        elif start and self._periodic_active:
            pyacdaq_logger.debug(self._name + ' Actor periodic_action() Request to start periodic action received. Periodic action is aready active.')
        else:
            pyacdaq_logger.debug(self._name + ' Actor periodic_action() Request to stop periodic action received.')
            self._in_future.do_periodic(PeriodicCallReason.Stop)
        pass

    def sw_revision(self):
        """
        Return revision of this class inclusive parents.

        Virtual protected method.
        Override with child class implementation.

        Returns
        -------
        string
            Software version

        """
        pyacdaq_logger.debug(self._name + ' Actor sw_revision()')
        self._software_revision = self._sw_revision()
        if self._mqtt is not None:
            self._mqtt.publish_topic(self._name + '/SoftwareRevision', self._software_revision, qos=0, retain=True, expand=False)
            pass
        return self._software_revision

    # Getter
    def getBirthday(self):
        """Property Birthday."""
        return self._birthday

    def getName(self):
        """Property Name."""
        return self._name

    def getPeriodicStatus(self):
        """Property Name."""
        return self._periodic_active

    def getSWRevision(self):
        """Property Software Revision."""
        return self._software_revision

    pass  # End of class definition


class ActorGui(Actor):
    """
    Base class for derived GUI classes.

    It provides a basic tkinter based GUI for Actor.
    """

    def __init__(self, name, start_periodic=False, mqtt=None, associated_actor_name=None, tkgui=None):
        """
        Initialize this instance.

        Parameters
        ----------
        name : str
            Name of instance.
        start_periodic : bool, optional
            Auto start periodic action if true. Default is False.
        mqtt_actor : Mqtt Proxy, optional
            Proxy to MQTT actor. Default is None.
        associated_actor_name : str, optional
            Name of associated actor. The default is None.
        tkgui : ActorTkGui, optional
            ActorTkGui instance. The default is None.

        Returns
        -------
        None.

        """
        super().__init__(name, start_periodic, mqtt)
        self._associated_actor_name = associated_actor_name
        self._actor_gui_subscriptions = ('Name', 'Birthday', 'SoftwareRevision', 'PeriodicActive', 'PeriodicCounter', 'Set-PeriodicActive')
        self._actor_gui_topics = ('Set-PerodicActive', )
        self._tkgui = tkgui
        self._tkgui.set_periopdic_activate_cb(self.on_set_periodic)
        pyacdaq_logger.debug(self._name + ' Actor Associated Actor Name: ' + str(self._associated_actor_name))
        pass

    def __del__(self):
        """Finalize instance."""
        super().__del__()
        pass

    # Protected override methods from super Actor
    def _on_connect(self):
        """
        Called from _on_receive() when message was received from paho.mqtt.client callback message on_connect.

        Perform action on MQTT connect here, e.g. publish and subscribe topics.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' ActorGui _on_connect() Received MQTT connected message. Publish and subscribe...')
        try:
            if self._mqtt is not None:
                pyacdaq_logger.debug(self._name + ' ActorGui _on_connect() Publishing:' + str(self._actor_gui_subscriptions))
                prefixed_topics = ()
                for topic in self._actor_gui_subscriptions:
                    prefixed_topic = self._associated_actor_name + '/' + topic
                    pyacdaq_logger.debug(self._name + ' ActorGui _on_connect() Publishing:' + str(prefixed_topic))
                    prefixed_topics = prefixed_topics + (prefixed_topic,)
                    self._mqtt.publish_topic(prefixed_topic, '', qos=0, retain=True, expand=False)
                rcs_mids = self._mqtt.subscribe_topics(self._in_future, prefixed_topics, qos=0).get()
                pyacdaq_logger.debug(self._name + ' ActorGui _on_connect() Subscribing to: ' + str(prefixed_topics) + ' returned rcs, mids = ' + str(rcs_mids))
        except Exception as e:
            pyacdaq_logger.exception(self._name + ' ActorGui _on_connect(): Exception cought:')
            pyacdaq_logger.exception(self._name + ' ActorGui _on_connect(): Exception: ' + str(e))
        super()._on_connect()
        pass

    def _on_failure(self, exception_type, exception_value, traceback):
        """
        Hook for doing any cleanup after an unhandled exception is raised, and before the actor stops.

        Virtual protected method. Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        self.close()
        super()._on_failure(exception_type, exception_value, traceback)
        pass

    def _on_message(self, topic, msg):
        """
        Called from AcdaqMqtt._on_receive() when message was received from paho.mqtt.client callback message on_message.

        Perform action on MQTT messages here or forward to super.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' ActorGui _on_message() Received MQTT topic.' + topic + ' msg: ' + str(msg))
        sub_topics = re.split('/', topic)
        if sub_topics[1].find(self._associated_actor_name) >= 0:
            match sub_topics[2]:
                case 'Name':
                    pyacdaq_logger.debug(self._name + ' ActorGui _on_message() Name received, msg: ' + str(msg))
                    self._tkgui.v_actor_name.set('Name: ' + str(msg))
                case 'Birthday':
                    pyacdaq_logger.debug(self._name + ' ActorGui _on_message() Birthday received, msg: ' + str(msg))
                    self._tkgui.v_actor_birthday.set('Born: ' + str(msg))
                case 'PeriodicActive':
                    pyacdaq_logger.debug(self._name + ' ActorGui _on_message() PeriodicActive received, msg: ' + str(msg))
                    self._tkgui.v_actor_periodic_active.set('Active: ' + str(msg))
                case 'PeriodicCounter':
                    pyacdaq_logger.debug(self._name + ' ActorGui _on_message() PeriodicCounter received, msg: ' + str(msg))
                    self._tkgui.v_actor_periodic_counter.set('Counter: ' + str(msg))
                case 'Set-PeriodicActive':
                    pyacdaq_logger.debug(self._name + ' ActorGui _on_message() Set-PeriodicActive received, msg: ' + str(msg))
                    self._tkgui.v_actor_set_periodic_active.set(msg.lower().find('true') >= 0)
                case 'SoftwareRevision':
                    pyacdaq_logger.debug(self._name + ' ActorGui _on_message() SoftwareRevision received, msg: ' + str(msg))
                    self._tkgui.v_actor_sw_revision.set('SW: ' + re.split('\|', msg, 1)[0])
                case _:  # Call super
                    pyacdaq_logger.debug(self._name + ' ActorGui _on_message() Calling super() due for unknown topic: ' + topic + ' msg: ' + str(msg))
                    super()._on_message(topic, msg)
        else:
            pyacdaq_logger.debug(self._name + ' ActorGui _on_message() Calling super() due for unknown topic: ' + topic + ' msg: ' + str(msg))
            super()._on_message(topic, msg)
        pass

    def _on_receive(self, message):
        """
        May be implemented for the actor to handle regular non-proxy messages or forward to super.

        Virtual protected method. Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        warnings.warn(self._name + ' Actor _on_receive() Method needs to be implemented by derived class.')
        super()._on_receive(message)
        pass

    def _on_start(self):
        """
        Hook for doing any setup that should be done after the actor is started, but before it starts processing messages.

        Initialize instrument.

        Virtual protected method. Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        super()._on_start()
        pass

    def _on_stop(self):
        """
        Hook for doing any cleanup that should be done after the actor has processed the last message, and before the actor stops.

        Close instrument.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' ActorGui _on_stop()')
        try:
            if self._mqtt is not None:
                prefixed_topics = ()
                pyacdaq_logger.debug(self._name + ' ActorGui _on_stop() Unsubscribing: ' + str(self._actor_gui_subscriptions))
                for topic in self._actor_gui_subscriptions:
                    prefixed_topic = self._name + '/' + topic
                    prefixed_topics = prefixed_topics + (prefixed_topic,)
                pyacdaq_logger.debug(self._name + ' ActorGui _on_stop() Unsubscribing from: ' + str(prefixed_topics))
                self._mqtt.unsubscribe_topics(self, prefixed_topics)
        except Exception as e:
            pyacdaq_logger.exception(self._name + ' ActorGui _on_stop(): Exception cought:')
            pyacdaq_logger.exception(self._name + ' ActorGui _on_stop(): Exception: ' + str(e))
        super()._on_stop()
        pass

    def _periodic(self):
        """
        Implementation of periodic actions.

        Virtual protected method. Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        warnings.warn(self._name + ' Actor _periodic() Method needs to be implemented by derived class.')
        super()._periodic()
        pass

    def _sw_revision(self):
        """
        Return revision of this class inclusive parents.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        string
            Software revision.

        """
        return 'ActorGui_0.1.0.0|' + super()._sw_revision()

    # Protected virtual methods defined by this class
    # Public methods
    def on_set_periodic(self):
        """Handle actor_set_periodic_active = tk.Checkbutton()."""
        activate_periodic = self._tkgui.v_actor_set_periodic_active.get()
        pyacdaq_logger.debug(self._name + ' ActorGui on_set_periodic():' + str(activate_periodic))
        if self._mqtt is not None:
            self._mqtt.publish_topic(self._associated_actor_name + '/Set-PeriodicActive', activate_periodic, qos=0, retain=True, expand=False)
        pass

    # Getter
    def getAssociatedActorName(self):
        """Return  associated actor name."""
        return self._associated_actor_name

    pass  # End of class definition


if __name__ == '__main__':
    app_name = re.split('.py', os.path.basename(__file__))[0]
    print(app_name + "\n\
Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme")
    # Logging setup
    print('Configuring logging...')
    # Define logger to be used
    # logging.basicConfig(  # filename='example.log', filemode='w', encoding='utf-8', level=logging.INFO, style='{', format='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    logger = logging.getLogger('App-Logger')
    logger.setLevel(logging.DEBUG)
    pyacdaq_logger = logging.getLogger('pyacdaq')
    pyacdaq_logger.setLevel(logging.DEBUG)
    # Define logging formatter
    logging_formatter = logging.Formatter(style='{', fmt='{asctime} [{levelname:8}] {module} {funcName} {lineno} {message}')
    # logging_formatter = logging.Formatter(style='{', fmt='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    # configure_logging_handler((logger, pyacdaq_logger), logging_formatter, severity={'console': None, 'file': None, 'mqtt': None, 'syslog': None}, mqtt={'broker': 'ee-raspi1001', 'topic': 'ACDAQ/log_msg', 'user': '', 'password': ''})

    try:
        mqtt_broker = input('MQTT Broker? (localhost)>')
        if len(mqtt_broker) == 0:
            mqtt_broker = 'localhost'
        mqtt_user = input('MQTT Username: ')
        if len(mqtt_user) == 0:
            mqtt_user = None
        mqtt_password = getpass.getpass()
        if len(mqtt_password) == 0:
            mqtt_password = None
        configure_logging_handler(app_name, (logger, pyacdaq_logger), logging_formatter, severity={'console': logging.INFO, 'file': logging.DEBUG, 'mqtt': logging.INFO, 'syslog': logging.WARNING}, mqtt={'broker': mqtt_broker, 'topic': 'ACDAQ/log_msg', 'user': mqtt_user, 'password': mqtt_password})
        logger.info('Please, wait for MQTT actor.')
        # Create MQTT actor proxy
        ma = Mqtt.start('Mqtt',
                        start_periodic=False,
                        broker=mqtt_broker,
                        port=1883,
                        client_id='ACDAQ',
                        clean_session=True,
                        retain=True,
                        will='offline',
                        username=mqtt_user,
                        password=mqtt_password
                        ).proxy()
        # Launch GUI
        logger.info('Please, wait for actor GUI.')
        time.sleep(3)
        root = tk.Tk()
        actor_tkgui = ActorTkGui(root)
        # Create Gui actor proxy
        actor_gui = ActorGui.start('ActorGui',
                                   start_periodic=False,
                                   mqtt=ma,
                                   associated_actor_name='Actor',
                                   tkgui=actor_tkgui
                                   ).proxy()
        # Create actor proxy
        logger.info('Please, wait for actor.')
        time.sleep(5)
        actor = Actor.start(name='Actor',
                            start_periodic=False,
                            mqtt_actor=ma
                            ).proxy()
        actor_name = actor.getName().get()
        actor_birthday = actor.getBirthday().get()
        actor_periodic = actor.getPeriodicStatus().get()
        actor_SWRevision = actor.getSWRevision().get()
        print(__name__, 'Name:', actor_name,
              '; Birthday:', actor_birthday,
              '; Periodic:', actor_periodic,
              '; SW:', actor_SWRevision)
        # Start the Tkinter GUI Loop
        logger.info('Start the Tkinter GUI loop.')
        actor_tkgui.mainloop()
    except Exception as e:
        print('Exception caught in __main__', e)
        logger.exception('_main__: Exception caught:', str(e))
    finally:
        logger.info('Stopping actor GUI.')
        actor_gui.stop()
        logger.info('Stopping actor.')
        time.sleep(3)
        actor.stop()
        logger.info('Stopping MQTT actor.')
        time.sleep(3)
        ma.stop()
    logging.shutdown()
    print(__name__, 'Done.')
