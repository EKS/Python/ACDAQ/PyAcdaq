#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Actor class implementing an interface to MQTT.

- The MQTT part is inpired by http://www.steves-internet-guide.com

Lizenziert unter EUPL V. 1.2

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
import getpass
import json
import logging
import numpy
import os
import pandas
import paho.mqtt.client as mqtt
import re
import time
from pyacdaq.logger import configure_logging_handler
from .actor import Actor as Actor
pyacdaq_logger = logging.getLogger('pyacdaq')


# paho-mqtt callback funktions
def on_log(client, userdata, level, buf):
    """Log buffer if callback is assigned."""
    pyacdaq_logger.debug('MQTT log: ' + buf)
    pass


def on_connect(client, userdata, flags, rc):
    """
    Handle broker connected callback.

    Publish all topic once.
    Subscribe to desired topics.
    """
    if rc == 0:
        client.connected_flag = True
        pyacdaq_logger.debug('MQTT Connected OK')
        userdata['actor_ref'].on_receive({'mqtt': 'on_connect', 'client': client, 'userdata': userdata, 'flags': flags, 'rc': rc})
    else:
        pyacdaq_logger.error('MQTT Bad connection, returned code rc=' + str(rc))
    pass


def on_disconnect(client, userdata, rc=0):
    """Handle broker disconnected callback."""
    client.connected_flag = False
    pyacdaq_logger.debug('MQTT Disconnected result code rc=' + str(rc))
    userdata['actor_ref'].on_receive({'mqtt': 'on_disconnect', 'client': client, 'userdata': userdata, 'rc': rc})
    pass


def on_message(client, userdata, msg):
    """
    Handle message received callback.

    Decode received message data and insert into command processor queue.
    """
    topic = msg.topic
    m_decode = str(msg.payload.decode('utf-8', 'ignore'))
    try:
        pyacdaq_logger.debug('MQTT Message received. Topic: ' + topic + ' Payload: ' + str(m_decode))
        userdata['actor_ref'].on_receive({'mqtt': 'on_message', 'client': client, 'userdata': userdata, 'topic': topic, 'msg': m_decode})
    except Exception as e:
        pyacdaq_logger.exception('MQTT Message received, but not transmitted to receiver. Topic: ' + topic + str(e))
    pass


def on_publish(client, userdata, mid):
    """Handle publish callback."""
    pyacdaq_logger.debug('MQTT Client published message ID=' + str(mid))
    pass


def on_subscribe(client, userdata, mid, granted_qos):
    """Handle subscribed callback."""
    pyacdaq_logger.debug('MQTT Client subscribed message ID=' + str(mid) + ' with qos=' + str(granted_qos))
    pass


def on_unsubscribe(client, userdata, mid):
    """Handle unsubscribed callback."""
    pyacdaq_logger.debug('MQTT Client unsubscribed message ID=' + str(mid))
    pass


class Mqtt(Actor):
    """
    Actor class providing an interface to MQTT using the paho-mqtt library.

    The MQTT part is inpired by http://www.steves-internet-guide.com
    """

    def __init__(self, name, start_periodic=False, mqtt_actor=None, broker='localhost', port=1883, client_id=None, clean_session=True, retain=False, will='offline', username=None, password=None):
        """
        Initialize this instance.

        Parameters
        ----------
        name : str
            Name of instance.
        start_periodic : bool, optional
            Auto start periodic action if true. Default is False.
        mqtt_actor : Mqtt Proxy, ignored
            Proxy for Mqtt actor instance.
        broker : string, optional
            IP/nodename of MQTT broker. Default is 'localhost'.
        port : int, optional
            Brokers listener port. Default is 1883.
        client_id : str, optional
            Client identifier. Default=None.
        clean_session : bool, optional
            Default is True.
        retain : bool, optional
            Override publish requests with True, Default is 'False'.
        will : str, optional
            Default is 'offline'.
        username : str, optional
            Default is None.
        password : str, optional
            Default is None.

        Returns
        -------
        None.

        """
        super().__init__(name, start_periodic, mqtt_actor)
        self._broker = broker
        self._port = port
        pyacdaq_logger.debug('Client ID: ' + client_id)
        if client_id is None:
            self._client_id = name
        else:
            self._client_id = client_id
        pyacdaq_logger.debug('self._client ID:' + str(self._client_id))
        self._clean_session = clean_session
        self._retain = retain
        self._will = will
        self._username = username
        self._password = password
        self._client = None
        # Subscriber Actor's
        self._subscribers = set()
        self._subscriptions = {}
        pyacdaq_logger.debug(self._name + ' Broker=' + self._broker + ' Port=' + str(self._port) + 'Client ID=' + self._client_id)
        pass

    def __del__(self):
        """Finalize instance."""
        super().__del__()
        pass

    # Private Methods
    def __connect_broker(self):
        """Connect MQTT client to broker."""
        pyacdaq_logger.debug(self._name + ' Mqtt _on_start() Connecting to broker.')
        if not self._mqtt:
            self._mqtt = self.actor_ref.proxy()
        try:
            self._client = mqtt.Client(self._client_id,
                                       self._clean_session,
                                       userdata={'actor_ref': self}
                                       )
            self._client.connected_flag = False
            if self._username is not None and self._password is not None:
                self._client.username_pw_set(username=self._username, password=self._password)
            self._client.will_set(self._client_id, self._will, 1, False)
            # bind call back function
            self._client.on_connect = on_connect
            self._client.on_disconnect = on_disconnect
            # self._client.on_log = on_log
            self._client.on_publish = on_publish
            self._client.on_message = on_message
            self._client.on_subscribe = on_subscribe
            self._client.on_unsubscribe = on_unsubscribe
            pyacdaq_logger.debug('Connecting client ' + self._client_id + ' to broker ' + self._broker)
            self._client.loop_start()
            self._client.connect(self._broker, self._port, keepalive=60, bind_address='')
        except BaseException as e:
            pyacdaq_logger.exception(self._name + ' Exception catched! ' + str(e))
            self._client.connected_flag = False
        pass

    def __disconnect_broker(self):
        """Disconnect MQTT client from broker."""
        pyacdaq_logger.debug(self._name + ' Mqtt __disconnect_broker() Disconnect broker.')
        try:
            pyacdaq_logger.debug(self._name + ' Mqtt __disconnect_broker() Unsubscribing from: #')
            rc, mid = self._client.unsubscribe('#')
            pyacdaq_logger.debug(self._name + ' Mqtt __disconnect_broker() Unsubscribing from: # returned rc=' + str(rc) + ' mid=' + str(mid))
            pyacdaq_logger.debug(self._name + ' Mqtt __disconnect_broker() Publishing: "disconnected"')
            rc, mid = self._client.publish(self._client_id, "disconnected")
            pyacdaq_logger.debug(self._name + ' Mqtt __disconnect_broker() Publishing: "disconnected" returned rc=' + str(rc) + ' mid=' + str(mid))
            pyacdaq_logger.debug(self._name + ' Mqtt __disconnect_broker() Disonnecting from broker ' + self._broker)
            self._client.disconnect()
            time.sleep(1)
            self._client.loop_stop(force=True)
        except BaseException as e:
            pyacdaq_logger.exception(self._name + " Mqtt __disconnect_broker() Exception catched when discopnnection from MQTT broker! ", e)
        finally:
            self._client.loop_stop(force=True)
        pass

    # Protected Methods
    def _on_connect(self):
        """
        Called from _on_receive() when message was received from paho.mqtt.client callback message on_connect.

        Perform action on MQTT connect here, e.g. publish and subscribe topics.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' Mqtt _on_connect() Received MQTT connected message.')
        super()._on_connect()
        pass

    def _on_disconnect(self):
        """
        Called from _on_receive() when message was received from paho.mqtt.client callback message on_disconnect.

        Perform action on MQTT disconnect here.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' Mqtt _on_disconnect() Received MQTT disconnected message.')
        super()._on_disconnect()
        pass

    def _on_failure(self, exception_type, exception_value, traceback):
        """
        Hook for doing any cleanup after an unhandled exception is raised, and before the actor stops.

        Returns
        -------
        None.

        """
        pyacdaq_logger.error(self._name + ' Mqtt _on_failure() disconnecting broker.')
        self.__disconnect_broker()
        super()._on_failure(exception_type, exception_value, traceback)
        pass

    def _on_message(self, topic, msg):
        """
        Called from Mqtt._on_receive() when message was received from paho.mqtt.client callback message on_message.

        Perform action on MQTT messages here or forward to super.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' Mqtt _on_message() Received MQTT topic.' + topic + ' msg: ' + str(msg))
        sub_topics = re.split('/', topic)
        match sub_topics[2]:  # Add more cases as needed.
            case _:  # Call super
                pyacdaq_logger.debug(self._name + ' Mqtt _on_message() Calling super() due for unknown topic: ' + topic + ' msg: ' + str(msg))
                super()._on_message(topic, msg)
        pass

    def _on_receive(self, message):
        """
        Handle regular actor non-proxy messages.

        Returns
        -------
        None.

        """
        match message:
            case 'TestCommand':  # sent from __main__ for testing
                print(self._name, 'Mqtt _on_receive() Received tell message: TestCommand.')
                pass
            case _:
                try:
                    match message['mqtt']:  # Messages from MQTT callback functions.
                        case 'on_connect':
                            pyacdaq_logger.debug(self._name + ' Mqtt _on_receive() Received MQTT connected message.')
                            self.on_connect()
                            for subscriber in self._subscribers:
                                subscriber.on_connect()
                            pass
                        case 'on_disconnect':
                            pyacdaq_logger.debug(self._name + ' Mqtt _on_receive() Received MQTT disconnected message.')
                            for subscriber in self._subscribers:
                                subscriber.on_disconnect()
                            pass
                        case 'on_message':
                            pyacdaq_logger.debug(self._name + ' Mqtt _on_receive() Received MQTT message. Topic: ' + message['topic'])
                            for subscriber in self._subscriptions[message['topic']]:  # send to topic subscribers only
                                subscriber.on_message(message['topic'], message['msg'])
                            pass
                        case _:  # Call super
                            pyacdaq_logger.error(self._name + ' Mqtt _on_receive() Received unhandled message: ' + message['mqtt'])
                            super()._on_receive(message)
                            pass
                except Exception as e:
                    pyacdaq_logger.exception(self._name + ' Mqtt _on_receive(): Exception cought:')
                    pyacdaq_logger.exception(self._name + ' Mqtt _on_receive(): Message: ' + str(message))
                    pyacdaq_logger.exception(self._name + ' Mqtt _on_receive(): Exception: ' + str(e))
                pass
        pass

    def _on_start(self):
        """
        Hook for doing any setup that should be done after the actor is started, but before it starts processing messages.

        Create MQTT client and connect to broker.

        Returns
        -------
        None.

        """
        self.__connect_broker()
        # super()._on_start()
        pass

    def _on_stop(self):
        """
        Hook for doing any cleanup that should be done after the actor has processed the last message, and before the actor stops.

        Disconnect broker.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' Mqtt _on_stop() Disconnecting broker.')
        super()._on_stop()
        self.__disconnect_broker()
        pass

    def _periodic(self, reason):
        """
        Implementation of periodic actions.

        Parameters
        ----------
        reason : PeriodicCallReason
            Reason for calling this method.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' Mqtt Mqtt _on_periodic() Calling super.')
        super()._periodic(reason)
        pass

    def _sw_revision(self):
        """
        Return revision of this class inclusive parents.

        Override method needs to be implemented by derived class.

        Returns
        -------
        string
            Software revision.

        """
        pyacdaq_logger.debug(self._name + ' Mqtt _sw_revision()')
        return 'Mqtt_0.1.0.0|' + super()._sw_revision()

    # Public Methods
    def publish_topic(self, topic, value, qos=0, retain=False, expand=False):
        """
        Publish a topic to MQTT broker.

        Parameters
        ----------
        topic : str
            Topic string will become prefixed with client_id.
        value : any
            Value will be dumped to json.
        expand : bool, optional
            Publish elements of list or tuple to next topic level by index. The default is False.

        Returns
        -------
        rcs_mids: ((rc,mid),...)
            rcs_mids: return codes and message ids from topic publication.
            rcs_mids[0]: topic; rcs_mids[n+1]: topic n

        """
        pyacdaq_logger.debug(self._name + ' Mqtt publish_topic()')
        try:
            rcs_mids = (())
            prefixed_topic = self._client_id + '/' + topic
            if (isinstance(value, list | tuple | set | numpy.ndarray | pandas.core.series.Series)):  # Performance improvement proposed by Bjorn Schmitt, B.Schmitt@gsi.de
                rc, mid = self._client.publish(prefixed_topic, json.dumps(value.tolist()), qos, retain or self._retain)
            else:
                rc, mid = self._client.publish(prefixed_topic, json.dumps(value), qos, retain or self._retain)
            rcs_mids = rcs_mids + ((rc, mid),)
            pyacdaq_logger.debug(self._name + ' Mqtt publish_topic() Publishing topic: ' + prefixed_topic + ' returned rc=' + str(rc) + ' mid=' + str(mid))
            if expand and (isinstance(value, list | tuple | set | numpy.ndarray | pandas.core.series.Series)):  # Performance improvement proposed by Bjorn Schmitt, B.Schmitt@gsi.de
                for index, val in enumerate(value):
                    sub_topic = prefixed_topic + '/' + str(index)
                    if isinstance(val, numpy.bool_):
                        rc, mid = self._client.publish(sub_topic, bool(val), qos, retain or self._retain)
                    elif isinstance(val, numpy.short | numpy.ushort | numpy.intc | numpy.uintc | numpy.int_ | numpy.uint | numpy.longlong | numpy.ulonglong):
                        rc, mid = self._client.publish(sub_topic, int(val), qos, retain or self._retain)
                    elif isinstance(val, numpy.half | numpy.float16 | numpy.single | numpy.double | numpy.longdouble):
                        rc, mid = self._client.publish(sub_topic, float(val), qos, retain or self._retain)
                    else:
                        rc, mid = self._client.publish(sub_topic, val, qos, retain or self._retain)
                    rcs_mids = rcs_mids + ((rc, mid),)
                    pyacdaq_logger.debug(self._name + ' Mqtt publish_topic() Publishing sub_topic: ' + sub_topic + ' returned rc=' + str(rc) + ' mid=' + str(mid))
        except Exception as e:
            pyacdaq_logger.exception(self._name + ' Mqtt publish_topic(): Exception cought:')
            pyacdaq_logger.exception(self._name + ' Mqtt publish_topic(): Topic: ' + topic)
            pyacdaq_logger.exception(self._name + ' Mqtt publish_topic(): Exception: ' + str(e))
        return rcs_mids

    def register_subscriber(self, subscriber):
        """
        Register Actor subscribing to MQTT topics.

        Parameters
        ----------
        subscriber : Actor Proxy
            Actor proxy of subscriber.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' Mqtt register_subscriber() add subscriber: ' + str(subscriber))
        if subscriber not in self._subscribers:
            self._subscribers.add(subscriber)
            subscriber.on_connect()
        pyacdaq_logger.debug(self._name + ' Mqtt register_subscriber() subscribers: ' + str(self._subscribers))
        pass

    def unregister_subscriber(self, subscriber):
        """
        Unregister Actor subscribing to MQTT topics.

        Parameters
        ----------
        subscriber : Actor Proxy
            Actor proxy of subscriber.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' Mqtt unregister_subscriber() discard subscriber: ' + str(subscriber))
        self._subscribers.discard(subscriber)
        pyacdaq_logger.debug(self._name + ' Mqtt unregister_subscriber() subscribers: ' + str(self._subscribers))
        pass

    def subscribe_topics(self, subscriber, topics, qos=0):
        """
        Subscribe topics of MQTT broker.

        Parameters
        ----------
        subscriber : Actor Proxy
            Actor proxy of subscriber.
        topics : tuple
            Tuple of topics. Topic string will become prefixed with client_id.

        Returns
        -------
        rcs_mids: ((rc,mid),...)
            rcs_mids: return codes and message ids from topic subscription.

        """
        pyacdaq_logger.debug(self._name + 'Mqtt subscribe_topics() topics: ' + str(topics))
        try:
            rcs_mids = (())
            for topic in topics:
                prefixed_topic = self._client_id + '/' + topic
                pyacdaq_logger.debug(self._name + ' Mqtt subscribe_topics() Subscribing topic: ' + prefixed_topic)
                if prefixed_topic not in self._subscriptions:
                    self._subscriptions.update({prefixed_topic: set((subscriber,))})
                else:
                    subs = self._subscriptions[prefixed_topic]
                    subs.add(subscriber)
                    self._subscriptions.update({prefixed_topic: subs})
                pyacdaq_logger.debug(self._name + ' Mqtt subscribe_topics() subscriptions: ' + str(self._subscriptions))
                rc, mid = self._client.subscribe(prefixed_topic, qos=0)
                pyacdaq_logger.debug(self._name + ' Mqtt subscribe_topics() Subscribing topic: ' + prefixed_topic + ' returned rc=' + str(rc) + ' mid=' + str(mid))
            rcs_mids = rcs_mids + ((rc, mid),)
        except Exception as e:
            pyacdaq_logger.exception(self._name + ' Mqtt subscribe_topics(): Exception cought:')
            pyacdaq_logger.exception(self._name + ' Mqtt subscribe_topics(): Subscriber: ' + str(subscriber))
            pyacdaq_logger.exception(self._name + ' Mqtt subscribe_topics(): Exception: ' + str(e))
        return rcs_mids

    def unsubscribe_topics(self, subscriber, topics):
        """
        Unsbscribe topics from MQTT broker.

        Parameters
        ----------
        subscriber : Actor Proxy
            Actor proxy of subscriber.
        topics : tuple
            Tuple of topics. Topic string will become prefixed with client_id.

        Returns
        -------
        rcs_mids: ((rc,mid),...)
            rcs_mids: return codes and message ids from topic unsubscription.

        """
        pyacdaq_logger.debug(self._name + 'Mqtt subscribe_topics() topics: ' + str(topics))
        try:
            rcs_mids = (())
            for topic in topics:
                prefixed_topic = self._client_id + '/' + topic
                pyacdaq_logger.debug(self._name + ' Mqtt unsubscribe_topics() unsubscribing topic: ' + prefixed_topic)
                if prefixed_topic in self._subscriptions:
                    subs = self._subscriptions[prefixed_topic]
                    subs.discard(subscriber)
                    if len(subs) > 0:
                        self._subscriptions.update({prefixed_topic: subs})
                    else:
                        del self._subscriptions[prefixed_topic]
                pyacdaq_logger.debug(self._name + ' Mqtt unsubscribe_topics() subscriptions: ' + str(self._subscriptions))
                rc, mid = self._client.unsubscribe(prefixed_topic)
                pyacdaq_logger.debug(self._name + ' Mqtt unsubscribe_topics() Unubscribing topic: ' + prefixed_topic + ' returned rc=' + str(rc) + ' mid=' + str(mid))
            rcs_mids = rcs_mids + ((rc, mid),)
        except Exception as e:
            pyacdaq_logger.exception(self._name + ' Mqtt unsubscribe_topics(): Exception cought:')
            pyacdaq_logger.exception(self._name + ' Mqtt unsubscribe_topics(): Substriber: ' + str(subscriber))
            pyacdaq_logger.exception(self._name + ' Mqtt unsubscribe_topics(): Exception: ' + str(e))
        return rcs_mids

    # Getter
    def getBroker(self):
        """Return broker node."""
        return self._broker

    def getPort(self):
        """Return hardware revision."""
        return self._port

    def getClientID(self):
        """Return mqtt client_id."""
        return self._client_id

    pass  # End of class definition


if __name__ == '__main__':
    print("mqtt.py\n\
Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme")
    app_name = re.split('.py', os.path.basename(__file__))[0]
    # Logging setup
    print('Configuring logging...')
    # Define logger to be used
    # logging.basicConfig(  # filename='example.log', filemode='w', encoding='utf-8', level=logging.INFO, style='{', format='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    logger = logging.getLogger('App-Logger')
    logger.setLevel(logging.DEBUG)
    pyacdaq_logger = logging.getLogger('pyacdaq')
    pyacdaq_logger.setLevel(logging.DEBUG)
    # Define logging formatter
    logging_formatter = logging.Formatter(style='{', fmt='{asctime} [{levelname:8}] {module} {funcName} {lineno} {message}')
    # logging_formatter = logging.Formatter(style='{', fmt='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    # configure_logging_handler((logger, pyacdaq_logger), logging_formatter, severity={'console': None, 'file': None, 'mqtt': None, 'syslog': None}, mqtt={'broker': 'ee-raspi1001', 'topic': 'ACDAC/log_msg', 'user': '', 'password': ''})

    try:
        mqtt_broker = input('MQTT Broker? (localhost)>')
        if len(mqtt_broker) == 0:
            mqtt_broker = 'localhost'
        mqtt_user = input('MQTT username: ')
        if len(mqtt_user) == 0:
            mqtt_user = None
        mqtt_password = getpass.getpass()
        if len(mqtt_password) == 0:
            mqtt_password = None
        configure_logging_handler(app_name, (logger, pyacdaq_logger), logging_formatter, severity={'console': logging.INFO, 'file': logging.DEBUG, 'mqtt': logging.INFO, 'syslog': logging.WARNING}, mqtt={'broker': mqtt_broker, 'topic': 'ACDAQ/log_msg', 'user': mqtt_user, 'password': mqtt_password})
        print('Please, wait until done.')
        a = Mqtt.start('Mqtt',
                       start_periodic=False,
                       mqtt_actor=None,
                       broker=mqtt_broker,
                       port=1883,
                       client_id='ACDAQ',
                       clean_session=True,
                       retain=True,
                       will='offline',
                       username=mqtt_user,
                       password=mqtt_password
                       )
        ma = a.proxy()
        a.tell('TestCommand')
        ma_name = ma.getName().get()
        ma_birthday = ma.getBirthday().get()
        ma_periodic = ma.getPeriodicStatus().get()
        ma_broker = ma.getBroker().get()
        ma_port = ma.getPort().get()
        ma_client_id = ma.getClientID().get()
        print(__name__, 'Name:', ma_name,
              '; Birthday:', ma_birthday,
              '; Periodic:', ma_periodic,
              '; Broker:', ma_broker,
              '; Port:', ma_port,
              '; Client ID:', ma_client_id,
              )
        time.sleep(5)
    except Exception as e:
        logger.exception('_main__: Exception caught:', str(e))
    finally:
        ma.stop()
    logging.shutdown()
    print(__name__, 'Done.')
