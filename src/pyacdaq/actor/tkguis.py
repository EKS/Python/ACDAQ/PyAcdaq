#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
GUI classes for associates Actors based on Tkinter

This script is used to test the layout of PyAcdaq Actor GUI's.

Lizenziert unter EUPL V. 1.2

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
import tkinter as tk


class ActorTkGui(tk.Frame):
    """
    GUI class for Actor.

    It provides a basic tkinter based GUI for Actor.
    """

    def __init__(self, master=None):
        super().__init__(master)
        self.pack()
        self.master.title('ActorGui')
        self.create_widgets()

    def create_widgets(self):
        """
        Create graphical user interface using tkinter widgets.

        Returns
        -------
        None.

        """
        self._create_widgets()
        pass

    # Protected virtual methods
    def _create_widgets(self):
        """
        Extend graphical user interface using tkinter widgets.

        Returns
        -------
        None.

        """
        # Variables
        self.v_actor_name = tk.StringVar()
        self.v_actor_name.set('NA')
        self.v_actor_birthday = tk.StringVar()
        self.v_actor_birthday.set('NA')
        self.v_actor_sw_revision = tk.StringVar()
        self.v_actor_sw_revision.set('NA')
        self.v_actor_set_periodic_active = tk.BooleanVar()
        self.v_actor_set_periodic_active.set(False)
        self.v_actor_periodic_active = tk.StringVar()
        self.v_actor_periodic_active.set('NA')
        self.v_actor_periodic_counter = tk.StringVar()
        self.v_actor_periodic_counter.set('NA')
        # Actor Frame label widget
        self.g_actor = tk.LabelFrame(self)
        self.g_actor['text'] = 'Actor Class Level'
        self.g_actor.pack()
        # Frame label widget
        self.g_actor_info = tk.LabelFrame(self.g_actor)
        self.g_actor_info['text'] = 'Associated Actor Information'
        self.g_actor_info.pack()
        # Frame label widget
        self.g_actor_periodic = tk.LabelFrame(self.g_actor)
        self.g_actor_periodic['text'] = 'Periodic Activity Information'
        self.g_actor_periodic.pack()
        # Widgets
        self.l_actor_name = tk.Label(self.g_actor_info)
        self.l_actor_name['textvariable'] = self.v_actor_name
        self.l_actor_name.pack()
        self.l_actor_birthday = tk.Label(self.g_actor_info)
        self.l_actor_birthday['textvariable'] = self.v_actor_birthday
        self.l_actor_birthday.pack()
        self.l_actor_sw_revision = tk.Label(self.g_actor_info)
        self.l_actor_sw_revision['textvariable'] = self.v_actor_sw_revision
        self.l_actor_sw_revision.pack()
        # Widgets
        self.c_actor_set_periodic_active = tk.Checkbutton(self.g_actor_periodic)
        self.c_actor_set_periodic_active['text'] = 'Set'
        self.c_actor_set_periodic_active['variable'] = self.v_actor_set_periodic_active
        self.c_actor_set_periodic_active.pack()
        self.l_actor_periodic_active = tk.Label(self.g_actor_periodic)
        self.l_actor_periodic_active['textvariable'] = self.v_actor_periodic_active
        self.l_actor_periodic_active.pack()
        self.l_actor_periodic_counter = tk.Label(self.g_actor_periodic)
        self.l_actor_periodic_counter['textvariable'] = self.v_actor_periodic_counter
        self.l_actor_periodic_counter.pack()
        pass

    def set_periopdic_activate_cb(self, cb_function):
        """Set callback function for set_periopdic_activate checkbutton."""
        self.c_actor_set_periodic_active['command'] = cb_function
        pass

    pass  # end of class definition


class DeviceTkGui(ActorTkGui):
    """
    GUI class for Device.

    It provides a basic tkinter based GUI for Device.
    """

    def __init__(self, master=None):
        super().__init__(master)
        self.master.title('DeviceGui')

    def _create_widgets(self):
        """
        Create graphical user interface using tkinter widgets.

        Returns
        -------
        None.

        """
        super()._create_widgets()
        # Variables
        self.v_device_resource = tk.StringVar()
        self.v_device_resource.set('NA')
        self.v_device_instrument_id = tk.StringVar()
        self.v_device_instrument_id.set('NA')
        self.v_device_serial_number = tk.StringVar()
        self.v_device_serial_number.set('NA')
        self.v_device_firmware_revision = tk.StringVar()
        self.v_device_firmware_revision.set('NA')
        self.v_device_hardware_revision = tk.StringVar()
        self.v_device_hardware_revision.set('NA')
        self.v_device_reset_result = tk.StringVar()
        self.v_device_reset_result.set('NA')
        self.v_device_selftest_code = tk.StringVar()
        self.v_device_selftest_code.set('NA')
        self.v_device_selftest_message = tk.StringVar()
        self.v_device_selftest_message.set('NA')
        self.v_device_set_reset = tk.BooleanVar()
        self.v_device_set_reset.set(False)
        self.v_device_set_selftest = tk.BooleanVar()
        self.v_device_set_selftest.set(False)
        # Device Frame label widget
        self.g_device = tk.LabelFrame(self)
        self.g_device['text'] = 'Device Class Level'
        self.g_device.pack()
        # Frame label widget
        self.g_device_info = tk.LabelFrame(self.g_device)
        self.g_device_info['text'] = 'Device Information'
        self.g_device_info.pack()
        # Frame label widget
        self.g_device_actions = tk.LabelFrame(self.g_device)
        self.g_device_actions['text'] = 'Device Actions'
        self.g_device_actions.pack()
        # Widgets
        self.l_device_resource = tk.Label(self.g_device_info)
        self.l_device_resource['textvariable'] = self.v_device_resource
        self.l_device_resource.pack()
        self.l_device_instrument_id = tk.Label(self.g_device_info)
        self.l_device_instrument_id['textvariable'] = self.v_device_instrument_id
        self.l_device_instrument_id.pack()
        self.l_device_serial_number = tk.Label(self.g_device_info)
        self.l_device_serial_number['textvariable'] = self.v_device_serial_number
        self.l_device_serial_number.pack()
        self.l_device_firmware_revision = tk.Label(self.g_device_info)
        self.l_device_firmware_revision['textvariable'] = self.v_device_firmware_revision
        self.l_device_firmware_revision.pack()
        self.l_device_hardware_revision = tk.Label(self.g_device_info)
        self.l_device_hardware_revision['textvariable'] = self.v_device_hardware_revision
        self.l_device_hardware_revision.pack()
        # Widgets
        self.c_device_set_reset = tk.Checkbutton(self.g_device_actions)
        self.c_device_set_reset['text'] = 'Reset'
        self.c_device_set_reset['variable'] = self.v_device_set_reset
        self.c_device_set_reset.pack()
        self.l_device_reset_result = tk.Label(self.g_device_actions)
        self.l_device_reset_result['textvariable'] = self.v_device_reset_result
        self.l_device_reset_result.pack()
        self.c_device_set_selftest = tk.Checkbutton(self.g_device_actions)
        self.c_device_set_selftest['text'] = 'Selftest'
        self.c_device_set_selftest['variable'] = self.v_device_set_selftest
        self.c_device_set_selftest.pack()
        self.l_device_selftest_code = tk.Label(self.g_device_actions)
        self.l_device_selftest_code['textvariable'] = self.v_device_selftest_code
        self.l_device_selftest_code.pack()
        self.l_device_selftest_message = tk.Label(self.g_device_actions)
        self.l_device_selftest_message['textvariable'] = self.v_device_selftest_message
        self.l_device_selftest_message.pack()
        pass

    def set_reset_cb(self, cb_function):
        """Set callback function for set_reset checkbutton."""
        self.c_device_set_reset['command'] = cb_function
        pass

    def set_selftest_cb(self, cb_function):
        """Set callback function for set_selftest checkbutton."""
        self.c_device_set_selftest['command'] = cb_function
        pass

    pass  # end of class definietion


if __name__ == '__main__':
    print("tkguis.py\n\
Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme\n")
    try:
        # Launch GUI
        root = tk.Tk()
        # app = ActorTkGui(root)
        app = DeviceTkGui(root)
        app.mainloop()
    except Exception as e:
        print('Exception caught:', e)
    finally:
        pass
    print(__name__, 'Done.')
