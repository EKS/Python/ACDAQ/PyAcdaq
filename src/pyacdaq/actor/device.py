#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Device is the base class for device actor classes.

It provides basic methods like initialize, close, etc.

Lizenziert unter EUPL V. 1.2

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
import getpass
import logging
import os
import re
import time
import tkinter as tk
import warnings
from pyacdaq.logger import configure_logging_handler
from .actor import Actor as Actor
from .actor import ActorGui as ActorGui
from .mqtt import Mqtt as Mqtt
from .tkguis import DeviceTkGui as DeviceTkGui

pyacdaq_logger = logging.getLogger('pyacdaq')


class Device(Actor):
    """
    Base class for derived device actor classes.

    It defines additional methods
    - initialize,
    - id_query,
    - reset,
    - revision_query
    - selftest,
    - sn_query and
    - close.
    """

    def __init__(self, name, start_periodic=False, mqtt=None, resource=None, id_query_flag=True, reset_flag=True, reset_options=None, selftest_flag=True):
        """
        Initialize this instance.

        Parameters
        ----------
        name : str
            Name of instance
        start_periodic : bool, optional
            Auto start periodic action if true. Default is False.
        mqtt : Mqtt Proxy, optional
            Proxy to MQTT actor. Default=None.
        resource : str, optional
            Interface to be used for communication with device. Default=None.
        id_query_flag : bool, optional
            Perform instrument reset; Default is True.
        reset_flag : bool, optional
            Perform instrument reset; Default is True.
        reset_options : str, optional
            Options to be applied after reset; Default is None
        selftest_flag : bool, optional
            Perform instrument self-test after option reset; Default is True.

        Returns
        -------
        None.

        """
        super().__init__(name, start_periodic, mqtt)
        self._firmware_revision = 'NA'
        self._hardware_revision = 'NA'
        self._id_query_flag = id_query_flag
        self._instrument_id = 'NA'
        self._reset_flag = reset_flag
        self._reset_options = reset_options
        self._reset_result = 'NA'
        self._resource = resource
        self._selftest_flag = selftest_flag
        self._selftest_code = -1
        self._selftest_message = 'NA'
        self._serial_number = 'NA'
        self._device_subscriptions = ('Set-Reset', 'Set-Selftest', )
        pyacdaq_logger.debug(self._name + ' Device Resource=' + self._resource + ' ID_Query=' + str(self._id_query_flag) + ' Reset=' + str(self._reset_flag) + ' Reset_Options=' + self._reset_options + ' Selftest=' + str(self._selftest_flag))
        pass

    def __del__(self):
        """Finalize instance."""
        super().__del__()
        pass

    # Protected override methods from super
    def _on_connect(self):
        """
        Called from _on_receive() when message was received from paho.mqtt.client callback message on_connect.

        Perform action on MQTT connect here, e.g. publish and subscribe topics.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' Device _on_connect() Received MQTT connected message. Publish and subscribe...')
        try:
            if self._mqtt is not None:
                self._mqtt.publish_topic(self._name + '/Resource', self._resource, qos=0, retain=True, expand=False)
                self._mqtt.publish_topic(self._name + '/InstrumentID', self._instrument_id, qos=0, retain=True, expand=False)
                self._mqtt.publish_topic(self._name + '/SerialNumber', self._serial_number, qos=0, retain=True, expand=False)
                self._mqtt.publish_topic(self._name + '/FirmwareRevision', self._firmware_revision, qos=0, retain=True, expand=False)
                self._mqtt.publish_topic(self._name + '/HardwareRevision', self._hardware_revision, qos=0, retain=True, expand=False)
                self._mqtt.publish_topic(self._name + '/ResetResult', self._reset_result, qos=0, retain=True, expand=False)
                self._mqtt.publish_topic(self._name + '/SelftestCode', self._selftest_code, qos=0, retain=True, expand=False)
                self._mqtt.publish_topic(self._name + '/SelftestMessage', self._selftest_message, qos=0, retain=True, expand=False)
                pyacdaq_logger.debug(self._name + ' Device _on_connect() Publishing:' + str(self._device_subscriptions))
                prefixed_topics = ()
                for topic in self._device_subscriptions:
                    prefixed_topic = self._name + '/' + topic
                    pyacdaq_logger.debug(self._name + ' Device _on_connect() Publishing:' + str(prefixed_topic))
                    prefixed_topics = prefixed_topics + (prefixed_topic,)
                    self._mqtt.publish_topic(prefixed_topic, '', qos=0, retain=True, expand=False)
                rcs_mids = self._mqtt.subscribe_topics(self._in_future, prefixed_topics, qos=0).get()
                pyacdaq_logger.debug(self._name + ' Device _on_connect() Subscribing to: ' + str(prefixed_topics) + ' returned rcs, mids = ' + str(rcs_mids))
        except Exception as e:
            pyacdaq_logger.exception(self._name + ' Device _on_connect(): Exception cought:')
            pyacdaq_logger.exception(self._name + ' Device _on_connect(): Exception: ' + str(e))
        super()._on_connect()
        pass

    def _on_failure(self, exception_type, exception_value, traceback):
        """
        Hook for doing any cleanup after an unhandled exception is raised, and before the actor stops.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        None.

        """
        self.close()
        super()._on_failure(exception_type, exception_value, traceback)
        pass

    def _on_message(self, topic, msg):
        """
        Called from Mqtt._on_receive() when message was received from paho.mqtt.client callback message on_message.

        Perform action on MQTT messages here or forward to super.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' Device _on_message() Received MQTT topic.' + topic + ' msg: ' + str(msg))
        sub_topics = re.split('/', topic)
        pyacdaq_logger.debug(self._name + ' Device _on_message() Received MQTT sub-topics: ' + str(sub_topics) + ' msg: ' + str(msg))
        match sub_topics[2]:
            case 'Set-Reset':
                options = re.split(' ', msg, 1)  # true option_0 option_1 ...
                pyacdaq_logger.debug(self._name + ' Device _on_message() Set-Reset options: ' + str(options))
                if re.match('false', options[0]) is not None:
                    pyacdaq_logger.debug(self._name + ' Device _on_message() Set-Reset=false. Ignored.')
                elif re.match('true', options[0]) is not None:
                    pyacdaq_logger.info(self._name + ' Device _on_message() Set-Reset=true. Send reset to self.')
                    if len(options) > 1:
                        pyacdaq_logger.debug(self._name + ' Device _on_message() Set-Reset=true. Send reset to self.')
                        self._in_future.reset(reset_options=options[1])
                    else:
                        self._in_future.reset(reset_options='')
                else:
                    pyacdaq_logger.debug(self._name + ' Device _on_message() Set-Reset ignored due to invalid value, msg: ' + str(msg))
            case 'Set-Selftest':
                if re.match('false', msg.lower()) is not None:
                    pyacdaq_logger.debug(self._name + ' Device _on_message() Set-Selftest=false. Ignored.')
                elif re.match('true', msg.lower()) is not None:
                    pyacdaq_logger.info(self._name + ' Device _on_message() Set-Selftest=true. Send selftest to self.')
                    self._in_future.selftest()
                else:
                    pyacdaq_logger.debug(self._name + ' Device _on_message() Set-Selftest ignored due to invalid value, msg: ' + str(msg))
            case _:  # Call super
                pyacdaq_logger.debug(self._name + ' Device _on_message() Calling super() due for unknown topic: ' + topic + ' msg: ' + str(msg))
                super()._on_message(topic, msg)
        pass

    def _on_receive(self, message):
        """
        May be implemented for the actor to handle regular non-proxy messages or forward to super.

        Virtual protected method. Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        warnings.warn(self._name + ' Device _on_receive() Method needs to be implemented by derived class.')
        super()._on_receive(message)
        pass

    def _on_start(self):
        """
        Hook for doing any setup that should be done after the actor is started, but before it starts processing messages.

        Initialize instrument.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        None.

        """
        # super()._on_start()
        self.initalize(
            self._resource,
            self._id_query_flag,
            self._reset_flag,
            self._reset_options,
            self._selftest_flag)
        pass

    def _on_stop(self):
        """
        Hook for doing any cleanup that should be done after the actor has processed the last message, and before the actor stops.

        Close instrument.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' Device _on_stop()')
        self.close()
        try:
            if self._mqtt is not None:
                prefixed_topics = ()
                pyacdaq_logger.debug(self._name + ' Device _on_stop() Unsubscribing: ' + str(self._device_subscriptions))
                for topic in self._device_subscriptions:
                    prefixed_topic = self._name + '/' + topic
                    prefixed_topics = prefixed_topics + (prefixed_topic,)
                pyacdaq_logger.debug(self._name + ' Device _on_stop() Unsubscribing from: ' + str(prefixed_topics))
                self._mqtt.unsubscribe_topics(self, prefixed_topics)
        except Exception as e:
            pyacdaq_logger.exception(self._name + ' Device _on_stop(): Exception: ' + str(e))
        super()._on_stop()
        pass

    def _periodic(self, reason):
        """
        Implementation of periodic actions.

        Virtual protected method. Override with concrete child class implementation.

        Parameters
        ----------
        reason : PeriodicCallReason
            Reason for calling this method.

        Returns
        -------
        None.

        """
        warnings.warn(self._name + ' Device _periodic() Method needs to be implemented by derived class.')
        super()._periodic(reason)
        pass

    def _sw_revision(self):
        """
        Return revision of this class inclusive parents.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        str
            Software revision.

        """
        return 'Device_0.1.0.0|' + super()._sw_revision()

    # Protected virtual methods defined by this class
    def _close(self):
        """
        Set instrument to save mode and close connection.

        Virtual protected method.
        Override method needs to be implemented by derived class.

        Returns
        -------
        int
            Error code.

        """
        warnings.warn(self._name + ' Device _close() Method needs to be implemented by derived class.')
        return -1

    def _id_query(self):
        """
        Perform id query of instrument.

        Virtual protected method. Override method needs to be implemented by derived class.

        Returns
        -------
        str
            Instrument ID.

        """
        warnings.warn(self._name + ' Device _id_query() Method needs to be implemented by derived class.')
        return 'NA'

    def _initalize(self, resource, id_query_flag=True, reset_flag=True, reset_options=None, selftest_flag=True):
        """
        Open connection and initialize instrument.

        Virtual protected method. Extend method needs to be implemented by derived class.

        Parameters
        ----------
        resource : str
            Resource identifier.
        id_query_flag : bool
            Flag requesting an id_query. Default isTrue.
        reset_flag : bool
            Flag requesting a reset. Default is True.
        reset_options : str
            String containing reset options. Default is None.
        selftest_flag : bool
            Flag requesting an id_query. Default is True.

        Returns
        -------
        int
            Error code.

        """
        warnings.warn(self._name + ' Device _initialize() Method needs to be implemented by derived class.')
        # Example code after opening instrument connection resource
        if id_query_flag:
            self.id_query()
        self.revision_query()
        if reset_flag:
            self.reset(reset_options)
        if selftest_flag:
            self.selftest()
        return -1

    def _reset(self, reset_options=None):
        """
        Reset instrument with options.

        Virtual protected method. Override method needs to be implemented by derived class.

        Parameters
        ----------
        reset_options : str
            String containing reset options. Default is None.

        Returns
        -------
        str
            Reset result.

        """
        warnings.warn(self._name + ' Device _reset() Method needs to be implemented by derived class.')
        return 'NA'

    def _revision_query(self):
        """
        Perform instrument revision query, firmware and hardware.

        Virtual protected method. Override method needs to be implemented by derived class.

        Returns
        -------
        str
            Firmware revision.
        str
            Hardware revision.

        """
        warnings.warn(self._name + ' Device _revision_query() Method needs to be implemented by derived class.')
        return "NA", "NA"

    def _selftest(self):
        """
        Perform instrument selftest.

        Virtual protected method. Override method needs to be implemented by derived class.

        Returns
        -------
        int
            Selftest return code.
        str
            Selftest message.

        """
        warnings.warn(self._name + ' Device _selftest() Method needs to be implemented by derived class.')
        return -1, 'NA'

    def _sn_query(self):
        """
        Perform instrument serial number query.

        Virtual protected method. Override method needs to be implemented by derived class.

        Returns
        -------
        str
            Serial number.

        """
        warnings.warn(self._name + ' Device _sn_query() Method needs to be implemented by derived class.')
        return 'NA'

    # Public methods
    def close(self):
        """
        Set instrument to save mode and close connection.

        Returns
        -------
        int
            Error code.

        """
        pyacdaq_logger.debug(self._name + ' Device close() closing connection.')
        return self._close()

    def id_query(self):
        """
        Perform id query of instrument.

        Returns
        -------
        str
            Instrument ID.
        str
            Serial number.

        """
        pyacdaq_logger.debug(self._name + ' Device id_query() Performing id query...')
        self._instrument_id = self._id_query()
        self._serial_number = self._sn_query()
        if self._mqtt is not None:
            self._mqtt.publish_topic(self._name + '/InstrumentID', self._instrument_id, qos=0, retain=True, expand=False)
            self._mqtt.publish_topic(self._name + '/SerialNumber', self._serial_number, qos=0, retain=True, expand=False)
        return self._instrument_id, self._serial_number

    # Public methods
    def initalize(self,
                  resource='',
                  id_query_flag=True,
                  reset_flag=True,
                  reset_options=None,
                  selftest_flag=True):
        """
        Open connection and initialize instrument.

        Parameters
        ----------
        resource : str
            Resource identifier.
        id_query_flag : bool
            Flag requesting an id_query. Default is True.
        reset_flag : bool
            Flag requesting a reset. Defaultis True.
        reset_options : str
            String containing reset options. Default is None.
        selftest_flag : bool
            Flag requesting an id_query. Default is True.

        Returns
        -------
        None

        """
        try:
            self.close()
        except Exception:
            pyacdaq_logger.debug(self._name + ' Device initialize() Exception during close()')
        pyacdaq_logger.debug(self._name + ' Device initialize() Initializing Resource= ' + resource + ' ID_Query=' + str(id_query_flag) + ' Reset=' + str(reset_flag) + ' Reset_Options=' + reset_options + ' Selftest=' + str(selftest_flag))
        self._initalize(resource, id_query_flag, reset_flag, reset_options, selftest_flag)
        self._resource = resource
        if self._mqtt is not None:
            self._mqtt.publish_topic(self._name + '/Resource', self._resource, qos=0, retain=True, expand=False)
        pass

    def reset(self, reset_options=None):
        """
        Reset instrument with options.

        Parameters
        ----------
        reset_options : str
            String containing reset options. Default is None.

        Returns
        -------
        str
            Reset result.

        """
        pyacdaq_logger.warning(self._name + ' Performing reset with options:' + str(reset_options))
        # print(self._name + ' Performing reset with options:' + str(reset_options))
        self._reset_result = self._reset(reset_options)
        if self._mqtt is not None:
            self._mqtt.publish_topic(self._name + '/ResetResult', self._reset_result, qos=0, retain=True, expand=False)
        return self._reset_result

    def revision_query(self):
        """
        Perform instrument revision query, firmware and hardware.

        Returns
        -------
        str
            Firmware revision.
        str
            Hardware revision.

        """
        pyacdaq_logger.debug(self._name + ' Device revision_query() Performing revision query...')
        self._firmware_revision, self._hardware_revision = self._revision_query()
        if self._mqtt is not None:
            self._mqtt.publish_topic(self._name + '/FirmwareRevision', self._firmware_revision, qos=0, retain=True, expand=False)
            self._mqtt.publish_topic(self._name + '/HardwareRevision', self._hardware_revision, qos=0, retain=True, expand=False)
        return self._firmware_revision, self._hardware_revision

    def selftest(self):
        """
        Perform instrument selftest.

        Returns
        -------
        int
            Selftest return code.
        str
            Selftest message.

        """
        pyacdaq_logger.debug(self._name + ' Device selftest() Performing selftest...')
        self._selftest_code, self._selftest_message = self._selftest()
        pyacdaq_logger.debug(self._name + ' Device selftest() Selftest code=' + str(self._selftest_code) + ' message: ' + self._selftest_message)
        if self._mqtt is not None:
            self._mqtt.publish_topic(self._name + '/SelftestCode', self._selftest_code, qos=0, retain=True, expand=False)
            self._mqtt.publish_topic(self._name + '/SelftestMessage', self._selftest_message, qos=0, retain=True, expand=False)
        return self._selftest_code, self._selftest_message

    def sn_query(self):
        """
        Perform instrument serial number query.

        Returns
        -------
        str
            Serial number.

        """
        pyacdaq_logger.debug(self._name + ' Device sn_query() Performing serial number query...')
        self._serial_number = self._sn_query()
        if self._mqtt is not None:
            self._mqtt.publish_topic(self._name + '/SerialNumber', self._serial_number, qos=0, retain=True, expand=False)
        return self._serial_number

    # Getter
    def getFirmwareRevision(self):
        """Return firmware revision."""
        return self._firmware_revision

    def getHardwareRevision(self):
        """Return hardware revision."""
        return self._hardware_revision

    def getInstrumentID(self):
        """Return instrument ID."""
        return self._instrument_id

    def getResetFlag(self):
        """Return reset flag."""
        return self._reset_flag

    def getResetOptions(self):
        """Return reset options."""
        return self._reset_options

    def getResetResult(self):
        """Return reset result."""
        return self._reset_result

    def getResource(self):
        """Return communication resource."""
        return self._resource

    def getSelftestFlag(self):
        """Return selftest flag."""
        return self._selftest_flag

    def getSelftestCode(self):
        """Return selftest code."""
        return self._selftest_code

    def getSelftestMessage(self):
        """Return selftest message."""
        return self._selftest_message

    def getSerialNumber(self):
        """Return serial number of instrument."""
        return self._serial_number

    pass  # End of class definition


class DeviceGui(ActorGui):
    """
    Base class for derived DeviceGui classes.

    It provides a basic tkinter based GUI for Device.
    """

    def __init__(self, name, start_periodic=False, mqtt=None, associated_actor_name=None, tkgui=None):
        """
        Initialize this instance.

        Parameters
        ----------
        name : str
            Name of instance.
        start_periodic : bool, optional
            Auto start periodic action if true. Default is False.
        mqtt_actor : Mqtt Proxy, optional
            Proxy to MQTT actor. Default is None.
        associated_actor_name : str, optional
            Name of associated actor. The default is None.
        tkgui : DeviceTkGui, optional
            DeviceTkGui instance. The default is None.

        Returns
        -------
        None.

        """
        super().__init__(name, start_periodic, mqtt, associated_actor_name, tkgui)
        self._device_gui_subscriptions = ('Resource', 'InstrumentID', 'SerialNumber', 'FirmwareRevision', 'HardwareRevision', 'ResetResult', 'SelftestCode', 'SelftestMessage', 'Set-Reset', 'Set-Selftest')
        self._device_gui_topics = ('Set-Reset', 'Set-Selftest', )
        self._tkgui.set_reset_cb(self.on_set_reset)
        self._tkgui.set_selftest_cb(self.on_set_selftest)
        pyacdaq_logger.debug(self._name + ' Associated Actor Name' + str(self._associated_actor_name))
        pass

    def __del__(self):
        """Finalize instance."""
        super().__del__()
        pass

    # Protected override methods from super Actor
    def _on_connect(self):
        """
        Called from _on_receive() when message was received from paho.mqtt.client callback message on_connect.

        Perform action on MQTT connect here, e.g. publish and subscribe topics.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' DeviceGui _on_connect() Received MQTT connected message. Publish and subscribe...')
        try:
            if self._mqtt is not None:
                pyacdaq_logger.debug(self._name + ' DeviceGui _on_connect() Publishing:' + str(self._device_gui_subscriptions))
                prefixed_topics = ()
                for topic in self._device_gui_subscriptions:
                    prefixed_topic = self._associated_actor_name + '/' + topic
                    pyacdaq_logger.debug(self._name + ' DeviceGui _on_connect() Publishing:' + str(prefixed_topic))
                    prefixed_topics = prefixed_topics + (prefixed_topic,)
                    self._mqtt.publish_topic(prefixed_topic, '', qos=0, retain=True, expand=False)
                rcs_mids = self._mqtt.subscribe_topics(self._in_future, prefixed_topics, qos=0).get()
                pyacdaq_logger.debug(self._name + ' DeviceGui _on_connect() Subscribing to: ' + str(prefixed_topics) + ' returned rcs, mids = ' + str(rcs_mids))
        except Exception as e:
            pyacdaq_logger.exception(self._name + ' DeviceGui _on_connect(): Exception cought:')
            pyacdaq_logger.exception(self._name + ' DeviceGui _on_connect(): Exception: ' + str(e))
        super()._on_connect()
        pass

    def _on_failure(self, exception_type, exception_value, traceback):
        """
        Hook for doing any cleanup after an unhandled exception is raised, and before the actor stops.

        Virtual protected method. Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        self.close()
        super()._on_failure(exception_type, exception_value, traceback)
        pass

    def _on_message(self, topic, msg):
        """
        Called from Mqtt._on_receive() when message was received from paho.mqtt.client callback message on_message.

        Perform action on MQTT messages here or forward to super.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' DeviceGui _on_message() Received MQTT topic.' + topic + ' msg: ' + str(msg))
        sub_topics = re.split('/', topic)
        if sub_topics[1].find(self._associated_actor_name) >= 0:
            match sub_topics[2]:
                case 'Resource':
                    pyacdaq_logger.debug(self._name + ' DeviceGui _on_message() Resource received, msg: ' + str(msg))
                    self._tkgui.v_device_resource.set('Resource: ' + str(msg))
                case 'InstrumentID':
                    pyacdaq_logger.debug(self._name + ' DeviceGui _on_message() InstrumentID received, msg: ' + str(msg))
                    self._tkgui.v_device_instrument_id.set('ID: ' + str(msg))
                case 'SerialNumber':
                    pyacdaq_logger.debug(self._name + ' DeviceGui _on_message() SerialNumber received, msg: ' + str(msg))
                    self._tkgui.v_device_serial_number.set('SN: ' + str(msg))
                case 'FirmwareRevision':
                    pyacdaq_logger.debug(self._name + ' DeviceGui _on_message() FirmwareRevision received, msg: ' + str(msg))
                    self._tkgui.v_device_firmware_revision.set('FW: ' + str(msg))
                case 'HardwareRevision':
                    pyacdaq_logger.debug(self._name + ' DeviceGui _on_message() HardwareRevision received, msg: ' + str(msg))
                    self._tkgui.v_device_hardware_revision.set('HW: ' + str(msg))
                case 'ResetResult':
                    pyacdaq_logger.debug(self._name + ' DeviceGui _on_message() ResetResult received, msg: ' + str(msg))
                    self._tkgui.v_device_reset_result.set(str('Result: ' + msg))
                case 'SelftestCode':
                    pyacdaq_logger.debug(self._name + ' ActorGui _on_message() SelftestCode received, msg: ' + str(msg))
                    self._tkgui.v_device_selftest_code.set('ST Code: ' + str(msg))
                case 'SelftestMessage':
                    pyacdaq_logger.debug(self._name + ' DeviceGui _on_message() SelftestMessage received, msg: ' + str(msg))
                    self._tkgui.v_device_selftest_message.set('ST Msg: ' + str(msg))
                case 'Set-Reset':
                    pyacdaq_logger.debug(self._name + ' DeviceGui _on_message() Set-Reset received, msg: ' + str(msg))
                    self._tkgui.v_device_set_reset.set(msg.lower().find('true') >= 0)
                case 'Set-Selftest':
                    pyacdaq_logger.debug(self._name + ' DeviceGui _on_message() Set-Selftest received, msg: ' + str(msg))
                    self._tkgui.v_device_set_selftest.set(msg.lower().find('true') >= 0)
                case _:  # Call super
                    pyacdaq_logger.debug(self._name + ' DeviceGui _on_message() Calling super() due for unknown topic: ' + topic + ' msg: ' + str(msg))
                    super()._on_message(topic, msg)
        else:
            pyacdaq_logger.debug(self._name + ' DeviceGui _on_message() Calling super() due for unknown topic: ' + topic + ' msg: ' + str(msg))
            super()._on_message(topic, msg)
        pass

    def _on_receive(self, message):
        """
        May be implemented for the actor to handle regular non-proxy messages or forward to super.

        Virtual protected method. Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        warnings.warn(self._name + ' DeviceGui _on_receive() Method needs to be implemented by derived class.')
        super()._on_receive(message)
        pass

    def _on_start(self):
        """
        Hook for doing any setup that should be done after the actor is started, but before it starts processing messages.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        None.

        """
        super()._on_start()
        pass

    def _on_stop(self):
        """
        Hook for doing any cleanup that should be done after the actor has processed the last message, and before the actor stops.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' DeviceGui _on_stop()')
        try:
            if self._mqtt is not None:
                prefixed_topics = ()
                # print(self._name + ' DeviceGui _on_stop() Unsubscribing: ' + str(self._device_gui_subscriptions))
                pyacdaq_logger.debug(self._name + ' DeviceGui _on_stop() Unsubscribing: ' + str(self._device_gui_subscriptions))
                for topic in self._device_gui_subscriptions:
                    prefixed_topic = self._name + '/' + topic
                    prefixed_topics = prefixed_topics + (prefixed_topic,)
                # print(self._name + ' DeviceGui _on_stop() Unsubscribing from: ' + str(prefixed_topics))
                pyacdaq_logger.debug(self._name + ' DeviceGui _on_stop() Unsubscribing from: ' + str(prefixed_topics))
                self._mqtt.unsubscribe_topics(self, prefixed_topics)
        except Exception as e:
            pyacdaq_logger.exception(self._name + ' DeviceGui _on_stop(): Exception cought:')
            pyacdaq_logger.exception(self._name + ' DeviceGui _on_stop(): Exception: ' + str(e))
        super()._on_stop()
        pass

    def _periodic(self):
        """
        Implementation of periodic actions.

        Virtual protected method. Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        warnings.warn(self._name + ' DeviceGui _periodic() Method needs to be implemented by derived class.')
        super()._periodic()
        pass

    def _sw_revision(self):
        """
        Return revision of this class inclusive parents.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        str
            Software revision.

        """
        return 'DeviceGui_0.1.0.0|' + super()._sw_revision()

    # Public methods
    def on_set_reset(self):
        """Handle actor_set_reset = tk.Checkbutton()."""
        reset = self._tkgui.v_device_set_reset.get()
        pyacdaq_logger.debug(self._name + ' DeviceGui on_set_reset():' + str(reset))
        if self._mqtt is not None:
            self._mqtt.publish_topic(self._associated_actor_name + '/Set-Reset', reset, qos=0, retain=True, expand=False)
        pass

    def on_set_selftest(self):
        """Handle actor_set_selftest = tk.Checkbutton()."""
        selftest = self._tkgui.v_device_set_selftest.get()
        pyacdaq_logger.debug(self._name + ' DeviceGui on_set_selftest():' + str(selftest))
        if self._mqtt is not None:
            self._mqtt.publish_topic(self._associated_actor_name + '/Set-Selftest', selftest, qos=0, retain=True, expand=False)
        pass

    pass  # End of class definition


if __name__ == '__main__':
    print("device.py\n\
Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme")
    app_name = re.split('.py', os.path.basename(__file__))[0]
    # Logging setup
    print('Configuring logging...')
    # Define logger to be used
    # logging.basicConfig(  # filename='example.log', filemode='w', encoding='utf-8', level=logging.INFO, style='{', format='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    logger = logging.getLogger('App-Logger')
    logger.setLevel(logging.DEBUG)
    pyacdaq_logger = logging.getLogger('pyacdaq')
    pyacdaq_logger.setLevel(logging.DEBUG)
    # Define logging formatter
    logging_formatter = logging.Formatter(style='{', fmt='{asctime} [{levelname:8}] {module} {funcName} {lineno} {message}')
    # logging_formatter = logging.Formatter(style='{', fmt='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    # configure_logging_handler((logger, pyacdaq_logger), logging_formatter, severity={'console': None, 'file': None, 'mqtt': None, 'syslog': None}, mqtt={'broker': 'ee-raspi1001', 'topic': 'ACDAQ/log_msg', 'user': '', 'password': ''})

    try:
        mqtt_broker = input('MQTT Broker? (localhost)>')
        if len(mqtt_broker) == 0:
            mqtt_broker = 'localhost'
        mqtt_user = input('MQTT Username: ')
        if len(mqtt_user) == 0:
            mqtt_user = None
        mqtt_password = getpass.getpass()
        if len(mqtt_password) == 0:
            mqtt_password = None
        configure_logging_handler(app_name, (logger, pyacdaq_logger), logging_formatter, severity={'console': logging.INFO, 'file': logging.DEBUG, 'mqtt': logging.INFO, 'syslog': logging.WARNING}, mqtt={'broker': mqtt_broker, 'topic': 'ACDAQ/log_msg', 'user': mqtt_user, 'password': mqtt_password})
        logger.info('Please, wait for MQTT actor.')
        # Create MQTT actor proxy
        ma = Mqtt.start('Mqtt',
                        start_periodic=False,
                        broker=mqtt_broker,
                        port=1883,
                        client_id='ACDAQ',
                        clean_session=True,
                        retain=True,
                        will='offline',
                        username=mqtt_user,
                        password=mqtt_password
                        ).proxy()
        # Launch GUI
        logger.info('Please, wait for device GUI.')
        time.sleep(3)
        root = tk.Tk()
        device_tkgui = DeviceTkGui(root)
        type(device_tkgui)
        # Create Gui actor proxy
        device_gui = DeviceGui.start('DeviceGui',
                                     start_periodic=False,
                                     mqtt=ma,
                                     associated_actor_name='Device',
                                     tkgui=device_tkgui
                                     ).proxy()
        # Create device proxy
        logger.info('Please, wait for device.')
        time.sleep(5)
        device = Device.start('Device',
                              start_periodic=False,
                              mqtt=ma,
                              resource='COM3',
                              id_query_flag=True,
                              reset_flag=False,
                              reset_options='myResetOptions',
                              selftest_flag=False
                              ).proxy()
        device_name = device.getName().get()
        device_birthday = device.getBirthday().get()
        device_periodic = device.getPeriodicStatus().get()
        device_sw_revision = device.getSWRevision().get()
        device_instrument_id = device.getInstrumentID().get()
        device_firmware_revision = device.getFirmwareRevision().get()
        device_hardware_revision = device.getHardwareRevision().get()
        device_serial_number = device.getSerialNumber().get()
        print(__name__, 'Name:', device_name,
              '; Birthday:', device_birthday,
              '; Periodic:', device_periodic,
              '; SW:', device_sw_revision,
              '; ID:', device_instrument_id,
              '; FW:', device_firmware_revision,
              '; HW:', device_hardware_revision,
              '; SN:', device_serial_number)
        # Start the Tkinter GUI Loop
        device_tkgui.mainloop()
    except Exception as e:
        logger.exception('_main__: Exception caught:', str(e))
    finally:
        device_gui.stop()
        time.sleep(3)
        device.stop()
        time.sleep(3)
        ma.stop()
    logging.shutdown()
    print(__name__, 'Done.')
