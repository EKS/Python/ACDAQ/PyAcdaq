#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This module contains pyacdaq actor and helper classes and associated tkinter GUI'.s

Actor classes
-------------
actor.Actor(pykka.ThreadingActor)
    Actor is the base class for active objects.
actor.Device(Actor)
    Base class for derived device classes.
Mqtt(Actor)
    Actor class providing an interface to MQTT using the Paho library.

Associated GUIActor classes
---------------------------
actor.ActorGui(Actor)
    Base class for derived GUI classes in py_acdaq.
actor.DeviceGui(ActorGui):
    Base class for derived DeviceGui classes

Classes
-------
actor.PeriodicCallReason(enum.Enum)
    Enumeration of reasons for calling Actor._periodic()
actor.tkguis.ActorTkGui(tk.Frame)
    GUI class definition for Actor.
actor.tkguis.DeviceTkGui(ActorTkGui)
    GUI class definition for Device.

Lizenziert unter EUPL V. 1.2

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
