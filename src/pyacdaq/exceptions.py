#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This module contains exceptions maybe raised by DeviceActors.

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""


class CommunicationTimeout(TimeoutError):
    """
    Derived class of Exception: TimeoutError.

    This exception is maybe raised by DeviceActors.
    """

    def __init__(self, command) -> None:
        self.command = command


class ExitApplication(Exception):
    """Exception is maybe raised due to user request."""


if __name__ == '__main__':
    print("exceptions.py\n\
Copyright 2024 Brand New Technologies\n\
Dr. Holger Brand, Asternweg 12a, 64291 Darmstadt, Germany\n\
eMail: Holger.Brand@BrandNewTechnologies.de\n\
Web: https//www.brandnewtechnolgies.de\n")
    try:
        raise ExitApplication('Test ExitApplication exception.')
    except Exception as ex:
        print(ex)
    print(__name__, 'Done.')

if __name__ == '__main__':
    print("exceptions.py\n\
Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme\n")
    raise CommunicationTimeout('Test CommunicationTimeout exception.')
