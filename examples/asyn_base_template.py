#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Module contains a template _MyAsynBase_ class for derived classes of _AsynBase_.

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
import asyncio
import argparse
import getpass
import inspect
import logging
import os
import re
import sys
from typing import Any, override
import pint
# from abc import abstractmethod
import aiomqtt
import nest_asyncio
from pyacdaq.asyn.asyn_base import ureg, AsynBase

# You may choose a different logger
pyacdaq_logger: logging.Logger = logging.getLogger('pyacdaq')


class MyAsynBase(AsynBase):
    """
    Derived classes of AsynBase supporting asyncio.

    It provides basic attributes and methods for:
    - asyncio task for handling of commands received by queue,
    - asyncio task for periodic actions,
    - asyncio task to subscribe to MQTT topics,
    - publish MQTT topics.

    For details refer to:
        https://git.gsi.de/EKS/Python/ACDAQ/PyAcdaq/-/blob/master/src/pyacdaq/asyn/asyn_base.py?ref_type=heads

    """

    def __init__(self,
                 name: str,
                 mqtt_client: aiomqtt.Client,
                 top_level_topic: str
                 ) -> None:
        """
        Initialize this instance.

        Parameters
        ----------
        name : str
            Name of instance.
        mqtt_client : aiomqtt.Client
            mqtt client connection.
        top_level_topic : str
            Name of top_level_topic to be uses as prefix.

        Returns
        -------
        None
        """
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        if not isinstance(name, str):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid name.')
        if not isinstance(mqtt_client, aiomqtt.Client):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid mqtt_client.')
        if not isinstance(top_level_topic, str):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid parameter.')
        super().__init__(name, mqtt_client, top_level_topic)
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Initializing self.')
        self._parameter: int = -1
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Initialized.')

    def __del__(self) -> None:
        """Finalize this instance."""
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{fn}: {self._name} Finalizing self.')
        super().__del__()

    # Private methods

    # Protected methods
    @override
    def _command_help(self) -> str:
        """Return help text for command line interface as defined in derived class override method."""
        help_text: str = \
            '  command parameter:int.\n'
        return help_text

    async def _handle_command(self, q_item: list[Any]) -> None:
        """Handle command received from queue."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        if not isinstance(q_item, list):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid q_item.')
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Handle command: {q_item}')
        match q_item[0]:
            case 'command':
                try:
                    pyacdaq_logger.info(f'{tn}.{cn}.{fn}: {self._name} command with {q_item[1]=}.')
                    await self.command(q_item[1])
                except Exception as ex:
                    pyacdaq_logger.warning(f'{tn}.{cn}.{fn}: {self._name} Command command ignored. {ex=}')
            case _:
                await super()._handle_command(q_item)

    async def _handle_subscriptions(self, topic_levels: list[str], payload: str) -> None:
        """Handle subscribed topics."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        if not isinstance(topic_levels, list):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid topic_levels.')
        if not isinstance(payload, str):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid payload.')
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Handle subscribed topics.')
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} {topic_levels=}:{payload=!r}')
        match topic_levels[1]:
            case _ if topic_levels[1] == self._name:
                match topic_levels[2]:
                    case _ if topic_levels[2] == 'set':
                        match topic_levels[3]:
                            case _ if topic_levels[3] == 'command':
                                try:
                                    pyacdaq_logger.info(f'{tn}.{cn}.{fn}: {self._name} command with {payload=}.')
                                    await self._command_queue.put(['command', int(payload)])
                                except Exception as ex:
                                    pyacdaq_logger.warning(f'{tn}.{cn}.{fn}: {self._name} command with {payload=} ignored. {str(ex)}')
                                    await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/log_msg', payload=f'command with {payload=} ignored. {str(ex)}', retain=True)
                            case _:
                                await super()._handle_subscriptions(topic_levels, payload)  # match topic_level[3]
                    case _:
                        await super()._handle_subscriptions(topic_levels, payload)  # match topic_level[2]
            case _:
                await super()._handle_subscriptions(topic_levels, payload)  # match topic_level[1]

    @override
    async def _periodic_action(self) -> None:
        """Implement periodic actions in derived class override method."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Perform periodic actions.')
        await super()._periodic_action()

    @override
    async def _periodic_start(self) -> None:
        """Implement periodic start actions in derived class override method."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Perform periodic start actions.')
        await super()._periodic_start()

    @override
    async def _periodic_stop(self) -> None:
        """Implement periodic stop actions in derived class override method."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Perform periodic stop actions.')
        await super()._periodic_stop()

    @override
    async def _publish_topics(self) -> None:
        """Publish topics in derived class override method."""
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Publish topics.')
        await super()._publish_topics()
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/parameter', payload=self._parameter, retain=True)
        await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/set/command', payload=self._parameter, retain=False)

    @override
    async def _start(self, subscribe_task: bool = False, periodic_interval: pint.Quantity = -1. * ureg.s) -> None:
        """
        Perform start actions in derived class override method.

        Parameters
        ----------
        subscribe: bool, optional
            Start subcriber task if subscribe is True. The default is False.
        periodic_interval: pint.Quantity, optional
            Start periodic task if greater or equal 0. The default is -1s.

        Returns
        -------
        None
        """
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        if not isinstance(subscribe_task, bool):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid subscribe_task.')
        if not isinstance(periodic_interval, pint.Quantity):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid periodic_interval.')
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Perform start actions.')
        await super()._start(subscribe_task, periodic_interval)
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Performing start actions done.')

    @override
    async def _stop(self) -> None:
        """
        Perform stop actions in derived class override method.

        Returns
        -------
        None
        """
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Perform stop actions.')
        await super()._stop()
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Performing stop actions done.')

    @override
    async def _subscribe_topics(self) -> list[str]:
        """
        Define topics to subscribe in derived class override method.

        Returns
        -------
        list[str]
            Topics to subscribe.
        """
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        pyacdaq_logger.debug(f'{tn}.{cn}.{fn}: {self._name} Subscribe topics.')
        topics: set[str] = await super()._subscribe_topics()
        topics.add(f'{self._top_level_topic}/{self._name}/set/command')
        return topics

    @override
    def _sw_revision(self) -> str:
        """
        Revision of this class inclusive parents.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        string
            Software version
        """
        return 'MyAsyncBase_0.0.0.0' + '|' + super()._sw_revision()

    # Public methods
    async def command(self, parameter: int = 0) -> int:
        """
        Apply command.

        Parameters
        ----------
        parameter : int, optional
            Set parameter. Default is 0.

        Returns
        -------
        parameter: int
            Parameter.

        """
        tn: str = asyncio.current_task().get_name()
        cn: str = asyncio.current_task().get_coro().cr_code.co_name
        fn: str = inspect.currentframe().f_code.co_name
        if not isinstance(parameter, int):
            raise TypeError(f'{tn}.{cn}.{fn}: {self._name}: Invalid parameter.')
        pyacdaq_logger.info(f'{tn}.{cn}.{fn}: {self._name} Execute command with {parameter=}')
        try:
            self._parameter = parameter
            await self._mqtt_client.publish(f'{self._top_level_topic}/{self._name}/parameter', payload=self._parameter, retain=True)
        except Exception as ex:
            pyacdaq_logger.exception(f'{tn}.{cn}.{fn}: {self._name} Exception cought. {ex}')
        return self._parameter

    # Getter
    def get_parameter(self) -> int:
        """Get property parameter."""
        return self._parameter

    # Setter
    def set_parameter(self, parameter: int = 0) -> int:
        """Get property parameter."""
        fn: str = inspect.currentframe().f_code.co_name
        if not isinstance(parameter, int):
            pyacdaq_logger.exception(f'{fn}: {self._name} parameter need to be of type: int.')
        self._parameter = parameter
        return self._parameter

    # Properties
    Parameter = property(get_parameter)


async def do_something(mqtt_client: aiomqtt.Client, top_level_topic: str):
    """Do something within mqtt client context."""
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    if not isinstance(top_level_topic, str):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid top_level_topic.')
    try:
        print(f'{tn}.{cn}.{fn}: Instantiate and start.')
        ab_0: MyAsynBase = MyAsynBase('myAsynBase_0', mqtt_client, top_level_topic)
        print(f'{tn}.{cn}.{fn}: {ab_0.Name=}, {str(ab_0.Birthday)=}, {ab_0.SWRevision=}, {ab_0.PeriodicInterval=}')
        print(f'{tn}.{cn}.{fn}: Command_help\n{ab_0.command_help()}')
        print(f'{tn}.{cn}.{fn}: {await ab_0.start(subscribe_task=True, periodic_interval=-1. * ureg.s)=}')
        print(f'{tn}.{cn}.{fn}: await asyncio.sleep(1.)')
        await asyncio.sleep(1.)
        if False:
            print(f'{tn}.{cn}.{fn}: Use methods for manipulation.')
            print(f'{tn}.{cn}.{fn}: {ab_0.PeriodicActive=} {ab_0.PeriodicInterval=} {ab_0.PeriodicCounter=}')
            print(f'{tn}.{cn}.{fn}: {await ab_0.start_periodic(1. * ureg.s)=}')
            print(f'{tn}.{cn}.{fn}: await asyncio.sleep(1.)')
            await asyncio.sleep(1.)
            print(f'{tn}.{cn}.{fn}: {await ab_0.command(1234)=}')
            print(f'{tn}.{cn}.{fn}: {ab_0.PeriodicActive=} {ab_0.PeriodicInterval=} {ab_0.PeriodicCounter=}')
            print(f'{tn}.{cn}.{fn}: await asyncio.sleep(10.)')
            await asyncio.sleep(10.)
            print(f'{tn}.{cn}.{fn}: {ab_0.PeriodicActive=} {ab_0.PeriodicInterval=} {ab_0.PeriodicCounter=}')
            print(f'{tn}.{cn}.{fn}: {await ab_0.set_periodic_interval(2. * ureg.s)=}')
            print(f'{tn}.{cn}.{fn}: await asyncio.sleep(10.)')
            await asyncio.sleep(10.)
            print(f'{tn}.{cn}.{fn}: {ab_0.PeriodicActive=} {ab_0.PeriodicInterval=} {ab_0.PeriodicCounter=}')
            print(f'{tn}.{cn}.{fn}: {await ab_0.stop_periodic()=}')
            print(f'{tn}.{cn}.{fn}: await asyncio.sleep(10.)')
            await asyncio.sleep(10.)
            print(f'{tn}.{cn}.{fn}: {ab_0.PeriodicActive=} {ab_0.PeriodicInterval=} {ab_0.PeriodicCounter=}')
            print(f'{tn}.{cn}.{fn}: Use queue for manipulation.')
            ab_0_cq: asyncio.Queue = ab_0.CommandQueue
            print(f'{tn}.{cn}.{fn}: {ab_0.PeriodicActive=} {ab_0.PeriodicInterval=} {ab_0.PeriodicCounter=}')
            print(f'{tn}.{cn}.{fn}: {await ab_0_cq.put(["periodic_start", 1. * ureg.s])=}')
            print(f'{tn}.{cn}.{fn}: await asyncio.sleep(1.)')
            await asyncio.sleep(1.)
            print(f'{tn}.{cn}.{fn}: {ab_0.PeriodicActive=} {ab_0.PeriodicInterval=} {ab_0.PeriodicCounter=}')
            print(f'{tn}.{cn}.{fn}: await asyncio.sleep(10.)')
            await asyncio.sleep(10.)
            print(f'{tn}.{cn}.{fn}: {await ab_0_cq.put(["command", 2345])=}')
            print(f'{tn}.{cn}.{fn}: {ab_0.PeriodicActive=} {ab_0.PeriodicInterval=} {ab_0.PeriodicCounter=}')
            print(f'{tn}.{cn}.{fn}: {await ab_0_cq.put(["periodic_interval", 2. * ureg.s])=}')
            print(f'{tn}.{cn}.{fn}: await asyncio.sleep(10.)')
            await asyncio.sleep(10.)
            print(f'{tn}.{cn}.{fn}: {ab_0.PeriodicActive=} {ab_0.PeriodicInterval=} {ab_0.PeriodicCounter=}')
            print(f'{tn}.{cn}.{fn}: {await ab_0_cq.put(["periodic_stop"])=}')
            print(f'{tn}.{cn}.{fn}: await asyncio.sleep(10.)')
            await asyncio.sleep(10.)
            print(f'{tn}.{cn}.{fn}: {ab_0.PeriodicActive=} {ab_0.PeriodicInterval=} {ab_0.PeriodicCounter=}')
        print('Waiting for topic changes...')
        while True:
            await asyncio.sleep(1.)
    finally:
        print(f'{tn}.{cn}.{fn}: Stop and delete')
        try:
            if ab_0:
                await ab_0.stop()
        finally:
            if ab_0:
                del ab_0


async def main() -> None:
    """Implement yout test code here."""
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    parser = argparse.ArgumentParser(prog=os.path.basename(__file__),
                                     description='This is the async_base_template example.',
                                     epilog='Default command: python asyn.asyn_base_template -b localhost -p 1883')
    parser.add_argument('-b', '--broker', dest='broker', required=False, default='localhost', help='MQTT Broker Node (Name or IP)')
    # parser.add_argument('-b', '--broker', dest='broker', required=True, default=None, help='MQTT Broker Node (Name or IP)')
    parser.add_argument('-p', '--port', dest='port', type=int, default=1883, help='MQTT Broker port (Default:1883)')
    parser.add_argument('--mqtt_user', dest='mqtt_user', default=None, help='MQTT Broker User Name')
    parser.add_argument('--top_level_topic', dest='top_level_topic', default='PyACDAQ', help='MQTT top-level topic. Default is "PyACDAQ")')
    args = parser.parse_args()
    while True:
        answer: str = input('THIS IS BETA SOFTWARE! You will use it on your own risk!\nDo you want to continue (yes or default: no)? ')
        if re.match('yes', answer):
            break
        if len(answer) == 0 or re.match('no', answer):
            sys.exit(1)
    mqtt_password: str | None = None
    if args.mqtt_user:  # but required
        print('Set password for MQTT')
        mqtt_password = getpass.getpass()
        if len(mqtt_password) == 0:
            sys.exit(-2)
    mqtt: dict[str, int | str | None] = {'broker': args.broker,
                                         'port': args.port,
                                         'user': args.mqtt_user,
                                         'password': mqtt_password}
    try:
        async with aiomqtt.Client(hostname=mqtt['broker'],
                                  port=mqtt['port'],
                                  username=mqtt['user'],
                                  password=mqtt['password'],
                                  will=aiomqtt.Will(topic=args.top_level_topic, payload='disconnected', retain=True),
                                  clean_session=True
                                  ) as mqtt_client:
            await mqtt_client.publish(args.top_level_topic, payload='online', retain=True)
            await mqtt_client.publish(args.top_level_topic + '/log_msg', payload='', retain=True)
            await do_something(mqtt_client, args.top_level_topic)
            await mqtt_client.publish(args.top_level_topic, payload='offline', retain=True)
    except Exception as ex:
        pyacdaq_logger.exception(f'{tn}.{cn}.{fn}: Stopping due to exception: {ex}')


if __name__ == '__main__':
    print(os.path.basename(__file__) + "\n\
Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme\n")
    logging.basicConfig(filename='./pyacdaq.log', filemode='w', encoding='utf-8', level=logging.DEBUG, style='{', format='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    pyacdaq_logger.setLevel(logging.INFO)
    nest_asyncio.apply()
    # Change to the "Selector" event loop if platform is Windows
    if sys.platform.lower() == "win32" or os.name.lower() == "nt":
        from asyncio import set_event_loop_policy, WindowsSelectorEventLoopPolicy
        set_event_loop_policy(WindowsSelectorEventLoopPolicy())
    # Run your async application as usual
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print('Script interrupted by <Ctr+C>')
    except asyncio.exceptions.CancelledError:
        pass
    except BaseException as e:
        # print(f'__main__: BaseException cought. {type(e)} {e}')
        print(f'__main__: BaseException cought. {type(e)}')
        pass
    logging.shutdown()
    print(__name__, 'Done.')
