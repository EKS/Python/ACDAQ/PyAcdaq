#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Derived actor class template.

- Replace DerivedActor with you class name.
- Replace derived_actor with instance name.

Lizenziert unter EUPL V. 1.2

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme

- Known issues
"""

import getpass
import logging
import os
import re
import time
import warnings
from pyacdaq.logger import configure_logging_handler
from pyacdaq.actor.actor import Actor as Actor
from pyacdaq.actor.actor import PeriodicCallReason as PeriodicCallReason
from pyacdaq.actor.mqtt import Mqtt as Mqtt

derived_actor_logger = logging.getLogger('DerivedActor').addHandler(logging.NullHandler())


class DerivedActor(Actor):
    """Derived actor class template.

    It extends/overrides virtual methods of its super class and
    adds derived actor specific attributes and methods.
    """

    def __init__(self, name,
                 start_periodic=False,
                 mqtt=None,
                 ):
        """
        Initialize this instance.

        Parameters
        ----------
        name : str
            Name of instance.
        start_periodic : bool, optional
            Auto start periodic action if true. Default is False.
        mqtt_actor : Mqtt Proxy, optional
            Proxy to MQTT actor. Default is None.

        Returns
        -------
        None.

        """
        self._derived_actor_subscriptions = ('Set-Command', )
        super().__init__(name, start_periodic, mqtt)
        pass

    def __del__(self):
        """Finalize instance."""
        super().__del__()
        pass

    # Private methods

    # Protected override methods from super
    def _on_connect(self):
        """
        Called from _on_receive() when message was received from paho.mqtt.client callback message on_connect.

        Perform action on MQTT disconnect here, e.g. publish and subscribe topics.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        None.

        """
        derived_actor_logger.debug(self._name + ' DerivedActor _on_connect() Received MQTT connected message. Publish and subscribe...')
        try:
            if self._mqtt is not None:
                derived_actor_logger.debug(self._name + ' DerivedActor _on_connect() Publishing:' + str(self._derived_actor_subscriptions))
                prefixed_topics = ()
                for topic in self._derived_actor_subscriptions:
                    prefixed_topic = self._name + '/' + topic
                    derived_actor_logger.debug(self._name + ' DerivedActor _on_connect() Publishing:' + str(prefixed_topic))
                    prefixed_topics = prefixed_topics + (prefixed_topic,)
                    self._mqtt.publish_topic(prefixed_topic, '', qos=0, retain=True, expand=False)
                derived_actor_logger.debug(self._name + ' DerivedActor _on_connect() Subscribing to: ' + str(prefixed_topics))
                rcs_mids = self._mqtt.subscribe_topics(self._in_future, prefixed_topics, qos=0).get()
                derived_actor_logger.debug(self._name + ' DerivedActor _on_connect() Subscribing to: ' + str(prefixed_topics) + ' returned rcs, mids = ' + str(rcs_mids))
        except Exception as e:
            derived_actor_logger.exception(self._name + ' DerivedActor _on_connect(): Exception cought:')
            derived_actor_logger.eception(self._name + ' DerivedActor _on_connect(): Exception: ' + str(e))
        super()._on_connect()
        pass

    def _on_disconnect(self):
        """
        Called from _on_receive() when message was received from paho.mqtt.client callback message on_disconnect.

        Perform action on MQTT disconnect here.

        Virtual protected method. Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        pyacdaq_logger.debug(self._name + ' DerivedActor _on_disconnect() Received MQTT disconnected message.')
        pass

    def _on_failure(self, exception_type, exception_value, traceback):
        """
        Hook for doing any cleanup after an unhandled exception is raised, and before the actor stops.

        Virtual protected method. Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        warnings.warn(self._name + ' DerivedActor _on_failure() Method needs to be implemented by derived class.')
        pass

    def _on_message(self, topic, msg):
        """
        Called from AcdaqMqtt._on_receive() when message was received from paho.mqtt.client callback message on_message.

        Perform action on MQTT messages here.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        None.

        """
        derived_actor_logger.debug(self._name + ' DerivedActor _on_message() Received MQTT topic.' + topic + ' msg: ' + str(msg))
        sub_topics = re.split('/', topic)
        derived_actor_logger.debug(self._name + ' DerivedActor _on_message() Received MQTT sub-topics: ' + str(sub_topics))
        match sub_topics[2]:
            case 'Set-Command':
                try:
                    print(self._name + ' DerivedActor _on_message() Set-Command received, msg=' + str(msg.strip()))
                except Exception as e:
                    derived_actor_logger.exception(self._name + ' _on_message() Set-Command ignored due to exception: ' + str(e))
            case _:  # Call super
                derived_actor_logger.debug(self._name + ' DerivedActor _on_message() Calling super() due for unknown topic: ' + topic + ' msg: ' + str(msg))
                super()._on_message(topic, msg)
        pass

    def _on_receive(self, message):
        """
        May be implemented for the actor to handle regular non-proxy messages.

        Virtual protected method. Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        derived_actor_logger.info(self._name + ' DerivedActor _on_receive() call super()._on_receive(message)')
        super()._on_receive(message)
        pass

    def _on_start(self):
        """
        Hook for doing any setup that should be done after the actor is started, but before it starts processing messages.

        Virtual protected method. Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        derived_actor_logger.info(self._name + ' DerivedActor _on_start() call super()._on_start()')
        super()._on_start()
        pass

    def _on_stop(self):
        """
        Hook for doing any cleanup that should be done after the actor has processed the last message, and before the actor stops.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        None.

        """
        derived_actor_logger.info(self._name + ' DerivedActor _on_stop()')
        try:
            if self._mqtt is not None:
                prefixed_topics = ()
                derived_actor_logger.debug(self._name + ' DerivedActor _on_stop() Unsubscribing: ' + str(self._derived_actor_subscriptions))
                for topic in self._derived_actor_subscriptions:
                    prefixed_topic = self._name + '/' + topic
                    prefixed_topics = prefixed_topics + (prefixed_topic,)
                derived_actor_logger.debug(self._name + ' DerivedActor _on_stop() Unsubscribing from: ' + str(prefixed_topics))
                self._mqtt.unsubscribe_topics(self, prefixed_topics)
        except Exception as e:
            derived_actor_logger.exception(self._name + ' DerivedActor _on_stop(): Exception cought:')
            derived_actor_logger.exception(self._name + ' DerivedActor _on_stop(): Exception: ' + str(e))
        super()._on_stop()
        pass

    def _periodic(self, reason):
        """
        Implementation of periodic actions.

        Virtual protected method. Override with concrete child class implementation.

        Parameters
        ----------
        reason : PeriodicCallReason
            Reason for calling this method.

        Returns
        -------
        None.

        """
        warnings.warn(self._name + ' DerivedActor _periodic() Method needs to be implemented by derived class.')
        match reason:
            case PeriodicCallReason.Start:
                pass
            case PeriodicCallReason.Do:
                time.sleep(1)
                pass
            case PeriodicCallReason.Stop:
                pass
        pass

    # Protected override methods from super Device
    def _sw_revision(self):
        """
        Return revision of this class inclusive parents.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        string
            Software version

        """
        return 'DerivedActor_0.0.0.0|' + super()._sw_revision()

    # Public methods of this class

    # Getter

    pass  # End of class definition


if __name__ == '__main__':
    app_name = re.split('.py', os.path.basename(__file__))[0]
    print(app_name + "\n\
Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme")
    # Logging setup
    print('Configuring logging...')
    # Define logger to be used
    # logging.basicConfig(  # filename='example.log', filemode='w', encoding='utf-8', level=logging.INFO, style='{', format='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    logger = logging.getLogger('App-Logger')
    logger.setLevel(logging.DEBUG)
    pyacdaq_logger = logging.getLogger('pyacdaq')
    pyacdaq_logger.setLevel(logging.DEBUG)
    derived_actor_logger = logging.getLogger('DerivedActor')
    derived_actor_logger.setLevel(logging.DEBUG)
    # Define logging formatter
    logging_formatter = logging.Formatter(style='{', fmt='{asctime} [{levelname:8}] {module} {funcName} {lineno} {message}')
    # logging_formatter = logging.Formatter(style='{', fmt='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    # configure_logging_handler((logger, pyacdaq_logger), logging_formatter, severity={'console': None, 'file': None, 'mqtt': None, 'syslog': None}, mqtt={'broker': 'ee-raspi1001', 'topic': app_name + '/log_msg', 'user': '', 'password': ''})

    mqtt_broker = input('MQTT Broker? (default: localhost)>')
    if len(mqtt_broker) == 0:
        mqtt_broker = 'localhost'
    mqtt_user = input('MQTT Username: ')
    if len(mqtt_user) == 0:
        mqtt_user = None
    mqtt_password = getpass.getpass()
    if len(mqtt_password) == 0:
        mqtt_password = None
    configure_logging_handler(app_name, (logger, pyacdaq_logger, derived_actor_logger), logging_formatter, severity={'console': logging.INFO, 'file': logging.DEBUG, 'mqtt': logging.INFO, 'syslog': logging.WARNING}, mqtt={'broker': mqtt_broker, 'topic': 'DerivedActor/log_msg', 'user': mqtt_user, 'password': mqtt_password})
    print('Please, wait for MQTT-Actor.')
    try:
        # Create MQTT actor proxy
        ma = Mqtt.start('Mqtt',
                        start_periodic=False,
                        broker=mqtt_broker,
                        port=1883,
                        client_id='DerivedActor',
                        clean_session=True,
                        retain=True,
                        will='offline',
                        username=mqtt_user,
                        password=mqtt_password
                        ).proxy()
        # Create hvs proxy
        print('Please, wait for derived_actor-Actor.')
        time.sleep(3)
        a_derived_actor = DerivedActor.start('derived_actor',
                                             start_periodic=False,
                                             mqtt=ma,
                                             )
        p_derived_actor = a_derived_actor.proxy()
        derived_actor_name = p_derived_actor.getName().get()
        derived_actor_birthday = p_derived_actor.getBirthday().get()
        derived_actor_periodic = p_derived_actor.getPeriodicStatus().get()
        derived_actor_sw_revision = p_derived_actor.getSWRevision().get()
        print(__name__, 'Name:', derived_actor_name,
              '; Birthday:', derived_actor_birthday,
              '; Periodic:', derived_actor_periodic,
              '; SW:', derived_actor_sw_revision)
        time.sleep(15)
    except Exception as e:
        print('_main__: Exception caught:', e)
    finally:
        p_derived_actor.stop()
        time.sleep(3)
        ma.stop()
    logging.shutdown()
    print(__name__, 'Done.')
