#!/usr/bin/env python
"""
This program can be used to simulate an scpi device with with ethernet interface.
It is the counterpart for ScpiDevSim class.
It was designed to be executed on a Raspberry Pi,
but could also run on Linux and Windows.

It will listen on localhost, port 12345 by default.

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""

import random
import re
import socket

host = '127.0.0.1'
port = 12345

random.seed()
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((host, port))
    s.listen(1)
    while True:  # Listen forever
        print('Listening to Port: {0} at {1}'.format(port, host))
        conn, addr = s.accept()
        with conn:
            print('Connected by ', addr)
            high = 10
            low = 0
            counter = 0
            command = ''
            response = ''
            disconnected = False
            while True:
                counter += 1
                line = conn.recv(1024)
                if not line:
                    break
                else:
                    print('Line:', line)
                    command = re.split(b' ', line)
                    cmd = command[0].strip()
                    print('Command', cmd)
                    if cmd != '':
                        if cmd == b'FW?':
                            response = b'FW_0.1\n'
                        elif cmd == b'HW?':
                            response = b'HW_0.1\n'
                        elif cmd == b'SN?':
                            response = b'1234\n'
                        elif cmd == b'*IDN?':
                            response = b'Serial Device Simulator\n'
                        elif cmd == b'READ?':
                            response = (str(random.randint(min(low, high), max(low, high))) + '\n').encode('utf-8')
                        elif cmd == b'*RST':
                            response = b'OK\n'
                        elif cmd == b'*STB?':
                            response = (str(random.randint(0, 2)) + '\n').encode('utf-8')
                        elif cmd == b'*TST?':
                            response = (str(random.randint(-2, 0)) + ';Selftest message.' + '\n').encode('utf-8')
                        elif cmd == b'High':
                            high = int(command[1].strip())
                            response = b'OK\n'
                        elif cmd == b'Low':
                            low = int(command[1].strip())
                            response = b'OK\n'
                        else:
                            response = b'ERR -1 Command unknown.\n'
                        print(counter, command, cmd, response)
                        conn.sendall(response)
                    else:
                        print('Connection read timed out.')
                        pass
                    pass  # while true:
                pass
            pass  # while True:  # Listen forever
