#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Derived GUI class for ScpiDevSimGui based on Tkinter

This script is used to test the layout of PyAcdaq Actor GUI's.

Lizenziert unter EUPL V. 1.2

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
import tkinter as tk
from pyacdaq.actor.tkguis import DeviceTkGui


class ScpiDevSimTkGui(DeviceTkGui):
    """
    GUI class for ScpiDevSim.

    It provides a basic tkinter based GUI for ScpiDevSim.
    """

    def __init__(self, master=None):
        super().__init__(master)
        self.master.title('AcdaqSimDevGui')

    def _create_widgets(self):
        """
        Create graphical user interface using tkinter widgets.

        Returns
        -------
        None.

        """
        super()._create_widgets()
        # Variables
        self.v_serdevsim_data = tk.StringVar()
        self.v_serdevsim_data.set('NA')
        self.v_serdevsim_measurement_range = tk.StringVar()
        self.v_serdevsim_measurement_range.set('NA')
        self.v_serdevsim_set_measurement_range = tk.StringVar()
        self.v_serdevsim_set_measurement_range.set('NA')
        self.v_serdevsim_set_measurement_range_button = tk.BooleanVar()
        self.v_serdevsim_set_measurement_range_button.set(False)
        # ScpiDevSim Frame label widget
        self.g_serdevsim = tk.LabelFrame(self)
        self.g_serdevsim['text'] = 'ScpiDevSim Class Level'
        self.g_serdevsim.pack()
        # Frame label widgetPrima, es ist ja nicht wirklich dringend.
        self.g_serdevsim_info = tk.LabelFrame(self.g_serdevsim)
        self.g_serdevsim_info['text'] = 'SerDevSim Information'
        self.g_serdevsim_info.pack()
        # Frame label widget
        self.g_serdevsim_actions = tk.LabelFrame(self.g_serdevsim)
        self.g_serdevsim_actions['text'] = 'SerDevSim Set-MeasuremtRange'
        self.g_serdevsim_actions.pack()
        # Frame label widget
        self.g_serdevsim_data = tk.LabelFrame(self.g_serdevsim)
        self.g_serdevsim_data['text'] = 'SerDevSim Data'
        self.g_serdevsim_data.pack()
        # Widgets
        self.l_serdevsim__measurement_range = tk.Label(self.g_serdevsim_info)
        self.l_serdevsim__measurement_range['textvariable'] = self.v_serdevsim_measurement_range
        self.l_serdevsim__measurement_range.pack()
        # Widgets
        self.b_serdevsim_set_measurement_range = tk.Button(self.g_serdevsim_actions)
        self.b_serdevsim_set_measurement_range['text'] = 'Set'
        self.b_serdevsim_set_measurement_range.pack(side='right')
        self.e_serdevsim_reset_result = tk.Entry(self.g_serdevsim_actions)
        self.e_serdevsim_reset_result['textvariable'] = self.v_serdevsim_set_measurement_range
        self.e_serdevsim_reset_result.pack(side='left')
        # Widgets
        self.l_serdevsim_data = tk.Label(self.g_serdevsim_data)
        self.l_serdevsim_data['textvariable'] = self.v_serdevsim_data
        self.l_serdevsim_data.pack()
        pass

    def set_measurement_range_cb(self, cb_function):
        """Set callback function for set_measurement_range checkbutton."""
        self.b_serdevsim_set_measurement_range['command'] = cb_function
        pass

    pass  # end of class definietion


if __name__ == '__main__':
    print("scpi_device_control_tkgui.py\n\
Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme\n")
    try:
        # Launch GUI
        root = tk.Tk()
        app = ScpiDevSimTkGui(root)
        app.mainloop()
    except Exception as e:
        print('Exception caught:', e)
    finally:
        pass
    print(__name__, 'Done.')
