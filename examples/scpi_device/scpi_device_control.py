#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
ScpiDevSim is a derived class of Device.

It supports basic methods and SCPI commands.
It can be used as template for other Device classes.

This class can be used with two simulator scripts in folder: SerialDeviceSimulator.
- EthernetDeviceSimulator.py using socket communication, com_type=SOCKET, default.
- SerialDeviceSimulator.py using RS232 serial line communication, com_type=ASRL

Lizenziert unter EUPL V. 1.2

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme

- Known issues
    - You may need to "pip install serial"
    - Linux: [Errno 13] could not open port /dev/ttyUSB0
        - You need to grant permission to user to access "/dev/tty..."
        - On Ubuntu 18.04, I fixed this issue with the following commands:
          - `sudo usermod -a -G tty $USER`
          - `sudo usermod -a -G dialout $USER`
          - Then reboot.
            - If you still have issues, try to debug with:
            - `strace -ff <COMMAND> > strace.txt 2>&1`
            - And look for "denied" in the strace.
"""

import getpass
import logging
import json
import os
import re
import serial
import socket
import tkinter as tk
import time
import warnings
import pyacdaq.exceptions as acdaq_exceptions
from pyacdaq.logger import configure_logging_handler
from pyacdaq.actor.mqtt import Mqtt as Mqtt
from pyacdaq.actor.device import Device
from pyacdaq.actor.device import DeviceGui as DeviceGui
from scpi_device_control_tkgui import ScpiDevSimTkGui as ScpiDevSimTkGui

scpidevsim_logger = logging.getLogger('ScpiDevSim').addHandler(logging.NullHandler())


class ScpiDevSim(Device):
    """Derived class for a device with SCPI communication, serial or socket."""

    def __init__(self, name,
                 start_periodic=False,
                 mqtt=None,
                 com_type='SOCKET',
                 resource='',
                 id_query_flag=True,
                 reset_flag=True,
                 reset_options=None,
                 selftest_flag=True,
                 measurement_range={'low': 0, 'high': 10}
                 ):
        """
        Initialize instance.

        Parameters
        ----------
        name : str
            Name of instance
        start_periodic : bool, optional
            Auto start periodic action if true. Default is False.
        mqtt : Mqtt Proxy, optional
            Proxy to MQTT actor. Default=None.
        com_type : str
            'ASRL' or 'SOCKET'. Default is 'SOCKET'
        resource : str, optional
            Interface to be used for communication with device. Default=None.
        id_query_flag : bool, optional
            Perform instrument reset; Default is True.
        reset_flag : bool, optional
            Perform instrument reset; Default is True.
        reset_options : str, optional
            Options to be applied after reset; Default is None
        selftest_flag : bool, optional
            Perform instrument self-test after option reset; Default is True.
        measurement_range : dict
            Measurement range. The default is {'low': 0, 'high': 10}.

        """
        self._serdevsim_subscriptions = ('Set-MeasurementRange', )
        super().__init__(name, start_periodic, mqtt, resource, id_query_flag, reset_flag, reset_options, selftest_flag)
        self._com_type = com_type
        self.__ser = None
        self.__soc = None
        self._range = {
            'low': min(measurement_range['low'], measurement_range['high']),
            'high': max(measurement_range['low'], measurement_range['high'])
        }
        if not re.match('ASRL'.upper(), com_type) and not re.match('SOCKET'.upper(), com_type):
            raise ValueError(self._name + ' SerDevSim __init__ com_type={0} ot supported.'.format(com_type))
        pass

    def __del__(self):
        """Finalize instance."""
        super().__del__()
        pass

    # Private methods
    def __transaction(self, command):
        """
        Communicate with instrument. Send cammand and read response.

        Parameters
        ----------
        command : str
            Command string

        Raises
        ------
        acdaq_exceptions.CommunicationTimeout

        Returns
        -------
        response : str
            Instrument response line with NewLine charater stripped.

        """
        formatted_command = command + '\n'
        if not self.__soc:  # Serial communication
            self.__ser.write(formatted_command.encode('utf-8'))
            line = self.__ser.readline()
            if line != b'':
                response = re.split('\n', line.decode('utf-8'))[0]
                scpidevsim_logger.debug(self._name + ' SerDevSim __transaction() Command:' + command + '; Response:' + response)
            else:
                scpidevsim_logger.error(self._name + ' SerDevSim __transaction() Command:' + command + '; timed out')
                raise acdaq_exceptions.CommunicationTimeout(command)
        else:  # Socket communication
            try:
                self.__soc.sendall(formatted_command.encode('utf-8'))
                line = self.__soc.recv(1024)
                if line != b'':
                    response = re.split('\n', line.decode('utf-8'))[0]
                    scpidevsim_logger.debug(self._name + ' SerDevSim __transaction() Command:' + command + '; Response:' + response)
                else:
                    scpidevsim_logger.error(self._name + ' SerDevSim __transaction() Command:' + command + '; timed out')
            except socket.TimeoutError:
                raise acdaq_exceptions.CommunicationTimeout(command)
        return response

    # Protected override methods from super Actor
    def _on_connect(self):
        """
        Called from _on_receive() when message was received from paho.mqtt.client callback message on_connect.

        Perform action on MQTT connect here, e.g. publish and subscribe topics.

        Returns
        -------
        None.

        """
        scpidevsim_logger.debug(self._name + ' SerDevSim _on_connect() Received MQTT connected message. Publish and subscribe...')
        try:
            if self._mqtt is not None:
                self._mqtt.publish_topic(self._name + '/MeasurementRange', self._range, qos=0, retain=True, expand=False)
                self._mqtt.publish_topic(self._name + '/Set-MeasurementRange', self._range, qos=0, retain=True, expand=False)
                scpidevsim_logger.debug(self._name + ' ActorDevice _on_connect() Publishing:' + str(self._serdevsim_subscriptions))
                prefixed_topics = ()
                for topic in self._serdevsim_subscriptions:
                    prefixed_topic = self._name + '/' + topic
                    scpidevsim_logger.debug(self._name + ' ActorDevice _on_connect() Publishing:' + str(prefixed_topic))
                    prefixed_topics = prefixed_topics + (prefixed_topic,)
                    self._mqtt.publish_topic(prefixed_topic, '', qos=0, retain=True, expand=False)
                rcs_mids = self._mqtt.subscribe_topics(self._in_future, prefixed_topics, qos=0).get()
                scpidevsim_logger.debug(self._name + ' ActorDevice _on_connect() Subscribing to: ' + str(prefixed_topics) + ' returned rcs, mids = ' + str(rcs_mids))
        except Exception as e:
            scpidevsim_logger.exeption(self._name + ' SerDevSim _on_connect(): Exception cought:')
            scpidevsim_logger.exception(self._name + ' SerDevSim _on_connect(): Exception: ' + str(e))
        super()._on_connect()
        pass

    def _on_message(self, topic, msg):
        """
        Called from Mqtt._on_receive() when message was received from paho.mqtt.client callback message on_message.

        Perform action on MQTT messages here or forward to super.

        Returns
        -------
        None.

        """
        scpidevsim_logger.debug(self._name + ' SerDevSim _on_message() Received MQTT topic.' + topic + ' msg: ' + str(msg))
        sub_topics = re.split('/', topic)
        match sub_topics[2]:
            case 'Set-MeasurementRange':
                try:
                    scpidevsim_logger.debug(self._name + ' SerDevSim _on_message() Set-MeasurementRange received, msg=' + str(msg.strip()))
                    if len(msg) >= 18:  # Example: {"low":0,"high":5}
                        self.measurement_range(json.loads(msg))
                    else:
                        scpidevsim_logger.info(self._name + ' SerDevSim _on_message() Set-MeasurementRange ignored. Payload string is too short.')
                except Exception as e:
                    scpidevsim_logger.warning(self._name + ' SerDevSim _on_message() Set-MeasurementRange ignored due to exception: ' + str(e))
            case _:  # Call super
                scpidevsim_logger.debug(self._name + ' SerDevSim _on_message() Calling super() due for unknown topic: ' + topic + ' msg: ' + str(msg))
                super()._on_message(topic, msg)
        pass

    def _on_receive(self, message):
        """
        May be implemented for the actor to handle regular non-proxy messages.

        Virtual protected method.
        Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        scpidevsim_logger.debug(self._name + ' SerDevSim _on_receive() call super()._on_receive(message)')
        super()._on_receive(message)
        pass

    def _on_start(self):
        """
        Hook for doing any setup that should be done after the actor is started, but before it starts processing messages.

        Virtual protected method.
        Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        scpidevsim_logger.debug(self._name + ' SerDevSim _on_start() call super()._on_start()')
        super()._on_start()
        pass

    def _on_stop(self):
        """
        Close instrument.

        Returns
        -------
        None.

        """
        scpidevsim_logger.info(self._name + ' SerDevSim _on_stop() SerDevSim _on_stop()')
        self.close()
        try:
            if self._mqtt is not None:
                prefixed_topics = ()
                scpidevsim_logger.debug(self._name + ' SerDevSim _on_stop() Unsubscribing: ' + str(self._device_subscriptions))
                for topic in self._serdevsim_subscriptions:
                    prefixed_topic = self._name + '/' + topic
                    prefixed_topics = prefixed_topics + (prefixed_topic,)
                scpidevsim_logger.debug(self._name + ' SerDevSim _on_stop() Unsubscribing from: ' + str(prefixed_topics))
                self._mqtt.unsubscribe_topics(self, prefixed_topics)
        except Exception as e:
            scpidevsim_logger.exception(self._name + ' SerDevSim _on_stop(): Exception cought:')
            scpidevsim_logger.exception(self._name + ' SerDevSim _on_stop(): Exception: ' + str(e))
        super()._on_stop()
        pass

    def _periodic(self, reason):
        """
        Read data from instrument.

        Parameters
        ----------
        reason : PeriodicCallReason
            Reason for calling this method.

        Returns
        -------
        None.

        """
        try:
            for ii in range(1):
                self._data = int(self.__transaction('READ?'))
                scpidevsim_logger.debug(self._name + " Data=" + str(self._data))
                if self._mqtt is not None:
                    self._mqtt.publish_topic(self._name + '/Data', self._data, qos=0, retain=True, expand=False)
                time.sleep(1)
        except acdaq_exceptions.CommunicationTimeout as e:
            scpidevsim_logger.exception(self._name + ' SerDevSim _periodic() Communication timed out for command:{}'.format(e.command))
            raise e
        pass

    # Protected override methods from super Device
    def _close(self):
        """
        Set instrument to save mode and close connection.

        Returns
        -------
        int
            Error code.
        """
        try:
            scpidevsim_logger.debug(self._name + ' SerDevSim _close() Closing serial connection.')
            self.__ser.close()
            pass
        except Exception:
            pass
        try:
            scpidevsim_logger.debug(self._name + ' SerDevSim _close() Closing socket connection.')
            self.__soc.close()
        except Exception:
            pass
        return

    def _id_query(self):
        """
        Perform id query of instrument.

        Returns
        -------
        str
            Instrument ID.

        """
        scpidevsim_logger.debug(self._name + ' SerDevSim _id_query() Performing id query.')
        try:
            id = self.__transaction('*IDN?')
        except acdaq_exceptions.CommunicationTimeout as e:
            id = 'NA'
            scpidevsim_logger.error(self._name + ' SerDevSim _id_query() Communication timed out for command:{}'.format(e.command))
            raise e
        return id

    def _initalize(self,
                   resource=None,
                   id_query_flag=True,
                   reset_flag=True,
                   reset_options=None,
                   selftest_flag=True):
        """
        Open connection and initialize instrument.

        Parameters
        ----------
        resource : str
            Resource identifier.
        id_query_flag : bool
            Flag requesting an id_query. Default isTrue.
        reset_flag : bool
            Flag requesting a reset. Default is True.
        reset_options : str
            String containing reset options. Default is None.
        selftest_flag : bool
            Flag requesting an id_query. Default is True.

        Returns
        -------
        int
            Error code.

        """
        scpidevsim_logger.debug(self._name + ' SerDevSim _initialize() Initializing connection.')
        try:
            if re.match('ASRL'.upper(), self._com_type):  # Serial lien communication
                scpidevsim_logger.debug('Connecting to {0}'.format(resource))
                self.__ser = serial.Serial(resource, timeout=1)
            else:  # Socket communication
                host = re.split(':', resource)[0]
                port = int(re.split(':', resource)[1])
                scpidevsim_logger.debug('Connecting to {0}:{1}'.format(host, port))
                self.__soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.__soc.settimeout(1)
                self.__soc.connect((host, port))
            if id_query_flag:
                self.id_query()
            self.revision_query()
            if reset_flag:
                self.reset(reset_options)
            if selftest_flag:
                self.selftest()
            self.measurement_range(self._range)
        except Exception as e:
            scpidevsim_logger.exception(self._name + ' SerDevSim _initialize() Initializing connection: ' + resource)
            scpidevsim_logger.exception(self._name + ' Exception caught: ' + str(e))
            raise e
        pass

    def _reset(self, reset_options=None):
        """
        Reset instrument with options.

        Parameters
        ----------
        reset_options : str
            String containing reset options. Default is None.

        Returns
        -------
        str
            Reset result.

        """
        scpidevsim_logger.debug(self._name + ' SerDevSim _reset() Performing reset.')
        try:
            response = self.__transaction('*RST')
        except acdaq_exceptions.CommunicationTimeout as e:
            response = "Failed"
            scpidevsim_logger.exception(self._name + ' SerDevSim _reset() Communication timed out for command:{}'.format(e.command))
            raise e
        return response

    def _revision_query(self):
        """
        Perform instrument revision query, firmware and hardware.

        Returns
        -------
        str
            Firmware revision.
        str
            Hardware revision.

        """
        scpidevsim_logger.debug(self._name + ' SerDevSim _revision_query() Performing revision query.')
        try:
            fw = self.__transaction('FW?')
            hw = self.__transaction('HW?')
        except acdaq_exceptions.CommunicationTimeout as e:
            fw = 'NA'
            hw = 'NA'
            scpidevsim_logger.exception(self._name + ' SerDevSim _revision_query() Communication timed out for command:{}'.format(e.command))
            raise e
        return fw, hw

    def _selftest(self):
        """
        Perform instrument selftest.

        Returns
        -------
        int
            Selftest return code.
        str
            Selftest message.

        """
        scpidevsim_logger.debug(self._name + ' SerDevSim _selftest() Performing selftest.')
        try:
            response = self.__transaction('*TST?')
            result = re.split(';', response)
            code = int(result[0])
            message = result[1]
        except acdaq_exceptions.CommunicationTimeout as e:
            code = -1
            message = 'Selftest failed.'
            scpidevsim_logger.exception(self._name + ' SerDevSim _selftest() Selftest response: ' + message)
            scpidevsim_logger.exception(self._name + ' SerDevSim _selftest() Communication timed out for command:{}'.format(e.command))
            raise e
        return code, message

    def _sn_query(self):
        """
        Perform instrument serial number query.

        Returns
        -------
        str
            Serial number.

        """
        scpidevsim_logger.debug(self._name + ' SerDevSim _sn_query() Performing serial number query.')
        try:
            sn = self.__transaction('SN?')
        except acdaq_exceptions.CommunicationTimeout as e:
            sn = 'NA'
            scpidevsim_logger.exception(self._name + ' SerDevSim _sn_query() Communication timed out for command:{}'.format(e.command))
            raise e
        return sn

    def _sw_revision(self):
        """
        Return revision of this class inclusive parents.

        Returns
        -------
        string
            Software revision.

        """
        return 'ScpiDevSim_0.1.0.0|' + super()._sw_revision()

    # Public methods of this class
    def measurement_range(self, m_range={'low': 0, 'high': 10}):
        """
        Set measurement range.

        Parameters
        ----------
        m_range : dict, optional
            Measurement range. The default is {'low': 0, 'high': 10}.

        Returns
        -------
        None.

        """
        try:
            new_m_range = {
                'low': min(m_range['low'], m_range['high']),
                'high': max(m_range['low'], m_range['high'])
            }
            scpidevsim_logger.info(self._name + ' SerDevSim measurement_range() New measurement_range = ' + str(new_m_range))
            self.__transaction('Low ' + str(new_m_range['low']))
            self.__transaction('High ' + str(new_m_range['high']))
            self._range = new_m_range
            self._mqtt.publish_topic(self._name + '/MeasurementRange', self._range, qos=0, retain=True, expand=False)
        except acdaq_exceptions.CommunicationTimeout as e:
            scpidevsim_logger.exception(self._name + ' SerDevSim measurement_range() Communication timed out for command:{}'.format(e.command))
            raise e
        pass

    # Getter
    def getData(self):
        """Return data."""
        return self._data

    def getMeasurementRange(self):
        """Return measurement range."""
        return self._m_range

    pass  # end of SerDevSim class definition


class ScpiDevSimGui(DeviceGui):
    """
    Gui class for ScpiDevSim.

    It provides a basic tkinter based GUI for Device.
    """

    def __init__(self, name, start_periodic=False, mqtt=None, associated_actor_name=None, tkgui=None):
        """
        Initialize this instance.

        Parameters
        ----------
        name : str
            Name of instance.
        start_periodic : bool, optional
            Auto start periodic action if true. Default is False.
        mqtt_actor : Mqtt Proxy, optional
            Proxy to MQTT actor. Default is None.
        associated_actor_name : str, optional
            Name of associated actor. The default is None.
        tkgui : ScpiDevSimTkGui, optional
            ScpiDevSimTkGui instance. The default is None.

        Returns
        -------
        None.

        """
        super().__init__(name, start_periodic, mqtt, associated_actor_name, tkgui)
        self._serdevsim_gui_subscriptions = ('Data', 'MeasurementRange', )
        self._serdevsim_gui_topics = ('Set-MeasurementRange', )
        self._tkgui.set_measurement_range_cb(self.on_set_measurement_range)
        scpidevsim_logger.debug(self._name + ' ScpiDevSimGui Associated Actor Name' + str(self._associated_actor_name))
        pass

    def __del__(self):
        """Finalize instance."""
        super().__del__()
        pass

    # Protected override methods from super Actor
    def _on_connect(self):
        """
        Called from _on_receive() when message was received from paho.mqtt.client callback message on_connect.

        Perform action on MQTT connect here, e.g. publish and subscribe topics.

        Returns
        -------
        None.

        """
        scpidevsim_logger.debug(self._name + ' ScpiDevSimGui _on_connect() Received MQTT connected message. Publish and subscribe...')
        try:
            if self._mqtt is not None:
                scpidevsim_logger.debug(self._name + ' ScpiDevSimGui _on_connect() Publishing:' + str(self._serdevsim_gui_subscriptions))
                prefixed_topics = ()
                for topic in self._serdevsim_gui_subscriptions:
                    prefixed_topic = self._associated_actor_name + '/' + topic
                    scpidevsim_logger.debug(self._name + ' ScpiDevSimGui _on_connect() Publishing:' + str(prefixed_topic))
                    prefixed_topics = prefixed_topics + (prefixed_topic,)
                    self._mqtt.publish_topic(prefixed_topic, '', qos=0, retain=True, expand=False)
                rcs_mids = self._mqtt.subscribe_topics(self._in_future, prefixed_topics, qos=0).get()
                scpidevsim_logger.debug(self._name + ' ScpiDevSimGui _on_connect() Subscribing to: ' + str(prefixed_topics) + ' returned rcs, mids = ' + str(rcs_mids))
        except Exception as e:
            scpidevsim_logger.exception(self._name + ' ScpiDevSimGui _on_connect(): Exception cought:')
            scpidevsim_logger.exception(self._name + ' ScpiDevSimGui _on_connect(): Exception: ' + str(e))
        super()._on_connect()
        pass

    def _on_failure(self, exception_type, exception_value, traceback):
        """
        Hook for doing any cleanup after an unhandled exception is raised, and before the actor stops.

        Returns
        -------
        None.

        """
        self.close()
        super()._on_failure(exception_type, exception_value, traceback)
        pass

    def _on_message(self, topic, msg):
        """
        Called from Mqtt._on_receive() when message was received from paho.mqtt.client callback message on_message.

        Perform action on MQTT messages here or forward to super.

        Returns
        -------
        None.

        """
        scpidevsim_logger.debug(self._name + ' ScpiDevSimGui _on_message() Received MQTT topic. ' + topic + ' msg: ' + str(msg))
        sub_topics = re.split('/', topic)
        if sub_topics[1].find(self._associated_actor_name) >= 0:
            match sub_topics[2]:
                case 'Data':
                    scpidevsim_logger.debug(self._name + ' ScpiDevSimGui _on_message() Data received, msg: ' + str(msg))
                    self._tkgui.v_serdevsim_data.set('Data: ' + str(msg))
                case 'MeasurementRange':
                    scpidevsim_logger.debug(self._name + ' ScpiDevSimGui _on_message() MeasurementRange received, msg: ' + str(msg))
                    self._tkgui.v_serdevsim_measurement_range.set('MesurmentRange: ' + str(msg))
                case _:  # Call super
                    scpidevsim_logger.debug(self._name + ' ScpiDevSimGui _on_message() Calling super() due for unknown topic: ' + topic + ' msg: ' + str(msg))
                    super()._on_message(topic, msg)
        else:
            scpidevsim_logger.debug(self._name + ' ScpiDevSimGui _on_message() Calling super() due for unknown topic: ' + topic + ' msg: ' + str(msg))
            super()._on_message(topic, msg)
        pass

    def _on_receive(self, message):
        """
        May be implemented for the actor to handle regular non-proxy messages or forward to super.

        Returns
        -------
        None.

        """
        warnings.warn(self._name + ' ScpiDevSimGui _on_receive() Method needs to be implemented by derived class.')
        super()._on_receive(message)
        pass

    def _on_start(self):
        """
        Hook for doing any setup that should be done after the actor is started, but before it starts processing messages.

        Returns
        -------
        None.

        """
        # super()._on_start()
        pass

    def _on_stop(self):
        """
        Hook for doing any cleanup that should be done after the actor has processed the last message, and before the actor stops.

        Returns
        -------
        None.

        """
        scpidevsim_logger.debug(self._name + ' ScpiDevSimGui _on_stop()')
        try:
            if self._mqtt is not None:
                prefixed_topics = ()
                scpidevsim_logger.debug(self._name + ' ScpiDevSimGui _on_stop() Unsubscribing: ' + str(self._serdevsim_gui_subscriptions))
                for topic in self._serdevsim_gui_subscriptions:
                    prefixed_topic = self._name + '/' + topic
                    prefixed_topics = prefixed_topics + (prefixed_topic,)
                scpidevsim_logger.debug(self._name + ' Acdaqacdaq.SerDevSimGui _on_stop() Unsubscribing from: ' + str(prefixed_topics))
                self._mqtt.unsubscribe_topics(self, prefixed_topics)
        except Exception as e:
            scpidevsim_logger.exception(self._name + ' ScpiDevSimGui _on_stop(): Exception cought:')
            scpidevsim_logger.exception(self._name + ' ScpiDevSimGui _on_stop(): Exception: ' + str(e))
        super()._on_stop()
        pass

    def _periodic(self):
        """
        Implementation of periodic actions.

        Virtual protected method. Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        warnings.warn(self._name + ' ScpiDevSimGui _periodic() Method needs to be implemented by derived class.')
        super()._periodic()
        pass

    def _sw_revision(self):
        """
        Return revision of this class inclusive parents.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        str
            Software revision.

        """
        return 'ScpiDevSimGui_0.1.0.0|' + super()._sw_revision()

    # Protected virtual methods defined by this class

    # Public methods
    def on_set_measurement_range(self):
        """Handle actor_set_measurement_range = tk.Button()."""
        value_string = self._tkgui.v_serdevsim_set_measurement_range.get()
        scpidevsim_logger.debug(self._name + ' ScpiDevSimGui on_set_measurement_range():' + str(value_string))
        value = json.loads(value_string)
        if self._mqtt is not None:
            self._mqtt.publish_topic(self._associated_actor_name + '/Set-MeasurementRange', value, qos=0, retain=True, expand=False)
        pass

    pass  # End of class definition


if __name__ == '__main__':
    app_name = re.split('.py', os.path.basename(__file__))[0]
    print(app_name + "\n\
Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme")
    # Logging setup
    print('Configuring logging...')
    # Define logger to be used
    # logging.basicConfig(  # filename='example.log', filemode='w', encoding='utf-8', level=logging.INFO, style='{', format='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    logger = logging.getLogger('App-Logger')
    logger.setLevel(logging.DEBUG)
    pyacdaq_logger = logging.getLogger('pyacdaq')
    pyacdaq_logger.setLevel(logging.DEBUG)
    scpidevsim_logger = logging.getLogger('ScpiDevSim')
    scpidevsim_logger.setLevel(logging.DEBUG)
    # Define logging formatter
    logging_formatter = logging.Formatter(style='{', fmt='{asctime} [{levelname:8}] {module} {funcName} {lineno} {message}')
    # logging_formatter = logging.Formatter(style='{', fmt='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    # configure_logging_handler((logger, pyacdaq_logger), logging_formatter, severity={'console': None, 'file': None, 'mqtt': None, 'syslog': None}, mqtt={'broker': 'ee-raspi1001', 'topic': app_name + '/log_msg', 'user': '', 'password': ''})

    mqtt_broker = input('MQTT Broker? (default: localhost)>')
    if len(mqtt_broker) == 0:
        mqtt_broker = 'localhost'
    mqtt_user = input('MQTT Username: ')
    if len(mqtt_user) == 0:
        mqtt_user = None
    mqtt_password = getpass.getpass()
    if len(mqtt_password) == 0:
        mqtt_password = None
    interface = input('Simulator Interface? (ASRL or default: SOCKET)>')
    configure_logging_handler(app_name, (logger, pyacdaq_logger, scpidevsim_logger), logging_formatter, severity={'console': logging.INFO, 'file': logging.DEBUG, 'mqtt': logging.INFO, 'syslog': logging.WARNING}, mqtt={'broker': mqtt_broker, 'topic': 'ACDAQ/log_msg', 'user': mqtt_user, 'password': mqtt_password})
    if len(interface) == 0:
        interface = 'SOCKET'
    resource = input('Simulator resource? (e.g. /dev/ttyUSB0 or default: localhost:12345)>')
    if len(resource) == 0:
        resource = 'localhost:12345'
    print('Please, wait for MQTT actor.')
    try:
        # Create MQTT actor proxy
        a_mqtt = Mqtt.start('Mqtt',
                            start_periodic=False,
                            broker=mqtt_broker,
                            port=1883,
                            client_id='ACDAQ',
                            clean_session=True,
                            retain=True,
                            will='offline',
                            username=mqtt_user,
                            password=mqtt_password
                            )
        p_mqtt = a_mqtt.proxy()
        # Launch GUI
        print('Please, wait for GUI.')
        time.sleep(3)
        root = tk.Tk()
        a_device_tkgui = ScpiDevSimTkGui(root)
        type(a_device_tkgui)
        # Create Gui actor proxy
        p_device_gui = ScpiDevSimGui.start('ScpiDevSimGui',
                                           start_periodic=False,
                                           mqtt=p_mqtt,
                                           associated_actor_name='ScpiDevSim',
                                           tkgui=a_device_tkgui
                                           ).proxy()
        # Create device proxy
        print('Please, wait for device.')
        time.sleep(5)
        a_device = ScpiDevSim.start('ScpiDevSim',
                                    start_periodic=False,
                                    mqtt=p_mqtt,
                                    com_type=interface,
                                    resource=resource,
                                    id_query_flag=True,
                                    reset_flag=True,
                                    reset_options='myResetOptions',
                                    selftest_flag=True,
                                    measurement_range={'low': 0, 'high': 10}
                                    )
        p_device = a_device.proxy()
        device_name = p_device.getName().get()
        device_birthday = p_device.getBirthday().get()
        device_periodic = p_device.getPeriodicStatus().get()
        device_sw_revision = p_device.getSWRevision().get()
        device_instrument_id = p_device.getInstrumentID().get()
        device_firmware_revision = p_device.getFirmwareRevision().get()
        device_hardware_revision = p_device.getHardwareRevision().get()
        device_serial_number = p_device.getSerialNumber().get()
        print(__name__, 'Name:', device_name,
              '; Birthday:', device_birthday,
              '; Periodic:', device_periodic,
              '; SW:', device_sw_revision,
              '; ID:', device_instrument_id,
              '; FW:', device_firmware_revision,
              '; HW:', device_hardware_revision,
              '; SN:', device_serial_number)
        time.sleep(15)
        # Start the Tkinter GUI Loop
        a_device_tkgui.mainloop()
    except Exception as e:
        print('_main__: Exception caught:', e)
    finally:
        p_device_gui.stop()
        time.sleep(3)
        p_device.stop()
        time.sleep(3)
        p_mqtt.stop()
    logging.shutdown()
    print(__name__, 'Done.')
