#!/usr/bin/env python
"""
This program can be used to simulate an scpi device with with serial line interface.
It is the counterpart for ScpiDevSim.
It was designed to be executed on a Raspberry Pi,
but could also run on Linux and Windows.

Specify serial interface and baudrate in the code.

You may need to install: pip install pyserial>=3.5
Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""

import random
import re
import serial

random.seed()
with serial.Serial('/dev/ttyS0', baudrate=9600, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, xonxoff=False, timeout=60) as ser:
    high = 10
    low = 0
    counter = 0
    command = ''
    response = ''
    disconnected = False
    termination = '\n'
    while not disconnected:
        counter += 1
        match termination:
            case '\n':
                line = ser.readline().decode('utf-8')
            case '\r':
                line = ser.read_until(termination.encode(), 1014).decode('utf-8')
            case '\r\n':
                line = ser.read_until(termination.encode(), 1014).decode('utf-8')
            case _:
                print('Stopped due to unhandled termination.')
                break
        print('Line:', line)
        command = re.split(' ', line)
        cmd = command[0].strip()
        print('Command', cmd)
        if cmd != '':
            if cmd == 'FW?':
                response = 'FW_0.1' + termination
            elif cmd == 'HW?':
                response = 'HW_0.1' + termination
            elif cmd == 'SN?':
                response = '1234' + termination
            elif cmd == '*IDN?':
                response = 'Serial Device Simulator' + termination
            elif cmd == 'READ?':
                response = str(random.randint(min(low, high), max(low, high))) + termination
            elif cmd == '*RST':
                response = 'OK' + termination
            elif cmd == '*STB?':
                response = str(random.randint(0, 2)) + termination
            elif cmd == '*TST?':
                response = str(random.randint(-2, 0)) + ';Selftest message.' + termination
            elif cmd == 'High':
                high = int(command[1].strip())
                response = 'OK' + termination
            elif cmd == 'Low':
                low = int(command[1].strip())
                response = 'OK' + termination
            else:
                response = 'ERR -1 Command unknown.' + termination
            print(counter, command, cmd, response)
            ser.write(response.encode())
        else:
            print('Serial read timed out.')
            pass
