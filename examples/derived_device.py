#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Derived device actor class template.

- Replace DerivedDevice with you class name.
- Replace derived_device with instance name.

Lizenziert unter EUPL V. 1.2

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""

import getpass
import logging
import os
import re
import time
# import pyacdaq.exceptions as acdaq_exceptions
from pyacdaq.logger import configure_logging_handler
from pyacdaq.actor.mqtt import Mqtt as Mqtt
from pyacdaq.actor.device import Device
from pyacdaq.actor.actor import PeriodicCallReason as PeriodicCallReason


derived_device_logger = logging.getLogger('DerivedDevice').addHandler(logging.NullHandler())


class DerivedDevice(Device):
    """Derived device actor class template.

    It extends/overrides virtual methods of its super class and
    adds instrument specific attributes and methods.
    """

    def __init__(self, name,
                 start_periodic=False,
                 mqtt=None,
                 resource=None,
                 id_query_flag=True,
                 reset_flag=True,
                 reset_options=None,
                 selftest_flag=True,
                 ):
        """
        Initialize this instance.

        Parameters
        ----------
        name : str
            Name of instance
        start_periodic : bool, optional
            Auto start periodic action if true. Default is False.
        mqtt : Mqtt Proxy, optional
            Proxy to MQTT actor. Default=None.
        resource : str, optional
            Interface to be used for communication with device. Default=None.
        id_query_flag : bool, optional
            Perform instrument reset; Default is True.
        reset_flag : bool, optional
            Perform instrument reset; Default is True.
        reset_options : str, optional
            Options to be applied after reset; Default is None
        selftest_flag : bool, optional
            Perform instrument self-test after option reset; Default is True.

        Returns
        -------
        None.

        """
        self._derived_device_subscriptions = ('Set-Command', )
        super().__init__(name, start_periodic, mqtt, resource, id_query_flag, reset_flag, reset_options, selftest_flag)
        pass

    def __del__(self):
        """Finalize instance."""
        super().__del__()
        pass

    # Private methods
    
    # Protected override methods from super Actor
    def _on_connect(self):
        """
        Called from _on_receive() when message was received from paho.mqtt.client callback message on_connect.

        Perform action on MQTT connect here, e.g. publish and subscribe topics.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        None.

        """
        derived_device_logger.debug(self._name + ' DerivedDevice _on_connect() Received MQTT connected message. Publish and subscribe...')
        try:
            if self._mqtt is not None:
                logger.debug(self._name + ' DerivedDevice _on_connect() Publishing:' + str(self._derived_device_subscriptions))
                prefixed_topics = ()
                for topic in self._derived_device_subscriptions:
                    prefixed_topic = self._name + '/' + topic
                    logger.debug(self._name + ' DerivedDevice _on_connect() Publishing:' + str(prefixed_topic))
                    prefixed_topics = prefixed_topics + (prefixed_topic,)
                    self._mqtt.publish_topic(prefixed_topic, '', qos=0, retain=True, expand=False)
                logger.debug(self._name + ' DerivedDevice _on_connect() Subscribing to: ' + str(prefixed_topics))
                rcs_mids = self._mqtt.subscribe_topics(self._in_future, prefixed_topics, qos=0).get()
                logger.debug(self._name + ' DerivedDevice _on_connect() Subscribing to: ' + str(prefixed_topics) + ' returned rcs, mids = ' + str(rcs_mids))
        except Exception as e:
            logger.exception(self._name + ' DerivedDevice _on_connect(): Exception cought: ' + str(e))
        super()._on_connect()
        pass

    def _on_disconnect(self):
        """
        Called from _on_receive() when message was received from paho.mqtt.client callback message on_disconnect.

        Perform action on MQTT disconnect here.

        Virtual protected method. Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        derived_device_logger.debug(self._name + ' DerivedDevice _on_disconnect() Received MQTT disconnected message.')
        pass

    def _on_failure(self, exception_type, exception_value, traceback):
        """
        Hook for doing any cleanup after an unhandled exception is raised, and before the actor stops.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        None.

        """
        self.close()
        super()._on_failure(exception_type, exception_value, traceback)
        pass

    def _on_message(self, topic, msg):
        """
        Called from Mqtt._on_receive() when message was received from paho.mqtt.client callback message on_message.

        Perform action on MQTT messages here or forward to super.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        None.

        """
        derived_device_logger.debug(self._name + ' DerivedDevice _on_message() Received MQTT topic.' + topic + ' msg: ' + str(msg))
        sub_topics = re.split('/', topic)
        logger.debug(self._name + ' DerivedDevice _on_message() Received MQTT sub-topics: ' + str(sub_topics))
        match sub_topics[2]:
            case 'Set-Command':
                try:
                    print(self._name + ' DerivedDevice _on_message() Set-Command received, msg=' + str(msg.strip()))
                except Exception as e:
                    logger.exception(self._name + ' _on_message() Set-Command ignored due to exception: ' + str(e))
            case _:  # Call super
                logger.debug(self._name + ' DerivedDevice _on_message() Calling super() due for unknown topic: ' + topic + ' msg: ' + str(msg))
                super()._on_message(topic, msg)
        pass

    def _on_receive(self, message):
        """
        May be implemented for the actor to handle regular non-proxy messages or forward to super.

        Virtual protected method. Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        derived_device_logger.debug(self._name + ' DerivedDevice _on_receive() call super()._on_receive(message)')
        super()._on_receive(message)
        pass

    def _on_start(self):
        """
        Hook for doing any setup that should be done after the actor is started, but before it starts processing messages.

        Initialize instrument.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        None.

        """
        derived_device_logger.debug(self._name + ' DerivedDevice _on_start() call super()._on_start()')
        super()._on_start()
        pass

    def _on_stop(self):
        """
        Hook for doing any cleanup that should be done after the actor has processed the last message, and before the actor stops.

        Close instrument.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        None.

        """
        derived_device_logger.debug(self._name + ' DerivedDevice _on_stop()')
        try:
            if self._mqtt is not None:
                prefixed_topics = ()
                logger.debug(self._name + ' DerivedDevice _on_stop() Unsubscribing: ' + str(self._derived_device_subscriptions))
                for topic in self._derived_device_subscriptions:
                    prefixed_topic = self._name + '/' + topic
                    prefixed_topics = prefixed_topics + (prefixed_topic,)
                logger.debug(self._name + ' DerivedDevice _on_stop() Unsubscribing from: ' + str(prefixed_topics))
                self._mqtt.unsubscribe_topics(self, prefixed_topics)
        except Exception as e:
            logger.exception(self._name + ' DerivedDevice _on_stop(): Exception: ' + str(e))
        super()._on_stop()
        pass

    def _periodic(self, reason):
        """
        Implementation of periodic actions.

        Virtual protected method. Override with concrete child class implementation.

        Parameters
        ----------
        reason : PeriodicCallReason
            Reason for calling this method.

        Returns
        -------
        None.

        """
        match reason:
            case PeriodicCallReason.Start:
                pass
            case PeriodicCallReason.Do:
                time.sleep(1)
                pass
            case PeriodicCallReason.Stop:
                pass
        pass

    def _sw_revision(self):
        """
        Return revision of this class inclusive parents.

        Virtual protected method. Extend with concrete child class implementation.

        Returns
        -------
        str
            Software revision.

        """
        return 'DerivedDevice_0.0.0.0|' + super()._sw_revision()

    # Protected override methods from super Device
    def _close(self):
        """
        Set instrument to save mode and close connection.

        Virtual protected method.
        Override method needs to be implemented by derived class.

        Returns
        -------
        int
            Error code.

        """
        derived_device_logger.debug(self._name + ' DerivedDevice _close() Closing device connection.')
        return

    def _id_query(self):
        """
        Perform id query of instrument.

        Virtual protected method. Override method needs to be implemented by derived class.

        Returns
        -------
        str
            Instrument ID.

        """
        derived_device_logger.debug(self._name + ' DerivedDevice _id_query() Performing id query.')
        id = 'NA'
        return id

    def _initalize(self,
                   resource=None,
                   id_query_flag=True,
                   reset_flag=True,
                   reset_options=None,
                   selftest_flag=True):
        """
        Open connection and initialize instrument.

        Virtual protected method. Extend method needs to be implemented by derived class.

        Parameters
        ----------
        resource : str
            Resource identifier.
        id_query_flag : bool
            Flag requesting an id_query. Default isTrue.
        reset_flag : bool
            Flag requesting a reset. Default is True.
        reset_options : str
            String containing reset options. Default is None.
        selftest_flag : bool
            Flag requesting an id_query. Default is True.

        Returns
        -------
        int
            Error code.

        """
        derived_device_logger.debug(self._name + ' DerivedDevice _initialize() Initializing connection.')
        try:
            if id_query_flag:
                self.id_query()
            self.revision_query()
            if reset_flag:
                self.reset(reset_options)
            if selftest_flag:
                self.selftest()
        except Exception as e:
            derived_device_logger.exception(self._name + ' DerivedDevice _initialize() Exception caught during initializing connection: ' + resource + ' Exception: ' + str(e))
            raise e
        pass

    def _reset(self, reset_options=None):
        """
        Reset instrument with options.

        Virtual protected method. Override method needs to be implemented by derived class.

        Parameters
        ----------
        reset_options : str
            String containing reset options. Default is None.

        Returns
        -------
        str
            Reset result.

        """
        derived_device_logger.debug(self._name + ' DerivedDevice _reset() Performing reset.')
        response = "Failed"
        return response

    def _revision_query(self):
        """
        Perform instrument revision query, firmware and hardware.

        Virtual protected method. Override method needs to be implemented by derived class.

        Returns
        -------
        str
            Firmware revision.
        str
            Hardware revision.

        """
        derived_device_logger.debug(self._name + ' DerivedDevice _revision_query() Performing revision query.')
        fw = 'NA'
        hw = 'NA'
        return fw, hw

    def _selftest(self):
        """
        Perform instrument selftest.

        Virtual protected method. Override method needs to be implemented by derived class.

        Returns
        -------
        int
            Selftest return code.
        str
            Selftest message.

        """
        derived_device_logger.debug(self._name + ' DerivedDevice _selftest() Performing selftest.')
        code = -1
        message = 'Selftest failed.'
        return code, message

    def _sn_query(self):
        """
        Perform instrument serial number query.

        Virtual protected method. Override method needs to be implemented by derived class.

        Returns
        -------
        str
            Serial number.

        """
        derived_device_logger.debug(self._name + ' DerivedDevice _sn_query() Performing serial number query.')
        sn = 'NA'
        return sn

    # Public methods of this class

    # Getter

    pass  # End of class definition

    pass  # end of DerivedDevice class definition


if __name__ == '__main__':
    app_name = re.split('.py', os.path.basename(__file__))[0]
    print(app_name + "\n\
Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme")
    # Logging setup
    print('Configuring logging...')
    # Define logger to be used
    # logging.basicConfig(  # filename='example.log', filemode='w', encoding='utf-8', level=logging.INFO, style='{', format='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    logger = logging.getLogger('App-Logger')
    logger.setLevel(logging.DEBUG)
    pyacdaq_logger = logging.getLogger('pyacdaq')
    pyacdaq_logger.setLevel(logging.DEBUG)
    derived_device_logger = logging.getLogger('DerivedActor')
    derived_device_logger.setLevel(logging.DEBUG)
    # Define logging formatter
    logging_formatter = logging.Formatter(style='{', fmt='{asctime} [{levelname:8}] {module} {funcName} {lineno} {message}')
    # logging_formatter = logging.Formatter(style='{', fmt='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    # configure_logging_handler((logger, pyacdaq_logger), logging_formatter, severity={'console': None, 'file': None, 'mqtt': None, 'syslog': None}, mqtt={'broker': 'ee-raspi1001', 'topic': app_name + '/log_msg', 'user': '', 'password': ''})

    mqtt_broker = input('MQTT Broker? (default: localhost)>')
    if len(mqtt_broker) == 0:
        mqtt_broker = 'localhost'
    mqtt_user = input('MQTT Username: ')
    if len(mqtt_user) == 0:
        mqtt_user = None
    mqtt_password = getpass.getpass()
    if len(mqtt_password) == 0:
        mqtt_password = None
    configure_logging_handler(app_name, (logger, pyacdaq_logger, derived_device_logger), logging_formatter, severity={'console': logging.INFO, 'file': logging.DEBUG, 'mqtt': logging.INFO, 'syslog': logging.WARNING}, mqtt={'broker': mqtt_broker, 'topic': 'DerivedDevice/log_msg', 'user': mqtt_user, 'password': mqtt_password})
    print('Please, wait for MQTT actor.')
    try:
        # Create MQTT actor proxy
        a_mqtt = Mqtt.start('Mqtt',
                            start_periodic=False,
                            broker=mqtt_broker,
                            port=1883,
                            client_id='DerivedDevice',
                            clean_session=True,
                            retain=True,
                            will='offline',
                            username=mqtt_user,
                            password=mqtt_password
                            )
        p_mqtt = a_mqtt.proxy()
        # Create device proxy
        print('Please, wait for device.')
        time.sleep(3)
        a_device = DerivedDevice.start('DerivedDevice',
                                       start_periodic=False,
                                       mqtt=p_mqtt,
                                       resource='NA',
                                       id_query_flag=True,
                                       reset_flag=True,
                                       reset_options='myResetOptions',
                                       selftest_flag=True,
                                       )
        p_device = a_device.proxy()
        device_name = p_device.getName().get()
        device_birthday = p_device.getBirthday().get()
        device_periodic = p_device.getPeriodicStatus().get()
        device_sw_revision = p_device.getSWRevision().get()
        device_instrument_id = p_device.getInstrumentID().get()
        device_firmware_revision = p_device.getFirmwareRevision().get()
        device_hardware_revision = p_device.getHardwareRevision().get()
        device_serial_number = p_device.getSerialNumber().get()
        print(__name__, 'Name:', device_name,
              '; Birthday:', device_birthday,
              '; Periodic:', device_periodic,
              '; SW:', device_sw_revision,
              '; ID:', device_instrument_id,
              '; FW:', device_firmware_revision,
              '; HW:', device_hardware_revision,
              '; SN:', device_serial_number)
        time.sleep(15)
    except Exception as e:
        print('_main__: Exception caught:', e)
    finally:
        p_device.stop()
        time.sleep(3)
        p_mqtt.stop()
    logging.shutdown()
    print(__name__, 'Done.')
