# PyAcdaq
PyAcadq is a Python class library for Automation Control & Data Acquisition Systems.
It is meant as a possible substitute (Plan B) for the LabVIEW based [CS](https://git.gsi.de/EE-LV/CS) and [CS++](https://git.gsi.de/EE-LV/CSPP) frameworks.

Basis design idea is sketched in documentation/PyACDAQ.pdf (not really up to date).
- Package name will be pyacdaq
- Actor oriented aproach is based on Pykka.
- An alternate approach is based on asyncio.
- Paho-MQTT will be used to publish process variables to network.
- Examples can be found in the repository.

This module uses its own logging handler for debug output:
logging.getLogger('pyacdaq').addHandler(logging.NullHandler()) 

Related documents and information
=================================
- README.md
- LICENSE & eupl_v1.2_de.pdf & eupl_v1.2_en.pdf
- Contact: H.Brand@gsi.de
- Cone or Download: https://git.gsi.de/EKS/Python/ACDAQ/PyAcdaq
- Bug reports: https://git.gsi.de/EKS/Python/ACDAQ/PyAcdaq/-/issues

Dependencies
============
Refer pyproject.toml for dependencies.
- Paho-MQTT
  - https://pypi.org/project/paho-mqtt/
- Pykka
  - https://pypi.org/project/pykka/
- Serial
  - https://pypi.org/project/pyserial/

Acknowledgements
================
- The MQTT part is inpired by http://www.steves-internet-guide.com
- Special thanks to Dennis Klein (D.Klein@gsi.de). He supported the package building.

Known issues
============
- Mqtt.publish_topic() supports a limited number of data types, especially when expanding array, list tuple or numpy.ndarray. Refer to source for details.
- tkinter is maybe not installed by default on Linux.
  - You may need to install missing tkinter, Make sure to install the version associated with yout Python version!
  >sudo apt install python311-tk

License
=======
Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme

Release History
===============
Release 0.1.4
---------------
31.08.2024, H.Brand@gsi.de
- Add skelleton for AsynBase supporting asyncio.
- Add skelleton for AsynBaseDevice supporting asyncio.
- Add AsynComInterface, AsynSerialInterface(AsynComInterface), AsynSocketInterface(AsynComInterface)
- Add type annotations.

Release 0.1.3
---------------
04.31.2024, H.Brand@gsi.de
- Modify SCPI serial example to support several typical termination characters (CR|CRLF|LF)

Release 0.1.2
-------------
17.01.2024, H.Brand@gsi.de
- Add hint to pip instal pyserial in README.md
- Add hint to pip instal pyserial in scpi_device_simulator_serial.py

Release 0.1.0
-------------
15.12.2023, H.Brand@gsi.de
- Release with improved logging and doc-strings.
- Previous releases were successfully used with the VacuumHeatingSystem.
  - https://git.gsi.de/EKS/Python/ACDAQ/VacuumHeatingSystem
